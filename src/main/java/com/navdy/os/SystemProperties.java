package com.navdy.os;

public class SystemProperties {
    private static Class<?> CLASS;

    static {
        try {
            CLASS = Class.forName("android.os.SystemProperties");
        } catch (ClassNotFoundException ignored) {
        }
    }

    public static String get(String key) {
        try {
            return (String) CLASS.getMethod("get", String.class).invoke(null, new Object[]{key});
        } catch (Exception e) {
            return null;
        }
    }

    public static String get(String key, String def) {
        try {
            return (String) CLASS.getMethod("get", String.class, String.class).invoke(null, new Object[]{key, def});
        } catch (Exception e) {
            return def;
        }
    }

    public static int getInt(String key, int def) {
        try {
            def = (Integer) CLASS.getMethod("getInt", String.class, Integer.TYPE).invoke(null, new Object[]{key, def});
        } catch (Exception ignored) {
        }
        return def;
    }

    public static long getLong(String key, long def) {
        try {
            def = (Long) CLASS.getMethod("getLong", String.class, Long.TYPE).invoke(null, new Object[]{key, def});
        } catch (Exception ignored) {
        }
        return def;
    }

    public static boolean getBoolean(String key, boolean def) {
        try {
            def = (Boolean) CLASS.getMethod("getBoolean", String.class, Boolean.TYPE).invoke(null, new Object[]{key, def});
        } catch (Exception ignored) {
        }
        return def;
    }

    private SystemProperties() {
    }
}
