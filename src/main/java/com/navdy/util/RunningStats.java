package com.navdy.util;

import kotlin.Metadata;

@Metadata(bv = {1, 0, 1}, d1 = {"\u0000$\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\n\u0002\u0010\t\n\u0002\b\u0005\n\u0002\u0010\u0006\n\u0002\b\b\n\u0002\u0010\u0002\n\u0002\b\b\u0018\u00002\u00020\u0001B\u0005¢\u0006\u0002\u0010\u0002J\u000e\u0010\u0012\u001a\u00020\u00132\u0006\u0010\u0014\u001a\u00020\nJ\u0006\u0010\u0015\u001a\u00020\u0004J\u0006\u0010\u0016\u001a\u00020\nJ\u0006\u0010\u0017\u001a\u00020\u0013J\u0006\u0010\u0018\u001a\u00020\nJ\u0006\u0010\u0019\u001a\u00020\nJ\u000e\u0010\u001a\u001a\u00020\n2\u0006\u0010\u0014\u001a\u00020\nR\u001a\u0010\u0003\u001a\u00020\u0004X\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0005\u0010\u0006\"\u0004\b\u0007\u0010\bR\u001a\u0010\t\u001a\u00020\nX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u000b\u0010\f\"\u0004\b\r\u0010\u000eR\u001a\u0010\u000f\u001a\u00020\nX\u000e¢\u0006\u000e\n\u0000\u001a\u0004\b\u0010\u0010\f\"\u0004\b\u0011\u0010\u000e¨\u0006\u001b"}, d2 = {"Lcom/navdy/util/RunningStats;", "", "()V", "n", "", "getN", "()J", "setN", "(J)V", "newM", "", "getNewM", "()D", "setNewM", "(D)V", "newV", "getNewV", "setNewV", "add", "", "value", "count", "mean", "reset", "standardDeviation", "variance", "zScore", "obd-service_release"}, k = 1, mv = {1, 1, 6})
/* compiled from: RunningStats.kt */
public final class RunningStats {
    private long n;
    private double newM;
    private double newV;

    public final long getN() {
        return this.n;
    }

    public final void setN(long _set) {
        this.n = _set;
    }

    public final double getNewM() {
        return this.newM;
    }

    public final void setNewM(double _set) {
        this.newM = _set;
    }

    public final double getNewV() {
        return this.newV;
    }

    public final void setNewV(double _set) {
        this.newV = _set;
    }

    public final void add(double value) {
        this.n++;
        if (this.n == 1) {
            this.newM = value;
            this.newV = 0.0d;
            return;
        }
        double oldM = this.newM;
        double oldV = this.newV;
        this.newM = ((value - oldM) / ((double) this.n)) + oldM;
        this.newV = ((value - oldM) * (value - this.newM)) + oldV;
    }

    public final long count() {
        return this.n;
    }

    public final double mean() {
        return this.n > ((long) 0) ? this.newM : 0.0d;
    }

    public final double variance() {
        return this.n > ((long) 1) ? this.newV / ((double) (this.n - ((long) 1))) : 0.0d;
    }

    public final double standardDeviation() {
        return Math.sqrt(variance());
    }

    public final double zScore(double value) {
        double standardDeviation = standardDeviation();
        double mean = mean();
        if (standardDeviation != 0.0d) {
            return Math.abs((value - mean) / standardDeviation);
        }
        if (value == mean) {
            return 0.0d;
        }
        return 10.0d;
    }

    public final void reset() {
        this.n = 0;
        this.newM = 0.0d;
        this.newV = 0.0d;
    }
}
