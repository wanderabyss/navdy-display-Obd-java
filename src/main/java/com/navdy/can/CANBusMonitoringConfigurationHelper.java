package com.navdy.can;

import android.content.Context;
import android.text.TextUtils;

import com.navdy.obd.can.CANBusDataDescriptor;
import com.navdy.obd.command.CANBusMonitoringCommand;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Marker;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

import util.Configuration;
import util.Util;

public class CANBusMonitoringConfigurationHelper {
    private static Configuration[] CAR_DETAILS_CONFIGURATION_MAPPING = null;
    public static final String CAR_DETAILS_CONFIGURATION_MAPPING_FILE = "configuration_mapping.csv";
    private static Configuration[] VIN_CONFIGURATION_MAPPING = null;
    public static final String VIN_CONFIGURATION_MAPPING_FILE = "vin_configuration_mapping.csv";

    class Column {
        public static final String BIT_LENGTH = "BitLength";
        public static final String BIT_OFFSET = "BitOffset";
        public static final String BYTE_OFFSET = "ByteOffset";
        public static final String ENDIAN = "Endian";
        public static final String FREQ = "Freq";
        public static final String HEADER = "Header";
        public static final String MAX_VALUE = "MaxValue";
        public static final String MIN_VALUE = "MinValue";
        public static final String NAME = "Name";
        public static final String OBD_PID = "OBD_Pid";
        public static final String RESOLUTION = "Resolution";
        public static final String VALUE_OFFSET = "ValueOffset";

        Column() {
        }
    }

    public static CANBusMonitoringCommand loadCANBusMonitoringSetupCommand(Context context, String vin) {
        if (VIN_CONFIGURATION_MAPPING == null) {
            VIN_CONFIGURATION_MAPPING = Util.loadConfigurationMappingList(context, VIN_CONFIGURATION_MAPPING_FILE);
        }
        Configuration matchingConfiguration = Util.pickConfiguration(VIN_CONFIGURATION_MAPPING, vin);
        if (matchingConfiguration != null) {
            return loadCommandFromConfigurationFile(context, matchingConfiguration.configurationName);
        }
        return null;
    }

    public static CANBusMonitoringCommand loadCANBusMonitoringSetupCommand(Context context, String make, String model, String year) {
        if (CAR_DETAILS_CONFIGURATION_MAPPING == null) {
            CAR_DETAILS_CONFIGURATION_MAPPING = Util.loadConfigurationMappingList(context, VIN_CONFIGURATION_MAPPING_FILE);
        }
        StringBuilder stringBuilder = new StringBuilder();
        if (TextUtils.isEmpty(make)) {
            make = Marker.ANY_MARKER;
        }
        stringBuilder = stringBuilder.append(make).append("_");
        if (TextUtils.isEmpty(model)) {
            model = Marker.ANY_MARKER;
        }
        stringBuilder = stringBuilder.append(model).append("_");
        if (TextUtils.isEmpty(year)) {
            year = Marker.ANY_MARKER;
        }
        Configuration matchingConfiguration = Util.pickConfiguration(CAR_DETAILS_CONFIGURATION_MAPPING, stringBuilder.append(year).toString());
        if (matchingConfiguration != null) {
            return loadCommandFromConfigurationFile(context, matchingConfiguration.configurationName);
        }
        return null;
    }

    private static CANBusMonitoringCommand loadCommandFromConfigurationFile(Context context, String configurationName) {
        try {
            Iterable<CSVRecord> records = CSVFormat.DEFAULT.withHeader().parse(new BufferedReader(new InputStreamReader(context.getAssets().open(configurationName))));
            ArrayList<CANBusDataDescriptor> descriptors = new ArrayList<>();
            for (CSVRecord record : records) {
                String name = record.get(Column.NAME);
                String header = record.get(Column.HEADER);
                int byteOffset = Integer.parseInt(record.get(Column.BYTE_OFFSET));
                int bitOffset = Integer.parseInt(record.get(Column.BIT_OFFSET));
                int bitLength = Integer.parseInt(record.get(Column.BIT_LENGTH));
                int valueOffset = Integer.parseInt(record.get(Column.VALUE_OFFSET));
                long j = (long) valueOffset;
                descriptors.add(new CANBusDataDescriptor(name, header, byteOffset, bitOffset, bitLength, Double.parseDouble(record.get(Column.RESOLUTION)), j, Double.parseDouble(record.get(Column.MIN_VALUE)), Double.parseDouble(record.get(Column.MAX_VALUE)), Integer.parseInt(record.get(Column.OBD_PID)), Integer.parseInt(record.get(Column.FREQ)), "L".equals(record.get(Column.ENDIAN))));
            }
            return new CANBusMonitoringCommand(descriptors);
        } catch (IOException e) {
            return null;
        }
    }
}
