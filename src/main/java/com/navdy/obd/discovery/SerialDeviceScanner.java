package com.navdy.obd.discovery;

import android.content.Context;
import android.os.Handler;

import com.navdy.obd.io.ChannelInfo;
import com.navdy.obd.io.ChannelInfo.ConnectionType;

public class SerialDeviceScanner extends ObdChannelScanner {
    private static final String SERIAL_DEVICE = "/dev/ttymxc3";
    private static final String[] SERIAL_PORTS = new String[]{SERIAL_DEVICE};
    private static final String TAG = "SerialDeviceScanner";

    public SerialDeviceScanner(Context context, Listener listener) {
        super(context, listener);
    }

    public boolean startScan() {
        this.mListener.onScanStarted(this);
        new Handler().post(SerialDeviceScanner.this::findDevices);
        return true;
    }

    public boolean stopScan() {
        this.mListener.onScanStopped(this);
        return true;
    }

    private void findDevices() {
        String[] ports = SERIAL_PORTS;
        for (String channelInfo : ports) {
            this.mListener.onDiscovered(this, new ChannelInfo(ConnectionType.SERIAL, "SerialPort", channelInfo));
        }
    }
}
