package com.navdy.obd.discovery;

import android.content.Context;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import com.navdy.obd.discovery.ObdChannelScanner.Listener;
import com.navdy.obd.io.ChannelInfo;
import com.navdy.obd.io.ChannelInfo.ConnectionType;
import com.navdy.os.SystemProperties;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;

public class GroupScanner extends ObdChannelScanner implements Listener {
    public static final String DEFAULT_CHANNELS = "bluetooth,serial";
    public static final String DEFAULT_CHANNELS_PROPERTY_NAME = "obd.channels";
    public static final String DEFAULT_HUD_CHANNELS = "serial";
    private static final String TAG = "GroupScanner";
    HashSet discoveredChannels = new HashSet();
    private List<ObdChannelScanner> scanners = new ArrayList<>();

    public GroupScanner(Context context, Listener listener) {
        super(context, listener);
        for (String channelType : TextUtils.split(SystemProperties.get(DEFAULT_CHANNELS_PROPERTY_NAME, defaultChannels()), ",")) {
            ObdChannelScanner scanner = instantiateScanner(context, channelType.trim());
            if (scanner != null) {
                this.scanners.add(scanner);
            }
        }
    }

    private ObdChannelScanner instantiateScanner(Context context, String channelType) {
        ConnectionType type = null;
        if (!TextUtils.isEmpty(channelType)) {
            try {
                type = ConnectionType.valueOf(channelType.toUpperCase(Locale.US));
            } catch (IllegalArgumentException e) {
                Log.e(TAG, "Failed to parse channelType " + channelType, e);
            }
        }
        if (type == null) {
            return null;
        }
        switch (type) {
            case BLUETOOTH:
                return new BluetoothObdScanner(context, this);
            case MOCK:
                return new MockObdScanner(context, this);
            case SERIAL:
                return new SerialDeviceScanner(context, this);
            default:
                return null;
        }
    }

    private String defaultChannels() {
        if (Build.MODEL.equalsIgnoreCase("NAVDY_HUD-MX6DL") || Build.MODEL.equalsIgnoreCase("Display")) {
            return "serial";
        }
        return DEFAULT_CHANNELS;
    }

    public HashSet getDiscoveredChannels() {
        return this.discoveredChannels;
    }

    public boolean startScan() {
        this.discoveredChannels.clear();
        for (ObdChannelScanner scanner : this.scanners) {
            scanner.startScan();
        }
        return true;
    }

    public boolean stopScan() {
        for (ObdChannelScanner scanner : this.scanners) {
            scanner.stopScan();
        }
        return true;
    }

    public void onScanStarted(ObdChannelScanner scanner) {
        if (this.mListener != null) {
            this.mListener.onScanStarted(this);
        }
    }

    public void onScanStopped(ObdChannelScanner scanner) {
        if (this.mListener != null) {
            this.mListener.onScanStopped(this);
        }
    }

    public void onDiscovered(ObdChannelScanner scanner, ChannelInfo obdDevice) {
        this.discoveredChannels.add(obdDevice);
        if (this.mListener != null) {
            this.mListener.onDiscovered(scanner, obdDevice);
        }
    }
}
