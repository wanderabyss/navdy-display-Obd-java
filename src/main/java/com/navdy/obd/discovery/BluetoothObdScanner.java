package com.navdy.obd.discovery;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Handler;
import android.util.Log;

import com.navdy.obd.io.ChannelInfo;
import com.navdy.obd.io.ChannelInfo.ConnectionType;

import java.util.Locale;
import java.util.Set;

public class BluetoothObdScanner extends ObdChannelScanner {
    private static final String TAG = "BluetoothObdScanner";
    private BluetoothAdapter mBtAdapter = BluetoothAdapter.getDefaultAdapter();
    private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if ("android.bluetooth.device.action.FOUND".equals(action) || "android.bluetooth.device.action.NAME_CHANGED".equals(action)) {
                BluetoothDevice device = intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
                Log.d(BluetoothObdScanner.TAG, "Friendly name = " + intent.getStringExtra("android.bluetooth.device.extra.NAME"));
                if (device.getBondState() != 12 && BluetoothObdScanner.this.isObdDevice(device)) {
                    BluetoothObdScanner.this.mListener.onDiscovered(BluetoothObdScanner.this, new ChannelInfo(ConnectionType.BLUETOOTH, device.getName(), device.getAddress()));
                }
            } else if ("android.bluetooth.adapter.action.DISCOVERY_FINISHED".equals(action)) {
                BluetoothObdScanner.this.mListener.onScanStopped(BluetoothObdScanner.this);
            }
        }
    };
    private boolean scanning = false;

    public BluetoothObdScanner(Context context, Listener listener) {
        super(context, listener);
    }

    public boolean startScan() {
        if (!this.scanning) {
            this.scanning = true;
            this.mContext.registerReceiver(this.mReceiver, new IntentFilter("android.bluetooth.device.action.FOUND"));
            this.mContext.registerReceiver(this.mReceiver, new IntentFilter("android.bluetooth.adapter.action.DISCOVERY_FINISHED"));
            if (this.mBtAdapter != null) {
                if (this.mBtAdapter.isDiscovering()) {
                    this.mBtAdapter.cancelDiscovery();
                }
                this.mBtAdapter.startDiscovery();
                new Handler().post(BluetoothObdScanner.this::enumerateBondedDevices);
            }
        }
        return this.scanning;
    }

    public boolean stopScan() {
        if (this.scanning) {
            this.scanning = false;
            if (this.mBtAdapter != null) {
                this.mBtAdapter.cancelDiscovery();
            }
            this.mContext.unregisterReceiver(this.mReceiver);
        }
        return this.scanning;
    }

    private void enumerateBondedDevices() {
        Set<BluetoothDevice> pairedDevices = this.mBtAdapter.getBondedDevices();
        if (pairedDevices.size() > 0) {
            for (BluetoothDevice device : pairedDevices) {
                if (isObdDevice(device)) {
                    this.mListener.onDiscovered(this, new ChannelInfo(ConnectionType.BLUETOOTH, device.getName(), device.getAddress(), true));
                }
            }
        }
    }

    private boolean isObdDevice(BluetoothDevice device) {
        return device.getName() != null && device.getName().toLowerCase(Locale.US).contains("obd");
    }
}
