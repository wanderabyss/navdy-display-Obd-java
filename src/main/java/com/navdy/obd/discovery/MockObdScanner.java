package com.navdy.obd.discovery;

import android.content.Context;
import android.os.Handler;

import com.navdy.obd.simulator.MockObdChannel;

public class MockObdScanner extends ObdChannelScanner {
    public MockObdScanner(Context context, Listener listener) {
        super(context, listener);
    }

    public boolean startScan() {
        this.mListener.onScanStarted(this);
        new Handler().post(() -> MockObdScanner.this.mListener.onDiscovered(MockObdScanner.this, MockObdChannel.MOCK_CHANNEL_INFO));
        return true;
    }

    public boolean stopScan() {
        this.mListener.onScanStopped(this);
        return true;
    }
}
