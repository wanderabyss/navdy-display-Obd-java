package com.navdy.obd.converters;

public interface AbstractConversion {
    double convert(byte[] bArr);
}
