package com.navdy.obd.converters;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import parsii.eval.Expression;
import parsii.eval.Parser;
import parsii.eval.Scope;
import parsii.eval.Variable;
import parsii.tokenizer.ParseError;
import parsii.tokenizer.ParseException;
import parsii.tokenizer.Position;

public class CustomConversion implements AbstractConversion {
    private Expression expression;
    private VariableOffset[] offsets;

    static class VariableOffset {
        private int offset;
        private Variable variable;

        public VariableOffset(Variable variable, int offset) {
            this.variable = variable;
            this.offset = offset;
        }
    }

    public CustomConversion(String expression) throws ParseException {
        Scope scope = Scope.create();
        this.expression = Parser.parse(expression, scope);
        Collection<Variable> variables = scope.getLocalVariables();
        this.offsets = new VariableOffset[variables.size()];
        int i = 0;
        for (Variable variable : variables) {
            int offset = offset(variable.getName());
            if (offset < 0) {
                List<ParseError> errors = new ArrayList<>();
                errors.add(ParseError.error(Position.UNKNOWN, "Invalid variable " + variable.getName() + " in (" + expression + ")"));
                throw ParseException.create(errors);
            }
            int i2 = i + 1;
            this.offsets[i] = new VariableOffset(variable, offset);
            i = i2;
        }
    }

    public double convert(byte[] rawData) {
        for (VariableOffset var : this.offsets) {
            if (var.variable != null) {
                var.variable.setValue((double) (rawData[var.offset + 2] & 255));
            }
        }
        return this.expression.evaluate();
    }

    private int offset(String variableName) {
        int i = 1;
        if (variableName == null || variableName.length() > 2 || variableName.length() == 0) {
            return -1;
        }
        boolean twoDigit;
        int tens;
        variableName = variableName.toUpperCase();
        if (variableName.length() > 1) {
            twoDigit = true;
        } else {
            twoDigit = false;
        }
        if (twoDigit) {
            tens = ((variableName.charAt(0) - 65) + 1) * 26;
        } else {
            tens = 0;
        }
        if (!twoDigit) {
            i = 0;
        }
        return tens + (variableName.charAt(i) - 65);
    }
}
