package com.navdy.obd;

public class FuelLevelPidProcessor extends PidProcessor {
    private int SAMPLE_SIZE = 5;
    private int STEP_SIZE = 1;
    private double mFilteredValue;
    private int mReadIndex = 0;
    private double[] mReadings = new double[this.SAMPLE_SIZE];
    private int mReadingsCount = 0;
    private double mRunningTotal = 0.0d;

    public boolean isSupported(PidSet supportedPids) {
        return supportedPids.contains(47);
    }

    public boolean processPidValue(PidLookupTable vehicleState) {
        double fuelLevel = vehicleState.getPidValue(47);
        if (fuelLevel < 0.0d || fuelLevel > 100.0d) {
            return vehicleState.updatePid(47, -1.0d);
        }
        this.mRunningTotal -= this.mReadings[this.mReadIndex];
        double[] dArr = this.mReadings;
        int i = this.mReadIndex;
        this.mReadIndex = i + 1;
        dArr[i] = fuelLevel;
        this.mRunningTotal += fuelLevel;
        if (this.mReadingsCount < this.SAMPLE_SIZE) {
            this.mReadingsCount++;
        }
        if (this.mReadIndex == this.SAMPLE_SIZE) {
            this.mReadIndex = 0;
        }
        this.mFilteredValue = this.mRunningTotal / ((double) this.mReadingsCount);
        this.mFilteredValue = quantize(this.STEP_SIZE, this.mFilteredValue);
        return vehicleState.updatePid(47, this.mFilteredValue);
    }

    private double quantize(int stepSize, double value) {
        return (double) (Math.round(value / ((double) stepSize)) * ((long) stepSize));
    }

    public PidSet resolveDependencies() {
        return null;
    }
}
