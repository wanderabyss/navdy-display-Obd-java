package com.navdy.obd.update;

public class DefaultFirmwareManager implements ObdDeviceFirmwareManager {
    public String getFirmwareVersion() {
        return "N/A";
    }

    public boolean updateFirmware(Update update) {
        return false;
    }

    public boolean isUpdatingFirmware() {
        return false;
    }
}
