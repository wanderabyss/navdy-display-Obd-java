package com.navdy.obd.update;

public class BootLoaderMessageError extends Error {
    short errorCode;

    public BootLoaderMessageError(short errorCode) {
        super("NACK");
        this.errorCode = errorCode;
    }

    public String toString() {
        String errorMessage = "Bootloader error";
        switch (this.errorCode) {
            case (short) -2:
                errorMessage = "Invalid CRC in the response";
                break;
            case (short) -1:
                errorMessage = "Invalid response from bootloader";
                break;
        }
        return "Bootloader E: <" + this.errorCode + ">," + errorMessage;
    }
}