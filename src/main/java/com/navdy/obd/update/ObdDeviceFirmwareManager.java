package com.navdy.obd.update;

import android.os.RemoteException;

public interface ObdDeviceFirmwareManager {
    String getFirmwareVersion() throws RemoteException;

    boolean isUpdatingFirmware();

    boolean updateFirmware(Update update);
}
