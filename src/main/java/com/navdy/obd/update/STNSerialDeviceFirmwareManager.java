package com.navdy.obd.update;

import android.support.v4.internal.view.SupportMenu;
import android.text.TextUtils;
import android.util.Log;

import com.navdy.hardware.SerialPort;
import com.navdy.obd.ObdService;
import com.navdy.obd.io.IChannel;
import com.navdy.obd.io.STNSerialChannel;
import com.navdy.obd.update.UpdateFileHeader.FWImageDescriptor;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.OutputStream;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class STNSerialDeviceFirmwareManager implements ObdDeviceFirmwareManager {
    public static final byte BOOTLOADER_MAJOR_VERSION = (byte) 2;
    private static final int CARRIAGE_RETURN = 13;
    public static final String CURRENT_UPDATE_FILE_VERSION = "05";
    private static final long DELAY_AFTER_RESET = 150;
    public static final String FILE_SIGNATURE = "STNFWv";
    public static final Pattern FIRMWARE_VERSION_PATTERN = Pattern.compile("^STN(.+)\\s{1}v(.+)");
    public static final String INFO_COMMAND = "sti";
    public static final int MAX_RETRIES_TO_CONNECT = 10;
    public static final String RESET_COMMAND = "atz";
    public static final String TAG = STNSerialDeviceFirmwareManager.class.getSimpleName();
    public static final int UPDATE_HEADER_SIGNATURE_SIZE = 6;
    private IChannel mChannel;
    private AtomicBoolean mIsUpdating = new AtomicBoolean(false);
    private ObdService mObdService;

    public STNSerialDeviceFirmwareManager(ObdService obdService, IChannel channel) {
        init(obdService, channel);
    }

    public void init(ObdService obdService, IChannel channel) {
        this.mObdService = obdService;
        this.mChannel = channel;
    }

    public boolean updateFirmware(Update update) {
        Log.d(TAG, "updateFirmware  : " + this.mChannel);
        if (!(this.mChannel instanceof STNSerialChannel)) {
            return false;
        }
        return update(update, ((STNSerialChannel) this.mChannel).getSerialPort(), true);
    }

    public boolean update(Update update, SerialPort serialPort, boolean isInValidState) {
        STNBootloaderChannel bootloaderChannel = null;
        boolean enteredBootMode = false;
        FileInputStream fileInputStream;
        try {
            if (this.mIsUpdating.compareAndSet(false, true)) {
                UpdateFileHeader headerFile = ensureFile(update.mUpdateFilePath);
                if (headerFile == null) {
                    this.mIsUpdating.set(false);
                    return false;
                }
                bootloaderChannel = new STNBootloaderChannel(serialPort);
                enteredBootMode = false;
                if (isInValidState) {
                    OutputStream out = this.mChannel.getOutputStream();
                    String command = RESET_COMMAND;
                    int length = command.length();
                    byte[] bytes = new byte[(length + 1)];
                    System.arraycopy(command.getBytes(), 0, bytes, 0, length);
                    bytes[length] = (byte) 13;
                    out.write(bytes);
                    out.flush();
                    Thread.sleep(DELAY_AFTER_RESET);
                }
                int tries = 0;
                while (!enteredBootMode && tries < 10) {
                    tries++;
                    enteredBootMode = bootloaderChannel.enterBootloaderMode();
                    Log.d(TAG, "Enter boot mode , Success : " + enteredBootMode + ", Tries : " + tries);
                }
                if (enteredBootMode) {
                    byte[] version = bootloaderChannel.getVersion();
                    Log.d(TAG, "Firmware status " + bootloaderChannel.getFirmwareStatus());
                    byte majorVersion = version[2];
                    Log.e(TAG, "Major version " + version[2] + ", Minor version " + version[3]);
                    if (majorVersion != (byte) 2) {
                        Log.e(TAG, "The bootloader version is not supported" + majorVersion);
                        updateStopped(true, bootloaderChannel);
                        updateStopped(enteredBootMode, bootloaderChannel);
                        return false;
                    }
                    short imagesCount = headerFile.fwImageDescriptorsCount;
                    short nextFwImageIndex = (short) 0;
                    FileInputStream reader = null;
                    while (nextFwImageIndex != (short) 255 && nextFwImageIndex >= (short) 0 && nextFwImageIndex < imagesCount) {
                        try {
                            Log.d(TAG, "Uploading fw image" + nextFwImageIndex);
                            FWImageDescriptor desc = headerFile.fwImageDescriptors[nextFwImageIndex];
                            nextFwImageIndex = desc.nextFwIndex;
                            int imageSize = (int) (desc.imageSize & 16777215);
                            Log.d(TAG, "Image size " + imageSize);
                            int maxChunkSize = bootloaderChannel.startUpload(imageSize);
                            Log.d(TAG, "Max chunk size " + maxChunkSize);
                            if (maxChunkSize > 0) {
                                int chunkSize = Math.min(STNBootloaderChannel.DEFAULT_CHUNK_SIZE, maxChunkSize);
                                int chunkCount = imageSize / chunkSize;
                                int fullChunks = chunkCount;
                                chunkCount += (imageSize % chunkSize) / 16;
                                Log.d(TAG, "Total chunk count : " + chunkCount);
                                File file = new File(update.mUpdateFilePath);
                                long offset = desc.imageOffset;
                                FileInputStream fileInputStream2 = new FileInputStream(file);
                                fileInputStream2.skip(offset);
                                try {
                                    byte[] buffer = new byte[(fullChunks > 0 ? chunkSize : 16)];
                                    int chunk = 0;
                                    while (chunk < chunkCount) {
                                        int size = chunkSize;
                                        if (chunk >= fullChunks) {
                                            size = 16;
                                        }
                                        Log.d(TAG, "Writing chunk " + chunk + ", Size : " + size);
                                        if (buffer.length != size) {
                                            buffer = new byte[size];
                                        }
                                        fileInputStream2.read(buffer);
                                        int chunkId = bootloaderChannel.sendChunk(chunk, buffer);
                                        if (chunk == chunkId) {
                                            Log.d(TAG, "Successfully sent chunk " + chunkId);
                                            chunk++;
                                        } else {
                                            Log.d(TAG, "Sending chunk " + chunk + " failed, id mismatch :" + chunkId);
                                            updateStopped(true, bootloaderChannel);
                                            if (fileInputStream2 != null) {
                                                fileInputStream2.close();
                                            }
                                            updateStopped(enteredBootMode, bootloaderChannel);
                                            return false;
                                        }
                                    }
                                    if (fileInputStream2 != null) {
                                        try {
                                            fileInputStream2.close();
                                        } catch (IOException e) {
                                            Log.e(TAG, "Error closing the file reader" + e.getMessage());
                                            e.printStackTrace();
                                        }
                                    }
                                    reader = fileInputStream2;
                                } catch (BootLoaderMessageError be) {
                                    Log.e(TAG, "Error response from bootloader while sending chunk " + be);
                                    if (desc.imageType == (short) 16 && be.errorCode == (short) -112) {
                                        nextFwImageIndex = desc.errorFwIndex;
                                    } else {
                                        updateStopped(true, bootloaderChannel);
                                        if (fileInputStream2 != null) {
                                            try {
                                                fileInputStream2.close();
                                            } catch (IOException e22) {
                                                Log.e(TAG, "Error closing the file reader" + e22.getMessage());
                                                e22.printStackTrace();
                                            }
                                        }
                                        updateStopped(enteredBootMode, bootloaderChannel);
                                        return false;
                                    }
                                } catch (Throwable th2) {
                                    if (fileInputStream2 != null) {
                                        try {
                                            fileInputStream2.close();
                                        } catch (IOException e222) {
                                            Log.e(TAG, "Error closing the file reader" + e222.getMessage());
                                            e222.printStackTrace();
                                        }
                                    }
                                }
                            } else {
                                Log.e(TAG, "Start upload command failed, bootloader returned invalid max chunk size");
                                updateStopped(true, bootloaderChannel);
                                updateStopped(enteredBootMode, bootloaderChannel);
                                return false;
                            }
                        } catch (Throwable th3) {
                            fileInputStream = reader;
                        }
                    }
                    boolean validFirmware = bootloaderChannel.getFirmwareStatus();
                    if (validFirmware) {
                        Log.d(TAG, "After uploading the firmware is valid");
                    } else {
                        Log.d(TAG, "Invalid firmware, needs re installation");
                    }
                    updateStopped(true, bootloaderChannel);
                    updateStopped(enteredBootMode, bootloaderChannel);
                    return validFirmware;
                }
                Log.e(TAG, "Failed to enter boot mode, aborting update");
                updateStopped(false, bootloaderChannel);
                updateStopped(enteredBootMode, bootloaderChannel);
                return false;
            }
            return false;
        } catch (Throwable th5) {
            Log.e(TAG, "Unhandled error during update process " + th5);
            th5.printStackTrace();
            updateStopped(enteredBootMode, bootloaderChannel);
            return false;
        }
    }

    private void updateStopped(boolean isInBootMode, STNBootloaderChannel bootloaderChannel) {
        if (this.mIsUpdating.get()) {
            if (isInBootMode && bootloaderChannel != null) {
                try {
                    bootloaderChannel.reset();
                } catch (Throwable e) {
                    Log.e(TAG, "Exception while resetting the device");
                    e.printStackTrace();
                }
            }
            this.mIsUpdating.set(false);
        }
    }

    public boolean isUpdatingFirmware() {
        return this.mIsUpdating.get();
    }

    public String getFirmwareVersion() {
        String versionResponse = this.mObdService.sendCommand(INFO_COMMAND);
        if (!TextUtils.isEmpty(versionResponse)) {
            Matcher matcher = FIRMWARE_VERSION_PATTERN.matcher(versionResponse);
            if (!matcher.find() || matcher.groupCount() != 2) {
                return versionResponse;
            }
            String compactVersion = matcher.group(2);
            String expression = matcher.group(0);
            Log.d(TAG, "STN device :" + matcher.group(1) + ", firmware :" + compactVersion);
            return compactVersion;
        }
        return null;
    }

    private UpdateFileHeader ensureFile(String filePath) {
        FileNotFoundException ie;
        Throwable th;
        Exception e;
        File file = new File(filePath);
        if (!file.exists() || !file.isFile() || !file.canRead()) {
            return null;
        }
        FileInputStream fis = null;
        try {
            FileInputStream fis2 = new FileInputStream(file);
            try {
                DataInputStream dis = new DataInputStream(fis2);
                UpdateFileHeader updateFileHeader = new UpdateFileHeader();
                byte[] signatureData = new byte[6];
                dis.read(signatureData, 0, 6);
                String signature = new String(signatureData);
                Log.d(TAG, "Signature : " + signature);
                if (signature.equals(FILE_SIGNATURE)) {
                    byte[] versionData = new byte[2];
                    dis.read(versionData, 0, 2);
                    String version = new String(versionData);
                    Log.d(TAG, "File version : " + version);
                    if (version.equals(CURRENT_UPDATE_FILE_VERSION)) {
                        short i;
                        short deviceIdCount = (short) dis.readByte();
                        boolean stnDeviceFound = false;
                        for (i = (short) 0; i < deviceIdCount; i = (short) (i + 1)) {
                            if ((dis.readShort() & SupportMenu.USER_MASK) == 4368) {
                                stnDeviceFound = true;
                            }
                        }
                        if (stnDeviceFound) {
                            short imageDescriptorCount = (short) dis.readByte();
                            updateFileHeader.fwImageDescriptorsCount = imageDescriptorCount;
                            Log.d(TAG, "Number of image descriptors : " + updateFileHeader.fwImageDescriptorsCount);
                            FWImageDescriptor[] descriptors = new FWImageDescriptor[imageDescriptorCount];
                            for (i = (short) 0; i < imageDescriptorCount; i = (short) (i + 1)) {
                                Log.d(TAG, "---------------------------------------------\n\n");
                                FWImageDescriptor descriptor = new FWImageDescriptor();
                                descriptor.imageType = (short) (dis.readByte() & 255);
                                Log.d(TAG, "ImageType : " + descriptor.imageType);
                                short reserved = (short) dis.readByte();
                                descriptor.nextFwIndex = (short) (dis.readByte() & 255);
                                Log.d(TAG, "Next FW index : " + descriptor.nextFwIndex);
                                descriptor.errorFwIndex = (short) (dis.readByte() & 255);
                                Log.d(TAG, "Error FW index : " + descriptor.errorFwIndex);
                                descriptor.imageOffset = (long) dis.readInt();
                                Log.d(TAG, "Image offset : " + descriptor.imageOffset);
                                descriptor.imageSize = (long) dis.readInt();
                                Log.d(TAG, "Image size : " + descriptor.imageSize);
                                descriptors[i] = descriptor;
                            }
                            updateFileHeader.fwImageDescriptors = descriptors;
                            try {
                                fis2.close();
                                return updateFileHeader;
                            } catch (IOException e2) {
                                Log.e(TAG, "Error closing the file input stream");
                                return updateFileHeader;
                            }
                        }
                        Log.d(TAG, "Update not compatible with our Obd Chip");
                        try {
                            fis2.close();
                            return null;
                        } catch (IOException e3) {
                            Log.e(TAG, "Error closing the file input stream");
                            return null;
                        }
                    }
                    try {
                        fis2.close();
                        return null;
                    } catch (IOException e4) {
                        Log.e(TAG, "Error closing the file input stream");
                        return null;
                    }
                }
                try {
                    fis2.close();
                    return null;
                } catch (IOException e5) {
                    Log.e(TAG, "Error closing the file input stream");
                    return null;
                }
            } catch (FileNotFoundException e6) {
                ie = e6;
                fis = fis2;
                try {
                    Log.e(TAG, "IOException while reading from update file " + ie);
                    try {
                        fis.close();
                    } catch (IOException e7) {
                        Log.e(TAG, "Error closing the file input stream");
                    }
                    return null;
                } catch (Throwable th2) {
                    try {
                        fis.close();
                    } catch (IOException e8) {
                        Log.e(TAG, "Error closing the file input stream");
                    }
                    throw th2;
                }
            } catch (Exception e9) {
                e = e9;
                fis = fis2;
                Log.e(TAG, "Error while reading from update file " + e);
                try {
                    fis.close();
                } catch (IOException e10) {
                    Log.e(TAG, "Error closing the file input stream");
                }
                return null;
            } catch (Throwable th3) {
                fis = fis2;
                fis.close();
                throw th3;
            }
        } catch (FileNotFoundException e11) {
            ie = e11;
            Log.e(TAG, "IOException while reading from update file " + ie);
            try {
                if (fis != null) {
                    fis.close();
                }
            } catch (IOException e10) {
                Log.e(TAG, "Error closing the file input stream");
            }
            return null;
        } catch (Exception e12) {
            e = e12;
            Log.e(TAG, "Error while reading from update file " + e);
            try {
                if (fis != null) {
                    fis.close();
                }
            } catch (IOException e10) {
                Log.e(TAG, "Error closing the file input stream");
            }
            return null;
        }
    }
}
