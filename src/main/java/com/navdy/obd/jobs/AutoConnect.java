package com.navdy.obd.jobs;

import com.navdy.obd.CarState;
import com.navdy.obd.IObdServiceListener.Stub;
import com.navdy.obd.IPidListener;
import com.navdy.obd.ObdService;
import com.navdy.obd.Pid;
import com.navdy.obd.PidSet;
import com.navdy.obd.discovery.GroupScanner;
import com.navdy.obd.io.ChannelInfo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class AutoConnect implements Runnable {
    private static final int FULL_SCAN_DELAY = 4000;
    private static final int INITIAL_SCAN_DELAY = 100;
    static final Logger Log = LoggerFactory.getLogger(AutoConnect.class);
    public static final int MINIMAL_DELAY_TO_RECONNECT = 500;
    private static final long RECONNECT_DELAY = TimeUnit.SECONDS.toMillis(1);
    private static final long SLEEP_DELAY = TimeUnit.SECONDS.toMillis(30);
    private AutoConnectState autoConnectState = AutoConnectState.SLEEPING;
    private final CarState carState;
    private Stub obdListener = new Stub() {
        public void onConnectionStateChange(int newState) {
            if (newState == 0) {
                AutoConnect.this.setState(AutoConnectState.DISCONNECTED);
            } else if (newState == 2) {
                AutoConnect.this.sleepDelay = AutoConnect.RECONNECT_DELAY;
                AutoConnect.this.setState(AutoConnectState.CONNECTED);
                AutoConnect.Log.info("Starting scanning");
                List<Pid> initialSet = new ArrayList<>();
                initialSet.add(new Pid(13));
                AutoConnect.this.getReadings(initialSet);
            }
        }

        public void onStatusMessage(String message) {
        }

        public void onRawData(String data) {
        }

        public void scannedPids(List<Pid> pids) {
            AutoConnect.Log.debug("Recording readings for " + (pids != null ? pids.size() : 0) + " pids");
            synchronized (AutoConnect.this.carState) {
                AutoConnect.this.carState.recordReadings(pids);
            }
        }

        public void supportedPids(List<Pid> list) {
        }

        public void scannedVIN(String vin) {
        }
    };
    private IPidListener pidListener = new IPidListener.Stub() {
        public void pidsChanged(List<Pid> list) {
        }

        public void onConnectionStateChange(int newState) {
        }

        public void pidsRead(List<Pid> list, List<Pid> list2) {
        }
    };
    private Queue<ChannelInfo> possibleChannels;
    private GroupScanner scanner;
    private PidSet scanningPids;
    private ObdService service;
    private long sleepDelay = SLEEP_DELAY;
    private boolean started;

    public enum AutoConnectState {
        DISCONNECTED,
        INITIAL_SCAN,
        FULL_SCAN,
        CONNECTING,
        CONNECTED,
        SLEEPING,
        STOPPING
    }

    public AutoConnect(ObdService service) {
        this.service = service;
        this.carState = new CarState();
        this.started = false;
    }

    public AutoConnectState getState() {
        return this.autoConnectState;
    }

    private void setState(AutoConnectState newState) {
        if (newState != this.autoConnectState) {
            Log.info("Switching to state:" + newState.name());
            this.autoConnectState = newState;
        }
    }

    public List<Pid> getReadings(List<Pid> pids) {
        List<Pid> readings;
        if (this.autoConnectState == AutoConnectState.CONNECTED && pidsChanged(pids)) {
            updateScan(pids);
        }
        synchronized (this.carState) {
            readings = this.carState.getReadings(pids);
        }
        return readings;
    }

    public void start() {
        if (!this.started) {
            this.started = true;
            this.service.addListener(this.obdListener);
            this.service.getHandler().post(this);
        }
    }

    public void stop() {
        if (this.started) {
            this.started = false;
            setState(AutoConnectState.STOPPING);
            this.service.removeListener(this.obdListener);
        }
    }

    private boolean pidsChanged(List<Pid> pids) {
        if (this.scanningPids == null) {
            return true;
        }
        for (Pid pid : pids) {
            if (!this.scanningPids.contains(pid)) {
                return true;
            }
        }
        return false;
    }

    private void updateScan(List<Pid> pids) {
        this.scanningPids = new PidSet(pids);
        Log.info("Starting to scan for " + pids.size() + " pids");
        this.service.addListener(pids, this.pidListener);
    }

    private LinkedList<ChannelInfo> prioritizeChannels(Set<ChannelInfo> channels) {
        ChannelInfo[] channelArray = new ChannelInfo[channels.size()];
        channels.toArray(channelArray);
        Arrays.sort(channelArray, new Comparator<ChannelInfo>() {
            public int compare(ChannelInfo lhs, ChannelInfo rhs) {
                int lhsType = lhs.getConnectionType().ordinal();
                int rhsType = rhs.getConnectionType().ordinal();
                if (lhsType < rhsType) {
                    return -1;
                }
                if (lhsType > rhsType) {
                    return 1;
                }
                return lhs.getAddress().compareTo(rhs.getAddress());
            }
        });
        LinkedList<ChannelInfo> result = new LinkedList<>();
        Collections.addAll(result, channelArray);
        return result;
    }

    public void run() {
        if (this.autoConnectState != AutoConnectState.STOPPING) {
            ChannelInfo info = null;
            Set<ChannelInfo> channels;
            switch (this.autoConnectState) {
                case DISCONNECTED:
                    Log.debug("AutoConnect: Checking for default channel");
                    this.scanningPids = null;
                    info = this.service.getDefaultChannelInfo();
                    if (info == null && this.possibleChannels != null) {
                        info = this.possibleChannels.poll();
                        if (info != null) {
                            Log.debug("Using next channel from scan");
                        }
                    }
                    if (info == null) {
                        Log.debug("No default found and exhausted previous scan - sleeping");
                        setState(AutoConnectState.SLEEPING);
                        break;
                    }
                    break;
                case SLEEPING:
                    setState(AutoConnectState.INITIAL_SCAN);
                    if (this.scanner == null) {
                        this.scanner = new GroupScanner(this.service, null);
                    }
                    this.scanner.startScan();
                    break;
                case INITIAL_SCAN:
                    channels = this.scanner.getDiscoveredChannels();
                    if (!channels.isEmpty()) {
                        this.scanner.stopScan();
                        this.possibleChannels = prioritizeChannels(channels);
                        info = this.possibleChannels.poll();
                        break;
                    }
                    Log.debug("AutoConnect: No channels found after initial scan, waiting for FULL_SCAN");
                    setState(AutoConnectState.FULL_SCAN);
                    break;
                case FULL_SCAN:
                    channels = this.scanner.getDiscoveredChannels();
                    this.scanner.stopScan();
                    if (!channels.isEmpty()) {
                        info = channels.iterator().next();
                        break;
                    }
                    Log.debug("AutoConnect: No channels found after full scan, restarting scan process");
                    setState(AutoConnectState.DISCONNECTED);
                    break;
            }
            if (info != null) {
                Log.debug("AutoConnect: found channel " + info.asString());
                setState(AutoConnectState.CONNECTING);
                this.scanner = null;
                this.service.openChannel(info);
            }
            this.service.getHandler().postDelayed(this, getDelay());
        }
    }

    private long getDelay() {
        long delay = RECONNECT_DELAY;
        switch (this.autoConnectState) {
            case SLEEPING:
                delay = this.service.reconnectImmediately() ? 500 : this.sleepDelay;
                break;
            case INITIAL_SCAN:
                delay = 100;
                break;
            case FULL_SCAN:
                delay = 4000;
                break;
        }
        if (this.service.reconnectImmediately()) {
            return 500;
        }
        return delay;
    }

    public void triggerReconnect() {
        this.service.getHandler().removeCallbacks(this);
        this.service.getHandler().post(this);
    }
}
