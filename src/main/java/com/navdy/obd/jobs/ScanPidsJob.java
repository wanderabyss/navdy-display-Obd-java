package com.navdy.obd.jobs;

import android.os.SystemClock;

import com.navdy.obd.ECU;
import com.navdy.obd.ObdJob;
import com.navdy.obd.Pid;
import com.navdy.obd.Profile;
import com.navdy.obd.Protocol;
import com.navdy.obd.ScanSchedule;
import com.navdy.obd.ScanSchedule.Scan;
import com.navdy.obd.VehicleInfo;
import com.navdy.obd.VehicleStateManager;
import com.navdy.obd.command.CANBusMonitoringCommand;
import com.navdy.obd.command.GatherCANBusDataCommand;
import com.navdy.obd.command.ICommand;
import com.navdy.obd.command.IObdDataObserver;
import com.navdy.obd.command.ObdCommand;
import com.navdy.obd.command.Sample;
import com.navdy.obd.command.ScheduledBatchCommand;
import com.navdy.obd.io.IChannel;
import com.navdy.util.RunningStats;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ScanPidsJob extends ObdJob {
    static final Logger Log = LoggerFactory.getLogger(ObdJob.class);
    private static final int MAX_SCAN_COUNT_WITH_NO_DATA = 5;
    public static final int MINIMUM_SAMPLES_REQUIRED = 10;
    static final ICommand NO_OP_COMMAND = new ICommand() {
        public String getName() {
            return "NO_OP";
        }

        public void execute(InputStream input, OutputStream output, Protocol protocol, IObdDataObserver commandObserver) {
        }

        public double getDoubleValue() {
            return -2.147483648E9d;
        }

        public int getIntValue() {
            return Integer.MIN_VALUE;
        }

        public String getResponse() {
            return "";
        }
    };
    public static final double SAMPLING_DATA_DELAY_THRESHOLD = 100.0d;
    private List<Pid> fullPids;
    private HashMap<Integer, Pid> fullPidsLookup = new HashMap();
    boolean isGatheringCanBusData = false;
    private List<Pid> sampledPids = new ArrayList<>();
    int scanCountWithNoData = 0;
    ScanSchedule schedule;
    private boolean started = false;
    private VehicleStateManager vehicleStateManager;

    public interface IListener extends com.navdy.obd.ObdJob.IListener {
        CANBusMonitoringCommand getCanBusMonitoringCommand();

        void onCanBusDataRead(ScanPidsJob scanPidsJob);

        void onCanBusMonitoringErrorDetected();
    }

    public ScanPidsJob(VehicleInfo vehicleInfo, Profile profile, ScanSchedule schedule, VehicleStateManager vehicleStateManager, IChannel channel, Protocol protocol, final IListener listener, IObdDataObserver obdDataObserver) {
        super(buildCommand(vehicleInfo, profile, schedule, listener.getCanBusMonitoringCommand()), channel, protocol, listener, obdDataObserver);
        if (listener.getCanBusMonitoringCommand() != null) {
            listener.getCanBusMonitoringCommand().setCANBusDataListener((pid, value) -> {
                Pid pidObject;
                if (ScanPidsJob.this.fullPidsLookup.containsKey(pid)) {
                    pidObject = ScanPidsJob.this.fullPidsLookup.get(pid);
                    pidObject.setTimeStamp(SystemClock.elapsedRealtime());
                    pidObject.setValue(value);
                    listener.onCanBusDataRead(ScanPidsJob.this);
                    return;
                }
                pidObject = new Pid(pid);
                ScanPidsJob.this.fullPids.add(pidObject);
                ScanPidsJob.this.fullPidsLookup.put(pid, pidObject);
                pidObject.setTimeStamp(SystemClock.elapsedRealtime());
                pidObject.setValue(value);
                listener.onCanBusDataRead(ScanPidsJob.this);
            });
        }
        this.fullPids = ((ScheduledBatchCommand) this.command).getPids();
        for (Pid pid : this.fullPids) {
            if (pid.getId() == 1000) {
                this.isGatheringCanBusData = true;
            }
            this.fullPidsLookup.put(pid.getId(), pid);
        }
        this.schedule = schedule;
        this.vehicleStateManager = vehicleStateManager;
    }

    protected void postExecute() throws IOException {
        if (!this.started) {
            this.started = true;
            this.vehicleStateManager.update(this.fullPids);
        }
        ScheduledBatchCommand batchCommand = (ScheduledBatchCommand) this.command;
        List<ICommand> commands = batchCommand.getCommands();
        this.sampledPids.clear();
        boolean hasData = false;
        int length = this.fullPids.size();
        for (int i = 0; i < length; i++) {
            Pid pid = this.fullPids.get(i);
            Sample sample = batchCommand.samples.get(i);
            if (sample.updated) {
                this.sampledPids.add(pid);
                double value = getValue(commands.get(i), pid);
                if (value != -2.147483648E9d) {
                    hasData = true;
                }
                long samplingTime = sample.getLastSamplingTime();
                boolean outlierSample = false;
                if (samplingTime > 0) {
                    RunningStats sampleStats = sample.samplingTimeStats;
                    sample.samplingTimeStats.add((double) samplingTime);
                    if (sampleStats.count() > 10 && ((double) samplingTime) >= sampleStats.mean() + SAMPLING_DATA_DELAY_THRESHOLD) {
                        this.commandObserver.onError("DELAYED," + samplingTime + "," + sampleStats.count() + "," + sampleStats.mean() + "," + sampleStats.standardDeviation());
                        outlierSample = true;
                    }
                }
                pid.setTimeStamp(sample.lastSampleTimestamp);
                if (!outlierSample) {
                    pid.setValue(value);
                }
            }
        }
        if (hasData) {
            this.scanCountWithNoData = 0;
        } else {
            this.scanCountWithNoData++;
        }
        if (this.scanCountWithNoData >= 5) {
            if (this.isGatheringCanBusData) {
                ((IListener) this.listener).onCanBusMonitoringErrorDetected();
            }
            throw new IOException("Not getting any data from Obd for any PIDS");
        }
        this.vehicleStateManager.onScanComplete();
    }

    public List<Pid> getSampledPids() {
        return this.sampledPids;
    }

    public List<Pid> getFullPidsList() {
        return this.fullPids;
    }

    private double getValue(ICommand command, Pid pid) {
        ObdCommand obdCommand = command instanceof ObdCommand ? (ObdCommand) command : null;
        if (pid.getId() != 47 || obdCommand == null || obdCommand.getResponseCount() <= 1) {
            return command.getDoubleValue();
        }
        int responses = obdCommand.getResponseCount();
        for (int i = 0; i < responses; i++) {
            obdCommand.setTargetEcu(obdCommand.getResponse(i).ecu);
            double val = obdCommand.getDoubleValue();
            if (val != -2.147483648E9d && val != 0.0d) {
                return val;
            }
        }
        return -2.147483648E9d;
    }

    private static ScheduledBatchCommand buildCommand(VehicleInfo vehicleInfo, Profile profile, ScanSchedule schedule, CANBusMonitoringCommand canBusMonitoringCommand) {
        int pidCount = schedule.size();
        List<Scan> scanList = schedule.getScanList();
        List<ICommand> commands = new ArrayList<>();
        for (int i = 0; i < pidCount; i++) {
            int pidId = scanList.get(i).pid;
            if (pidId == 1000) {
                Log.info("Added CAN bus gathering data command to the schedule");
                commands.add(GatherCANBusDataCommand.MONITOR_COMMAND);
            } else if (pidId == 1001) {
                Log.info("Added CAN bus monitoring data command to the schedule");
                canBusMonitoringCommand.reset();
                commands.add(canBusMonitoringCommand);
            } else {
                ObdCommand command = profile.lookup(pidId);
                if (command != null) {
                    int ecusSupportingPid = supportedEcuCount(vehicleInfo, pidId);
                    if (ecusSupportingPid > 1) {
                        command.setTargetEcu(bestEcu(vehicleInfo, pidId).address);
                        command.setExpectedResponses(ecusSupportingPid);
                        commands.add(command);
                    } else if (ecusSupportingPid == 1) {
                        command.setTargetEcu(-1);
                        command.setExpectedResponses(1);
                        commands.add(command);
                    } else {
                        Log.warn("Scanning for unsupported pid! " + pidId);
                        commands.add(NO_OP_COMMAND);
                    }
                } else {
                    commands.add(NO_OP_COMMAND);
                    Log.warn("Unable to find obd command to read , adding a NO op command" + pidId);
                }
            }
        }
        return new ScheduledBatchCommand(scanList, commands.toArray(new ICommand[pidCount]));
    }

    private static ECU bestEcu(VehicleInfo vehicleInfo, int pid) {
        int bestScore = 0;
        ECU bestEcu = null;
        for (ECU ecu : vehicleInfo.ecus) {
            int score = ecu.supportedPids.size();
            if (ecu.supportedPids.contains(pid) && score > bestScore) {
                bestEcu = ecu;
                bestScore = score;
            }
        }
        return bestEcu;
    }

    private static int supportedEcuCount(VehicleInfo vehicleInfo, int pid) {
        int count = 0;
        for (ECU ecu : vehicleInfo.ecus) {
            if (ecu.supportedPids.contains(pid)) {
                count++;
            }
        }
        return count;
    }
}
