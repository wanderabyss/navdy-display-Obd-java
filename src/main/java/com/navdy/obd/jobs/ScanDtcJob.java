package com.navdy.obd.jobs;

import com.navdy.obd.ObdJob;
import com.navdy.obd.Protocol;
import com.navdy.obd.VehicleInfo;
import com.navdy.obd.command.BatchCommand;
import com.navdy.obd.command.CheckDTCCommand;
import com.navdy.obd.command.ICommand;
import com.navdy.obd.command.IObdDataObserver;
import com.navdy.obd.command.ObdCommand;
import com.navdy.obd.command.ReadTroubleCodesCommand;
import com.navdy.obd.io.IChannel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ScanDtcJob extends ObdJob {
    static final Logger Log = LoggerFactory.getLogger(ObdJob.class);
    private static final int MAX_SCAN_COUNT_WITH_NO_DATA = 5;
    public static final int MINIMUM_SAMPLES_REQUIRED = 10;
    public static final double SAMPLING_DATA_DELAY_THRESHOLD = 100.0d;

    private boolean started = false;
    private VehicleInfo vehicleInfo;

    private CheckDTCCommand checkDtcCommand;
    private ReadTroubleCodesCommand activeTroubleCodesCommand;
    private ReadTroubleCodesCommand pendingTroubleCodesCommand;


    public ScanDtcJob(BatchCommand cmds, VehicleInfo vehicleInfo, IChannel channel, Protocol protocol, final IListener listener, IObdDataObserver obdDataObserver) {
        super(cmds, channel, protocol, listener, obdDataObserver);
        this.vehicleInfo = vehicleInfo;
        this.checkDtcCommand = new CheckDTCCommand(vehicleInfo.ecus);
        this.activeTroubleCodesCommand = new ReadTroubleCodesCommand(vehicleInfo.ecus, true);
        this.pendingTroubleCodesCommand = new ReadTroubleCodesCommand(vehicleInfo.ecus, false);
        cmds.add(this.checkDtcCommand);
        cmds.add(this.activeTroubleCodesCommand);
        cmds.add(this.pendingTroubleCodesCommand);
    }

    protected void postExecute() throws IOException {
        if (!this.started) {
            this.started = true;
        }

        List<String> troubleCodes = this.activeTroubleCodesCommand.getTroubleCodes();

        if (troubleCodes != null) {
            for (String troubleCode : troubleCodes) {
                Log.info("Active Code: " + troubleCode);
            }
        }

        List<String> pendingCodes = this.pendingTroubleCodesCommand.getTroubleCodes();

        if (pendingCodes != null) {
            for (String troubleCode : pendingCodes) {
                Log.info("Pending Code: " + troubleCode);
                if (troubleCodes != null) {
                    if (!troubleCodes.contains(troubleCode)) {
                        troubleCodes.add(troubleCode);
                    }
                }
            }
            if (troubleCodes == null) {
                troubleCodes = pendingCodes;
            }
        }

        this.vehicleInfo.troubleCodes = troubleCodes;
        
    }

    // private double getValue(ICommand command, Pid pid) {
    //     ObdCommand obdCommand = command instanceof ObdCommand ? (ObdCommand) command : null;
    //     if (pid.getId() != 47 || obdCommand == null || obdCommand.getResponseCount() <= 1) {
    //         return command.getDoubleValue();
    //     }
    //     int responses = obdCommand.getResponseCount();
    //     for (int i = 0; i < responses; i++) {
    //         obdCommand.setTargetEcu(obdCommand.getResponse(i).ecu);
    //         double val = obdCommand.getDoubleValue();
    //         if (val != -2.147483648E9d && val != 0.0d) {
    //             return val;
    //         }
    //     }
    //     return -2.147483648E9d;
    // }

    private static BatchCommand buildCommand(List<ObdCommand> commands, VehicleInfo vehicleInfo) {
        
        if (vehicleInfo.ecus != null && vehicleInfo.ecus.size() > 0) {
            for (int i = 0; i < commands.size(); i++) {
                commands.get(i).setExpectedResponses(vehicleInfo.ecus.size());
            }
        }

        return new BatchCommand((ICommand[]) commands.toArray(new ICommand[0]));
    }

    public static ScanDtcJob newScanDtcJob(VehicleInfo vehicleInfo, IChannel channel, Protocol protocol, final IListener listener, IObdDataObserver obdDataObserver) {

        BatchCommand cmds = new BatchCommand(new ArrayList<>());
        return new ScanDtcJob(cmds, vehicleInfo, channel, protocol, listener, obdDataObserver);
    }
};


