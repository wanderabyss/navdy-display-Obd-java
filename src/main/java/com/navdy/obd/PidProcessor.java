package com.navdy.obd;

public abstract class PidProcessor {
    public abstract boolean isSupported(PidSet pidSet);

    public abstract boolean processPidValue(PidLookupTable pidLookupTable);

    public abstract PidSet resolveDependencies();
}
