package com.navdy.obd;


import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.RemoteException;
import android.os.SystemClock;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.Log;
import android.util.SparseArray;

import com.navdy.obd.IObdService.Stub;
import com.navdy.obd.ObdJob.IListener;
import com.navdy.obd.ScanSchedule.Scan;
import com.navdy.obd.app.R;
import com.navdy.obd.command.BaseSTNInitializeCommand;
import com.navdy.obd.command.BatchCommand;
import com.navdy.obd.command.CANBusMonitoringCommand;
import com.navdy.obd.command.CheckDTCCommand;
import com.navdy.obd.command.CustomInitializeCommand;
import com.navdy.obd.command.GatherCANBusDataCommand;
import com.navdy.obd.command.ICommand;
import com.navdy.obd.command.InitializeCommand;
import com.navdy.obd.command.InitializeJ1939Command;
import com.navdy.obd.command.ObdCommand;
import com.navdy.obd.command.ReadTroubleCodesCommand;
import com.navdy.obd.command.ReadVoltageCommand;
import com.navdy.obd.command.ValidPidsCommand;
import com.navdy.obd.command.VinCommand;
import com.navdy.obd.command.WriteConfigurationCommand;
import com.navdy.obd.io.BluetoothChannel;
import com.navdy.obd.io.ChannelInfo;
import com.navdy.obd.io.ChannelInfo.ConnectionType;
import com.navdy.obd.io.IChannel;
import com.navdy.obd.io.IChannelSink;
import com.navdy.obd.io.STNSerialChannel;
import com.navdy.obd.j1939.J1939ObdJobAdapter;
import com.navdy.obd.j1939.J1939ObdJobAdapter.J1939ObdJobListener;
import com.navdy.obd.j1939.J1939Profile;
import com.navdy.obd.jobs.AutoConnect;
import com.navdy.obd.jobs.ScanDtcJob;
import com.navdy.obd.jobs.ScanPidsJob;
import com.navdy.obd.simulator.MockObdChannel;
import com.navdy.obd.update.DefaultFirmwareManager;
import com.navdy.obd.update.ObdDeviceFirmwareManager;
import com.navdy.obd.update.STNSerialDeviceFirmwareManager;
import com.navdy.obd.update.Update;
import com.navdy.os.SystemProperties;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;


public class ObdService extends Service implements IChannelSink {
    private static final String BAUD_RATE_PROP = "persist.sys.obd.baudrate";
    public static final int BAUD_RATE = SystemProperties.getInt(BAUD_RATE_PROP, 460800);
    public static final String CAN_BUS_LOGGER = "com.navdy.obd.CanBusRaw";
    private static final String DEFAULT_MODE_PROP = "persist.sys.obd.mode";
    private static final int DEFAULT_MODE = SystemProperties.getInt(DEFAULT_MODE_PROP, 0);
    public static final boolean CAN_BUS_MONITORING_ENABLED = false;

    private static final int BATTERY_CHECK_INTERVAL = 2000;
    public static final int CAN_BUS_SAMPLING_INTERVAL = 1000;
    public static final int DEFAULT_SCAN_INTERVAL = 500;
    public static final int GATHER_CAN_BUS_DATA_INTERVAL = 1000;
    public static final int MONITOR_CAN_BUS_BATCH_INTERVAL = 30000;
    public static final int MONITOR_CAN_BUS_BATCH_SIZE = 5;

    public static final int MESSAGE_CHANGE_MODE = 9;
    public static final int MESSAGE_CHARGING = 4;
    public static final int MESSAGE_DISABLE_OBD_PID_SCAN = 5;
    public static final int MESSAGE_ENABLE_OBD_PID_SCAN = 6;
    public static final int MESSAGE_RESCAN = 3;
    public static final int MESSAGE_RESET_CHANNEL = 7;
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_TOAST = 2;
    public static final int MESSAGE_UPDATE_FIRMWARE = 8;

    public static final int MODE_ONE_READ_RESPONSE = 65;
    public static final String OBD_DEBUG_MODE = "persist.sys.obd.debug";
    public static final String OVERRIDE_PROTOCOL = "persist.sys.obd.protocol";
    public static final String PREFS_FILE_OBD_DEVICE = "ObdDevice";
    public static final String PREFS_KEY_DEFAULT_CHANNEL_INFO = "DefaultChannelInfo";
    public static final String PREFS_KEY_LAST_PROTOCOL = "Protocol";
    public static final String PREFS_KEY_OBD_SCAN_ENABLED = "ObdCommunicationEnabled";
    public static final String PREFS_KEY_OBD_SCAN_MODE = "ObdScanMode";
    public static final String PREFS_LAST_CONNECTED_BAUD_RATE = "last_connected_baud_rate";
    private static long RECONNECT_DELAY = TimeUnit.SECONDS.toMillis(30);
    private static final int SCAN_INTERVAL = 10;
    private static final String TAG = ObdService.class.getSimpleName();
    public static final String TOAST = "toast";
    public static final String VOLTAGE_FREQUENCY_PROPERTY = "persist.sys.obd.volt.freq";
    public static final int VOLTAGE_REPORT_INTERVAL = (SystemProperties.getInt(VOLTAGE_FREQUENCY_PROPERTY, 30) * 1000);
    public static final int VOLTAGE_THRESHOLD_MINIMUM_TIME_IN_SECONDS = 10;
    private static ObdService obdService;

    private Stub apiEndpoint = new Stub() {
        public void connect(String address, IObdServiceListener listener) {
            synchronized (ObdService.this.listeners) {
                ObdService.this.listeners.add(listener);
            }
            ChannelInfo info = ChannelInfo.parse(address);
            if (info != null) {
                Log.d(ObdService.TAG, "Attempting to connect to " + address);
                ObdService.this.openChannel(info);
            }
        }

        public void disconnect() {
            if (ObdService.this.channel != null) {
                ObdService.this.channel.disconnect();
            }
        }

        public int getState() {
            return ObdService.this.connectionState;
        }

        public String getFirmwareVersion() {
            try {
                return ObdService.this.currentFirmwareVersion;
            } catch (Throwable th) {
                return null;
            }
        }

        public boolean updateDeviceFirmware(String version, String updateFilePath) {
            try {
                if (ObdService.this.firmwareManager == null || ObdService.this.firmwareManager.isUpdatingFirmware()) {
                    if (ObdService.this.firmwareManager != null) {
                        Log.d(ObdService.TAG, "FirmwareManager is busy installing update already");
                    }
                    return true;
                }
                final Update update = new Update();
                update.mVersion = version;
                update.mUpdateFilePath = updateFilePath;
                Runnable updateJob = () -> {
                    Log.d(ObdService.TAG, "Update success : " + ObdService.this.firmwareManager.updateFirmware(update));
                    ObdService.this.reconnectImmediately = false;
                    ObdService.this.firmwareUpdateVersion = "";
                    ObdService.this.firmwareUpdatePath = "";
                    ObdService.this.forceReconnect();
                };
                ObdService.this.cancelCurrentSchedule();
                ObdService.this.cancelVoltageMonitoring();
                ObdService.this.cancelDtcMonitoring();
                ObdService.this.scheduler.submit(updateJob);
                return true;
            } catch (Throwable t) {
                Log.e(ObdService.TAG, "Exception while updating the firmware :" + t);
            }
            return true;
        }

        public void reset() {
            ObdService.this.serviceHandler.sendEmptyMessage(7);
        }

        public void scanSupportedPids() {
            ObdService.this.scanSupportedPids();
        }

        public void scanVIN() {
            ObdService.this.scanVIN();
        }

        public String readPid(int pid) {
            try {
                ObdService.this.synchronousCommand(new ObdCommand(String.format("01%02x1", pid)));
                return null;
            } catch (ExecutionException e) {
                return null;
            }
        }

        public void scanPids(List<Pid> pids, int intervalMs) {
            ObdService.this.scanPids(pids, intervalMs);
        }

        public void startRawScan() {
            ObdService.this.cancelCurrentSchedule();
            ObdService.this.cancelVoltageMonitoring();
            ObdService.this.cancelDtcMonitoring();
            ObdService.this.currentScan = new ObdScanJob(ObdService.this.channel, ObdService.this.protocol, data -> {
                synchronized (ObdService.this.listeners) {
                    for (IObdServiceListener listener : ObdService.this.listeners) {
                        try {
                            listener.onRawData(data);
                        } catch (RemoteException e) {
                            Log.e(ObdService.TAG, "Error notifying listener", e);
                        }
                    }
                }
            }, ObdService.this.obdDataObserver);
            ObdService.this.scheduler.submit(ObdService.this.currentScan);
        }

        public void addListener(IObdServiceListener listener) {
            ObdService.this.addListener(listener);
        }

        public void removeListener(IObdServiceListener listener) {
            ObdService.this.removeListener(listener);
        }
    };
    private AutoConnect autoConnect;
    private ICanBusMonitoringListener canBusMonitoringListener;
    private CANBusMonitoringStatus canBusMonitoringStatus = new CANBusMonitoringStatus();
    private ICarService.Stub carApiEndpoint = new ICarService.Stub() {
        public int getConnectionState() {
            return ObdService.this.connectionState;
        }

        public List<Pid> getSupportedPids() {
            if (ObdService.this.currentMode == 0) {
                if (ObdService.this.vehicleInfo != null) {
                    return ObdService.this.vehicleInfo.getPrimaryEcu().supportedPids.asList();
                }
            } else if (ObdService.this.currentMode == 1 && getConnectionState() == 2) {
                return J1939ObdJobAdapter.J1939SupportedObdPids.asList();
            }
            return null;
        }

        public List<ECU> getEcus() {
            if (ObdService.this.vehicleInfo != null) {
                return ObdService.this.vehicleInfo.getEcus();
            }
            return null;
        }

        public String getProtocol() {
            return ObdService.this.vehicleInfo.protocol;
        }

        public String getVIN() {
            if (ObdService.this.vehicleInfo != null) {
                return ObdService.this.vehicleInfo.vin;
            }
            return null;
        }

        public List<Pid> getReadings(List<Pid> pids) {
            if (ObdService.this.autoConnect == null) {
                return null;
            }
            return ObdService.this.autoConnect.getReadings(pids);
        }

        public void addListener(List<Pid> pids, IPidListener listener) {
            ObdService.this.addListener(pids, listener);
        }

        public void removeListener(IPidListener listener) {
            ObdService.this.removeListener(listener);
        }

        public String getCurrentConfigurationName() {
            return ObdService.this.configurationName;
        }

        public boolean applyConfiguration(String configuration) {
            try {
                ObdService.this.applyConfigurationInternal(configuration);
                ObdService.this.updateScan();
                return true;
            } catch (IllegalArgumentException t) {
                ObdService.this.updateScan();
                throw t;
            } catch (Throwable th) {
                ObdService.this.applyFallbackConfiguration();
                return false;
            }
        }

        public void updateScan(ScanSchedule schedule, IPidListener listener) {
            ObdService.this.addListener(schedule, listener);
        }

        public void rescan() {
            ObdService.this.serviceHandler.sendEmptyMessage(3);
        }

        public double getBatteryVoltage() {
            return ObdService.this.currentBatteryVoltage;
        }

        public void setObdPidsScanningEnabled(boolean enable) {
            if (enable == ObdService.this.isScanningEnabled) {
                return;
            }
            if (enable) {
                ObdService.this.serviceHandler.sendEmptyMessage(6);
            } else {
                ObdService.this.serviceHandler.sendEmptyMessage(5);
            }
        }

        public boolean isObdPidsScanningEnabled() {
            return ObdService.this.isScanningEnabled;
        }

        public void sleep(boolean deep) {
            Log.d(ObdService.TAG, "sleep API is invoked");
            ObdService.this.deepSleep = deep;
            ObdService.this.postStateChange(5);
        }

        public void wakeup() {
            if (ObdService.this.connectionState == 5) {
                ObdService.this.postStateChange(4);
            }
        }

        public void setVoltageSettings(VoltageSettings settings) {
            ObdService.this.setVoltageSettings(settings);
        }

        public String getObdChipFirmwareVersion() {
            return ObdService.this.currentFirmwareVersion;
        }

        public void updateFirmware(String version, String updateFilePath) {
            Log.d(ObdService.TAG, "updateFirmware , version :" + version + " , Path : " + updateFilePath);
            ObdService.this.firmwareUpdateVersion = version;
            ObdService.this.firmwareUpdatePath = updateFilePath;
            ObdService.this.getHandler().sendEmptyMessage(8);
        }

        public int getMode() {
            return ObdService.this.currentMode;
        }

        public void setMode(int mode, boolean persistent) {
            Log.d(ObdService.TAG, "setMode : " + mode + ", Persistent : " + persistent);
            if (persistent) {
                Log.d(ObdService.TAG, "Saving the mode");
                ObdService.this.mSharedPrefs.edit().putInt(ObdService.PREFS_KEY_OBD_SCAN_MODE, mode).apply();
            }
            ObdService.this.getHandler().obtainMessage(9, mode, -1).sendToTarget();
        }

        public void setCANBusMonitoringListener(ICanBusMonitoringListener listener) {
            ObdService.this.canBusMonitoringListener = listener;
        }

        public void startCanBusMonitoring() {
            ObdService.this.gatherCanBusDataToLogs = true;
            ObdService.this.updateScan();
        }

        public void stopCanBusMonitoring() {
            ObdService.this.gatherCanBusDataToLogs = false;
            ObdService.this.canBusMonitoringListener = null;
            ObdService.this.updateScan();
        }

        public boolean isCheckEngineLightOn() {
            if (ObdService.this.vehicleInfo != null) {
                return ObdService.this.vehicleInfo.isCheckEngineLightOn;
            }
            return false;
        }

        public List<String> getTroubleCodes() {
            if (ObdService.this.vehicleInfo != null) {
                return ObdService.this.vehicleInfo.troubleCodes;
            }
            return null;
        }
    };
    private IChannel channel;
    private ChannelInfo channelInfo;
    private String configurationName;
    private int connectionState = 0;
    private double currentBatteryVoltage = -1.0d;
    private String currentFirmwareVersion = null;
    private int currentMode = -1;
    private ObdScanJob currentScan;
    private ScheduledFuture currentSchedule;
    public static boolean debugMode;
    private boolean deepSleep = false;
    private boolean clear0401 = false;
    private String dtcClearCommand = "04";
    private String[] dtcClearCommands = null;
    private ObdDeviceFirmwareManager firmwareManager;
    private String firmwareUpdatePath;
    private String firmwareUpdateVersion;
    private boolean gatherCanBusDataToLogs = false;
    private volatile boolean isScanningEnabled = true;
    private J1939Profile j1939Profile;
    private volatile int lastConnectedBaudRate = 0;
    private long lastDisconnectTime = 0;
    private long lastMonitorBatchCompletedTime = 0;
    private long lastVoltageReport;
    private final List<IObdServiceListener> listeners = new ArrayList<>();
    protected SharedPreferences mSharedPrefs;
    private VehicleStateManager mVehicleStateManager;
    private int monitorCounter = 0;
    public boolean monitoringCanBusLimitReached = false;
    private ObdDataObserver obdDataObserver;
    private boolean onMonitoringStatusReported = false;
    private final ArrayMap<IBinder, PidListener> pidListeners = new ArrayMap<>();
    private YamlProfile profile;
    private Protocol protocol;
    private boolean reconnectImmediately = false;
    private final ScheduledExecutorService scheduler = Executors.newSingleThreadScheduledExecutor();
    private ServiceHandler serviceHandler;
    private Looper serviceLooper;
    private BatchCommand sleepWithWakeUpTriggersCommand;
    private VehicleInfo vehicleInfo;
    private ScheduledFuture voltageMonitoring;
    private ScheduledFuture celMonitoring;
    private ScheduledFuture troubleMonitoring;
    private ScheduledFuture pendingMonitoring;
    private VoltageSettings voltageSettings;
    private final Object voltageSettingsLock = new Object();

    private boolean remoteClientConnected = false;

    enum CANBusMonitoringState {
        UNKNOWN,
        UNAVAILABLE,
        SAMPLING,
        MONITORING
    }

    static class CANBusMonitoringStatus {
        CANBusMonitoringCommand canBusMonitoringCommand;
        CANBusMonitoringState state = CANBusMonitoringState.UNKNOWN;

        CANBusMonitoringStatus() {
        }
    }

    private final class ServiceHandler extends Handler {
        private ServiceHandler(Looper looper) {
            super(looper);
        }

        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    ObdService.this.handleStateChange(msg.arg1);
                    break;
                case 2:
                    Log.i(ObdService.TAG, "MESSAGE_TOAST: " + msg.arg1);
                    for (IObdServiceListener listener : ObdService.this.listeners) {
                        try {
                            listener.onStatusMessage(msg.getData().getString("toast"));
                        } catch (RemoteException e) {
                            Log.e(ObdService.TAG, "Failed to notify listener of statusMessage");
                        }
                    }
                    break;
                case 3:
                case 4:
                    if (ObdService.this.connectionState == 4 && ObdService.this.isScanningEnabled) {
                        Log.i(ObdService.TAG, "Rescanning for ECUs");
                        ObdService.this.handleStateChange(3);
                    }
                    break;
                case 5:
                    Log.i(ObdService.TAG, "Disabling PIDs scan");
                    ObdService.this.mSharedPrefs.edit().putBoolean(ObdService.PREFS_KEY_OBD_SCAN_ENABLED, false).apply();
                    ObdService.this.isScanningEnabled = false;
                    if (ObdService.this.connectionState == 2) {
                        ObdService.this.cancelCurrentSchedule();
                        ObdService.this.handleStateChange(4);
                        return;
                    }
                    break;
                case 6:
                    Log.i(ObdService.TAG, "Enabling PIDs scan");
                    ObdService.this.mSharedPrefs.edit().putBoolean(ObdService.PREFS_KEY_OBD_SCAN_ENABLED, true).apply();
                    ObdService.this.isScanningEnabled = true;
                    break;
                case 7:
                    ObdService.this.resetChannel();
                    break;
                case 8:
                    ObdService.this.cancelCurrentSchedule();
                    ObdService.this.reconnectImmediately = true;
                    ObdService.this.forceReconnect();
                    break;
                case 9:
                    ObdService.this.cancelCurrentSchedule();
                    ObdService.this.setMode(msg.arg1);
                    ObdService.this.forceReconnect();
                    break;
                default:
            }
        }
    }

    public ObdService() {
        setVoltageSettings(new VoltageSettings());

        startRemoteClientServer();
    }

    private void startRemoteClientServer() {
        Runnable serverTask = new Runnable() {
            @Override
            public void run() {
                try {
                    ServerSocket serverSocket = new ServerSocket(6500);
                    Log.v(TAG, "Started RemoteClientServer on port 6500");
                    while (true) {
                        final Socket tcpSocket = serverSocket.accept();
                        Log.v(TAG, "Remote Client Connected, blocking local communication");
                        ObdService.this.remoteClientConnected = true;

                        // Run the forwarding task on the main scheduler to block other operations.
                        ObdService.this.scheduler.submit(new Runnable() {
                            public void run() {
                                try {
                                    InputStream tcpIn;
                                    OutputStream tcpOut;
                                    InputStream obdIn;
                                    OutputStream obdOut;
                                    try {
                                        // Turn on keep-alive for both the sockets
//                                        clientSocket.setKeepAlive(true);

                                        // Obtain client & server input & output streams
                                        tcpIn = tcpSocket.getInputStream();
                                        tcpOut = tcpSocket.getOutputStream();

                                        obdIn = ObdService.this.channel.getInputStream();
                                        obdOut = ObdService.this.channel.getOutputStream();

                                    } catch (IOException ioe) {
                                        Log.e(TAG, "Failed to connect RemoteClientServer", ioe);
                                        return;
                                    }

                                    // Start forwarding data between server and client
                                    ForwardThread tcpForward = new ForwardThread(tcpIn, obdOut, ">");
                                    ForwardThread obdForward = new ForwardThread(obdIn, tcpOut, "<");
                                    tcpForward.start();
                                    obdForward.start();
                                    try {
                                        tcpForward.join();
                                        obdForward.close();
                                        obdForward.join();

                                    } catch (InterruptedException ce) {
                                        Log.e(TAG, "Remote Client Interrupted", ce);

                                    }

                                    try {
                                        tcpIn.close();
                                        tcpOut.close();
                                        obdIn.close();
                                        obdOut.close();
                                        tcpSocket.close();

                                    } catch (IOException ce) {
                                        Log.e(TAG, "Failed to close connection cleanly", ce);
                                    }
                                } finally {
                                    ObdService.this.remoteClientConnected = false;
                                    Log.v(TAG, "Remote Client Disconnected, resuming local communication");
                                }
                            }

                        });
                    }
                } catch (IOException e) {
                    Log.e(TAG, "Failed to start RemoteClientServer", e);
                } finally {
                    ObdService.this.remoteClientConnected = false;
                }
            }
        };
        Thread serverThread = new Thread(serverTask);
        serverThread.start();
    }

    class ForwardThread extends Thread {
        private static final int BUFFER_SIZE = 64;

        InputStream mInputStream;
        OutputStream mOutputStream;
        boolean closed = false;
        String dirMarker;

        public ForwardThread(InputStream aInputStream, OutputStream aOutputStream, String dirMarker) {
            mInputStream = aInputStream;
            mOutputStream = aOutputStream;
            this.dirMarker = dirMarker;
        }

        public void close() {
            closed = true;
            try {
                this.mInputStream.close();
                this.mOutputStream.close();
            } catch (IOException e) {
                // Read/write failed --> connection is broken
            }
        }

        public void run() {
            byte[] buffer = new byte[BUFFER_SIZE];
            try {
                while (!closed) {
                    int bytesRead = mInputStream.read(buffer);
                    if (bytesRead == -1)
                        break; // End of stream is reached --> exit
                    if (bytesRead > 0) {
                        Log.v(ObdService.TAG, "Remote " + this.dirMarker + " " + new String(Arrays.copyOfRange(buffer, 0, bytesRead), "ASCII"));
                        mOutputStream.write(buffer, 0, bytesRead);
                        mOutputStream.flush();
                    }
                }
            } catch (IOException e) {
                // Read/write failed --> connection is broken
            }

            // Notify parent thread that the connection is broken
            ObdService.this.remoteClientConnected = false;
        }
    }

        private void setVoltageSettings(VoltageSettings settings) {
        if (settings == null) {
            Log.e(TAG, "Invalid voltage settings");
            return;
        }
        Log.d(TAG, "Setting voltage settings to " + settings);
        synchronized (this.voltageSettingsLock) {
            this.voltageSettings = settings;
        }
    }

    private void setMode(int mode) {
        if (mode != this.currentMode) {
            this.currentMode = mode;
            Log.d(TAG, "currentMode " + (this.currentMode == 0 ? "OBD2" : "J1939"));
            if (this.currentMode == 1 && this.j1939Profile == null) {
                this.j1939Profile = new J1939Profile();
                this.j1939Profile.load(getResources().openRawResource(R.raw.j1939_profile));
            }
        }
    }

    public boolean reconnectImmediately() {
        return this.reconnectImmediately;
    }

    public void forceReconnect() {
        if (this.channel != null) {
            Log.d(TAG, "Disconnecting channel as its connected");
            this.channel.disconnect();
            return;
        }
        Log.d(TAG, "Triggering reconnect, current state : " + this.connectionState);
        this.autoConnect.triggerReconnect();
    }

    private void handleStateChange(int state) {
        if (state != this.connectionState) {
            int i;
            this.connectionState = state;
            Log.i(TAG, "Switching to state:" + state);
            switch (this.connectionState) {
                case 0:
                    cancelCurrentSchedule();
                    cancelVoltageMonitoring();
                    cancelDtcMonitoring();
                    closeChannel();
                    this.lastDisconnectTime = SystemClock.elapsedRealtime();
                    break;
                case 2:
                    if (!this.isScanningEnabled) {
                        Log.d(TAG, "handleStateChange : New State is 'connected' but scanning is not enabled , moving back to 'idle' state");
                        postStateChange(4);
                        break;
                    }
                    updateScan();
                    monitorCheckEngineLight();
                    // monitorTroubleCodes();
                    monitorPendingCodes();
                    break;
                case 3:
                    resetChannel();
                    break;
                case 4:
                    if (!this.reconnectImmediately) {
                        resetObdChip();
                        monitorBatteryVoltage();
                        break;
                    }
                    try {
                        this.apiEndpoint.updateDeviceFirmware(this.firmwareUpdateVersion, this.firmwareUpdatePath);
                    } catch (RemoteException e) {
                        Log.d(TAG, "Error updating firmware ", e);
                    }
                    this.reconnectImmediately = false;
                    break;
                case 5:
                    sleepWithVoltageWakeUpTriggers();
                    break;
            }
            synchronized (this.listeners) {
                for (i = this.listeners.size() - 1; i >= 0; i--) {
                    try {
                        this.listeners.get(i).onConnectionStateChange(state);
                    } catch (DeadObjectException e2) {
                        Log.e(TAG, "Removing dead obd service listener");
                        this.listeners.remove(i);
                    } catch (RemoteException e3) {
                        Log.e(TAG, "Failed to notify listener of connection state change");
                    }
                }
            }
            synchronized (this.pidListeners) {
                for (i = this.pidListeners.size() - 1; i >= 0; i--) {
                    try {
                        this.pidListeners.valueAt(i).listener.onConnectionStateChange(state);
                    } catch (DeadObjectException e4) {
                        Log.e(TAG, "Removing dead pid listener");
                        this.pidListeners.removeAt(i);
                    } catch (RemoteException e5) {
                        Log.e(TAG, "Failed to notify listener of connection state change");
                    }
                }
            }
        }
    }

    public void onCreate() {
        super.onCreate();
        Log.i(TAG, "Creating service");
        obdService = this;
        this.profile = new YamlProfile();

        File file = new File("/maps/obd_profile.yml");
        if((! file.exists()) || (file.length() == 0)) {
            Log.i(TAG, "Copying default_profile.yml to /maps/obd_profile.yml");
            // Copy default from resources to file on disk
            try {
                InputStream inputStream = getResources().openRawResource(R.raw.default_profile);
                FileOutputStream out = new FileOutputStream(file);
                byte[] buf = new byte[1024];
                int len;
                while((len=inputStream.read(buf))>0){
                    out.write(buf,0,len);
                }
                out.close();
                inputStream.close();
            } catch (Exception e) {
                e.printStackTrace();
                Log.e(TAG, "Exception " + e);
            }
        }
        try{
            FileInputStream fileInputStream = new FileInputStream(file);
            this.profile.load(fileInputStream);  //closes inputstream
        } catch (IOException e) {
            Log.e(TAG, "Exception " + e);
            // If loading from file fails, load from resources instead
            this.profile.load(getResources().openRawResource(R.raw.default_profile));
        }
        this.mSharedPrefs = getSharedPreferences(PREFS_FILE_OBD_DEVICE, 0);
        debugMode = SystemProperties.getBoolean(OBD_DEBUG_MODE, false);
        this.lastConnectedBaudRate = this.mSharedPrefs.getInt(PREFS_LAST_CONNECTED_BAUD_RATE, BAUD_RATE);
        this.isScanningEnabled = this.mSharedPrefs.getBoolean(PREFS_KEY_OBD_SCAN_ENABLED, true);
        this.clear0401 = SystemProperties.getBoolean("persist.sys.obd.clear0401", false);
        this.dtcClearCommand = SystemProperties.get("persist.sys.obd.clearCommand", this.dtcClearCommand);
        this.dtcClearCommands = this.dtcClearCommand.split(";");  
        setMode(this.mSharedPrefs.getInt(PREFS_KEY_OBD_SCAN_MODE, DEFAULT_MODE));
        this.obdDataObserver = new ObdDataObserver();
        HandlerThread thread = new HandlerThread("ObdServiceHandlerThread");
        thread.start();
        this.serviceLooper = thread.getLooper();
        this.serviceHandler = new ServiceHandler(this.serviceLooper);
    }

    public IBinder onBind(Intent intent) {
        Log.d(TAG, "onBind - " + intent.getAction());
        if (IObdService.class.getName().equals(intent.getAction())) {
            Log.d(TAG, "returning IObdService API endpoint");
            return this.apiEndpoint;
        } else if (!ICarService.class.getName().equals(intent.getAction())) {
            return null;
        } else {
            Log.d(TAG, "returning ICarService API endpoint");
            return this.carApiEndpoint;
        }
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null) {
            String action = intent.getAction();
            if (ObdServiceInterface.ACTION_START_AUTO_CONNECT.equals(action)) {
                if (this.autoConnect == null) {
                    this.autoConnect = new AutoConnect(this);
                    this.autoConnect.start();
                }
            } else if (ObdServiceInterface.ACTION_STOP_AUTO_CONNECT.equals(action)) {
                if (this.autoConnect != null) {
                    this.autoConnect.stop();
                }
                this.autoConnect = null;
            } else if (ObdServiceInterface.ACTION_RESCAN.equals(action)) {
                this.serviceHandler.sendEmptyMessage(3);
            }
        }
        return Service.START_STICKY;
    }

    public void onDestroy() {
        super.onDestroy();
        if (this.autoConnect != null) {
            this.autoConnect.stop();
        }
        cancelCurrentSchedule();
        cancelVoltageMonitoring();
        cancelDtcMonitoring();
        this.serviceLooper.quit();
        Log.i(TAG, "Destroying service");
    }

    private void applyFallbackConfiguration() {
        InputStream is = getResources().openRawResource(R.raw.default_stn_configuration);
        String configuration = Utility.convertInputStreamToString(is);
        try {
            is.close();
        } catch (IOException e) {
            Log.e(TAG, "Error closing the input stream while reading from asset file ", e);
        }
        try {
            applyConfigurationInternal(configuration);
        } catch (Throwable t) {
            Log.e(TAG, "Critical error, failed to apply fallback configuration", t);
        }
    }

    private void applyConfigurationInternal(String configuration) throws IllegalArgumentException, ExecutionException {
        Log.d(TAG, "Applying device configuration");
        Log.d(TAG, "Executing the write configuration command");
        synchronousCommand(new WriteConfigurationCommand(configuration));
        resetChannel();
    }

    public Handler getHandler() {
        return this.serviceHandler;
    }

    public ChannelInfo getDefaultChannelInfo() {
        ChannelInfo info = null;
        try {
            info = ChannelInfo.parse(this.mSharedPrefs.getString(PREFS_KEY_DEFAULT_CHANNEL_INFO, null));
        } catch (Exception e) {
            Log.e(TAG, "Exception " + e);
        }
        return info;
    }

    public String sendCommand(String command) {
        try {
            return synchronousCommand(new ObdCommand(command));
        } catch (ExecutionException e) {
            Log.e(TAG, "Exception " + e);
            return null;
        }
    }

    public void setDefaultChannelInfo(ChannelInfo info) {
        try {
            this.mSharedPrefs.edit().putString(PREFS_KEY_DEFAULT_CHANNEL_INFO, info.asString()).apply();
        } catch (Exception e) {
            Log.e(TAG, "Exception " + e);
        }
    }

    private void cancelVoltageMonitoring() {
        if (this.voltageMonitoring != null) {
            this.voltageMonitoring.cancel(false);
            this.voltageMonitoring = null;
        }
        this.currentBatteryVoltage = -1.0d;
    }

    private void cancelDtcMonitoring() {
        if (this.celMonitoring != null) {
            this.celMonitoring.cancel(false);
            this.celMonitoring = null;
        }
        if (this.troubleMonitoring != null) {
            this.troubleMonitoring.cancel(false);
            this.troubleMonitoring = null;
        }
        if (this.pendingMonitoring != null) {
            this.pendingMonitoring.cancel(false);
            this.pendingMonitoring = null;
        }
    }

    private void cancelCurrentSchedule() {
        boolean z = true;
        if (this.currentSchedule != null) {
            ScheduledFuture scheduledFuture = this.currentSchedule;
            if (this.currentMode != 1) {
                z = false;
            }
            scheduledFuture.cancel(z);
            this.currentSchedule = null;
        }
        if (this.currentScan != null) {
            this.currentScan.cancel();
            this.currentScan = null;
        }
    }

    private String synchronousCommand(ICommand command) throws ExecutionException {
        if (this.remoteClientConnected) {
            return null;
        }
        cancelCurrentSchedule();
        IChannel obdChannel = this.channel;
        if (obdChannel == null) {
            Log.e(TAG, "Invalid channel - unable to execute command");
            return null;
        }
        try {
            this.scheduler.submit(new ObdJob(command, obdChannel, this.protocol, new IListener() {
                public void onComplete(ObdJob job, boolean success) {
                    if (success) {
                        ICommand command = job.command;
                        String response = command.getResponse();
                        Log.d(ObdService.TAG, "Response of the Synchronous command " + (response != null ? response.trim() : "EMPTY") + ", Command : " + command);
                    }
                }
            }, this.obdDataObserver)).get();
            return command.getResponse();
        } catch (InterruptedException e) {
            Log.e(TAG, "Interrupted while executing command", e);
            return e.getMessage();
        } catch (ExecutionException e2) {
            Log.e(TAG, "Error executing command", e2);
            throw e2;
        }
    }

    public void addListener(List<Pid> pids, IPidListener listener) {
        ScanSchedule schedule = new ScanSchedule();
        schedule.addPids(pids, 500);
        addListener(schedule, listener);
    }

    public void addListener(ScanSchedule schedule, IPidListener listener) {
        synchronized (this.pidListeners) {
            Log.i(TAG, "adding listener " + listener + " ibinder:" + listener.asBinder());
            this.pidListeners.put(listener.asBinder(), new PidListener(listener, schedule));
        }
        updateScan();
    }

    public void removeListener(IPidListener listener) {
        synchronized (this.pidListeners) {
            Log.i(TAG, "removing listener " + listener + " binder:" + listener.asBinder());
            this.pidListeners.remove(listener.asBinder());
        }
        updateScan();
    }

    private void updateScan() {
        Log.e(TAG, "updateScan");
        this.monitoringCanBusLimitReached = false;
        if (this.connectionState != 2) {
            Log.i(TAG, "ignoring scan update - not connected");
        } else if (this.isScanningEnabled) {
            ScanSchedule mergedSchedule = new ScanSchedule();
            long now = SystemClock.elapsedRealtime();
            if (this.canBusMonitoringStatus.state == CANBusMonitoringState.SAMPLING || this.canBusMonitoringStatus.state == CANBusMonitoringState.MONITORING) {
                this.gatherCanBusDataToLogs = false;
                mergedSchedule.addPid(1001, 1000);
            }
            if (this.canBusMonitoringStatus.state == CANBusMonitoringState.MONITORING) {
                List<Integer> pids = this.canBusMonitoringStatus.canBusMonitoringCommand.getMonitoredPidsList();
                if (pids != null) {
                    for (Integer pid : pids) {
                        mergedSchedule.remove(pid.intValue());
                    }
                }
            }
            if (this.gatherCanBusDataToLogs && (this.lastMonitorBatchCompletedTime == 0 || now - this.lastMonitorBatchCompletedTime > 30000)) {
                Log.i(TAG, "Starting new batch of monitoring");
                this.monitorCounter = 0;
                mergedSchedule.addPid(1000, 1000);
            } else if (!this.gatherCanBusDataToLogs) {
                this.monitorCounter = 0;
                this.lastMonitorBatchCompletedTime = 0;
            }
            synchronized (this.pidListeners) {
                for (int i = 0; i < this.pidListeners.size(); i++) {
                    mergedSchedule.merge(this.pidListeners.valueAt(i).schedule);
                }
            }
            if (mergedSchedule.isEmpty()) {
                Log.i(TAG, "cancelling scan schedule - no pids to scan");
                cancelCurrentSchedule();
                return;
            }
            this.mVehicleStateManager.resolveDependenciesForCustomPids(mergedSchedule);
            switch (this.currentMode) {
                case 0:
                    scanPids(mergedSchedule);
                    break;
                case 1:
                    monitorCANJ1939(mergedSchedule);
                    break;
                default:
                    break;
            }

        } else {
            Log.i(TAG, "ignoring scan update - not enabled");
        }
    }

    public void addListener(IObdServiceListener listener) {
        synchronized (this.listeners) {
            this.listeners.add(listener);
        }
    }

    public void removeListener(IObdServiceListener listener) {
        synchronized (this.listeners) {
            this.listeners.remove(listener);
        }
    }

    public void resetObdChip() {
        if (remoteClientConnected) {
            Log.i(TAG, "Not resetting the obd chip, remote client connected");
        }
        Log.i(TAG, "Resetting the obd chip");
        cancelVoltageMonitoring();
        cancelDtcMonitoring();
        try {
            Log.i(TAG, "Turning off echo");
            synchronousCommand(new BatchCommand(new ObdCommand("DUMMY", "\r"), ObdCommand.RESET_COMMAND, ObdCommand.ECHO_OFF_COMMAND));
        } catch (ExecutionException e) {
            Log.i(TAG, "Failed to turn off echo", e);
        }
        if (this.channel == null) {
            Log.i(TAG, "Failed to reset the obd chip, Channel is null");
            return;
        }
        this.currentFirmwareVersion = null;
        if (this.firmwareManager != null) {
            try {
                this.currentFirmwareVersion = this.firmwareManager.getFirmwareVersion();
            } catch (RemoteException e) {
                e.printStackTrace();
            }
        }
        Log.i(TAG, "Current firmware version of the Obd chip : " + this.currentFirmwareVersion);
    }

    public void monitorBatteryVoltage() {
        Log.i(TAG, "Starting to monitor voltage");
        if (this.channel == null) {
            Log.i(TAG, "Failed to start monitoring voltage");
            return;
        }
        final ReadVoltageCommand command = new ReadVoltageCommand();
        this.voltageMonitoring = this.scheduler.scheduleWithFixedDelay(new ObdJob(command, this.channel, null, (job, success) -> {
            if (success) {
                double voltage = (double) command.getVoltage();
                ObdService.this.currentBatteryVoltage = voltage;
                long now = SystemClock.elapsedRealtime();
                if (ObdService.this.lastVoltageReport == 0 || now - ObdService.this.lastVoltageReport > ((long) ObdService.VOLTAGE_REPORT_INTERVAL)) {
                    Log.i(ObdService.TAG, "Battery voltage: " + voltage);
                    ObdService.this.lastVoltageReport = now;
                }
                if (ObdService.this.connectionState != 4 || !ObdService.this.isScanningEnabled) {
                    return;
                }
                if (voltage <= ((double) ObdService.this.voltageSettings.chargingVoltage) && !debugMode) {
                    return;
                }
                if (ObdService.this.lastDisconnectTime == 0 || SystemClock.elapsedRealtime() - ObdService.this.lastDisconnectTime > ObdService.RECONNECT_DELAY) {
                    Log.i(ObdService.TAG, "Battery now charging: " + voltage);
                    ObdService.this.serviceHandler.sendEmptyMessage(4);
                    return;
                }
                Log.i(ObdService.TAG, "Waiting for voltage to settle: " + voltage);
            }
        }, this.obdDataObserver), 0, 2000, TimeUnit.MILLISECONDS);
    }

    public void monitorCheckEngineLight() {
        Log.i(TAG, "Starting to monitor CEL");
        if (this.channel == null) {
            Log.i(TAG, "Failed to start monitoring CEL");
            return;
        }
        final CheckDTCCommand command = new CheckDTCCommand(this.vehicleInfo.ecus);
        this.celMonitoring = this.scheduler.scheduleWithFixedDelay(new ObdJob(command, this.channel, this.protocol, new IListener() {
            public void onComplete(ObdJob job, boolean success) {
                if (success) {
                    ObdService.this.vehicleInfo.isCheckEngineLightOn = command.isCheckEngineLightOn();
                    Log.i(ObdService.TAG, "CEL: " + (ObdService.this.vehicleInfo.isCheckEngineLightOn?"On":"Off"));
                }
            }
        }, this.obdDataObserver), 0, 2000, TimeUnit.MILLISECONDS);
    }

    public void monitorTroubleCodes() {
        Log.i(TAG, "Starting to monitor TroubleCodes");
        if (this.channel == null) {
            Log.i(TAG, "Failed to start monitoring TroubleCodes");
            return;
        }
        final ReadTroubleCodesCommand command = new ReadTroubleCodesCommand(this.vehicleInfo.ecus);
        this.troubleMonitoring = this.scheduler.scheduleWithFixedDelay(new ObdJob(command, this.channel, this.protocol, new IListener() {
            public void onComplete(ObdJob job, boolean success) {
                if (success) {
                    List<String> troubleCodes = null;
                    troubleCodes = command.getTroubleCodes();
                    if (troubleCodes != null) {
                        for (String troubleCode : troubleCodes) {
                            Log.i(ObdService.TAG, "Current Trouble Code: " + troubleCode);
                        }
                        ObdService.this.vehicleInfo.troubleCodes = troubleCodes;
                    }
                }
            }
        }, this.obdDataObserver), 0, 2000, TimeUnit.MILLISECONDS);
    }

    public void monitorPendingCodes() {
        Log.i(TAG, "Starting to monitor Trouble and Pending Codes");
        if (this.channel == null) {
            Log.i(TAG, "Failed to start monitoring PendingCodes");
            return;
        }

        ScheduledExecutorService scheduledExecutorService = this.scheduler;
        this.pendingMonitoring = scheduledExecutorService.scheduleWithFixedDelay(
            ScanDtcJob.newScanDtcJob(this.vehicleInfo, this.channel, this.protocol, new IListener() {
                    public void onComplete(ObdJob job, boolean success) {
                        String runCommand = SystemProperties.get("persist.sys.obd.runCommand", null);
                        if (!TextUtils.isEmpty(runCommand)) {

                            String[] runCommands = runCommand.split(";");  
                            BatchCommand bc = new BatchCommand();
                            for (String com : runCommands) {
                                Log.i(ObdService.TAG, "Command: " + com);
                                bc.add(new ObdCommand(com));
                            }

                            ScheduledExecutorService scheduledExecutorService = ObdService.this.scheduler;
                            scheduledExecutorService.schedule(new ObdJob(
                                bc,
                                ObdService.this.channel, ObdService.this.protocol, new IListener() {
                                public void onComplete(ObdJob job, boolean success) {
                                    if (success) {
                                        // Log.i(ObdService.TAG, "R");
                                    }
                                }
                            }, ObdService.this.obdDataObserver), 100, TimeUnit.MILLISECONDS);

                        }

                        if (success) {
                            List<String> troubleCodes = ObdService.this.vehicleInfo.troubleCodes;

                            if (troubleCodes == null) {
                                Log.d(TAG, "No DTC");
                            } else {
                                for (String troubleCode : troubleCodes) {
                                    Log.d(TAG, "Current DTC: " + troubleCode);
                                }

                                String code = "P0401";
                                if (ObdService.this.clear0401 && troubleCodes.size() == 1 && code.equals(troubleCodes.get(0))) {

                                    Log.i(ObdService.TAG, "Will clear DTC 0401");
                                    // ObdCommand setHeader = new ObdCommand("Set Header", "ATSH8110F1");
                                    // ObdCommand sendClear = new ObdCommand("Clear Codes", "14FF001");
                                    
                                    ObdService.this.dtcClearCommand = SystemProperties.get("persist.sys.obd.clearCommand", ObdService.this.dtcClearCommand);
                                    ObdService.this.dtcClearCommands = ObdService.this.dtcClearCommand.split(";");  


                                    ICommand clearDTCCommand = null;
                                    // if (ObdService.this.dtcClearCommands.length > 1) {
                                        BatchCommand bc = new BatchCommand();
                                        for (String comm : ObdService.this.dtcClearCommands) {
                                            Log.i(ObdService.TAG, "Command: " + comm);
                                            bc.add(new ObdCommand(comm));
                                        }
                                        clearDTCCommand = bc;
                                    // } else {
                                    //     clearDTCCommand = new ObdCommand(ObdService.this.dtcClearCommand);
                                    // }

                                    ObdService.this.scheduler.schedule(new ObdJob(
                                        clearDTCCommand,
                                        ObdService.this.channel, ObdService.this.protocol, (job1, success1) -> {
                                            if (success1) {
                                                Log.i(ObdService.TAG, "Cleared DTC P0401");
                                            }
                                        }, ObdService.this.obdDataObserver), 100, TimeUnit.MILLISECONDS);
                                }
                            }
                        }
                    }
                }, this.obdDataObserver), 
            0, 2000, TimeUnit.MILLISECONDS);
    }

    public void sleepWithVoltageWakeUpTriggers() {
        cancelVoltageMonitoring();
        cancelDtcMonitoring();
        cancelCurrentSchedule();
        BatchCommand sleepCommand = new BatchCommand(ObdCommand.ECHO_OFF_COMMAND);
        synchronized (this.voltageSettingsLock) {
            if (this.deepSleep) {
                ObdCommand chargingVoltageTrigger = ObdCommand.createSetVoltageLevelWakeupTriggerCommand(false, this.voltageSettings.chargingVoltage, 0);
                sleepCommand.add(ObdCommand.TURN_OFF_VOLTAGE_DELTA_WAKEUP);
                sleepCommand.add(ObdCommand.TURN_ON_VOLTAGE_LEVEL_WAKEUP);
                sleepCommand.add(chargingVoltageTrigger);
            } else {
                ObdCommand lowVoltageTrigger = ObdCommand.createSetVoltageLevelWakeupTriggerCommand(true, this.voltageSettings.lowBatteryVoltage, 10);
                sleepCommand.add(ObdCommand.TURN_ON_VOLTAGE_DELTA_WAKEUP);
                sleepCommand.add(ObdCommand.TURN_ON_VOLTAGE_LEVEL_WAKEUP);
                sleepCommand.add(lowVoltageTrigger);
            }
            sleepCommand.add(ObdCommand.RESET_COMMAND);
            sleepCommand.add(ObdCommand.ECHO_OFF_COMMAND);
        }
        Log.d(TAG, "Configuring sleep triggers:" + sleepCommand.getName());
        try {
            synchronousCommand(sleepCommand);
            ObdCommand waitCommand = ObdCommand.SLEEP_COMMAND;
            IChannel obdChannel = this.channel;
            if (obdChannel == null) {
                Log.e(TAG, "Invalid channel - unable to execute command");
            } else {
                this.scheduler.submit(new ObdJob(waitCommand, obdChannel, this.protocol, new IListener() {
                    public void onComplete(ObdJob job, boolean success) {
                        if (success) {
                            Log.d(ObdService.TAG, "Successfully able to put the Obd chip to sleep");
                        }
                    }
                }, this.obdDataObserver));
            }
        } catch (ExecutionException e) {
            Log.i(TAG, "Failed to enable monitoring during sleep", e);
        }
    }

    public void scanPids(List<Pid> pids, int intervalMs) {
        ScanSchedule schedule = new ScanSchedule();
        schedule.addPids(pids, intervalMs);
        scanPids(schedule);
    }

    public void monitorCANJ1939(ScanSchedule scanSchedule) {
        if (this.currentSchedule == null || this.currentSchedule.isDone()) {
            cancelVoltageMonitoring();
            cancelDtcMonitoring();
            Log.d(TAG, "Starting to monitor the CAN bus for data, using SAE J1939");
            List<Scan> scanList = scanSchedule.getScanList();
            List<Pid> pids = new ArrayList<>();
            for (Scan scan : scanList) {
                pids.add(new Pid(scan.pid));
            }
            final J1939ObdJobAdapter jobdAdpater = new J1939ObdJobAdapter(this.channel, this.j1939Profile, pids, this.mVehicleStateManager, this.obdDataObserver);
            jobdAdpater.setListener(new J1939ObdJobListener() {
                public void onNewDataAvailable() {
                    ObdService.this.updateListenersWithNewData(jobdAdpater.getMonitoredPidsList());
                }
            });
            this.currentSchedule = this.scheduler.schedule(jobdAdpater, 0, TimeUnit.MILLISECONDS);
            return;
        }
        Log.d(TAG, "Monitoring is already running");
    }

    public void scanPids(ScanSchedule schedule) {
        cancelCurrentSchedule();
        Log.d(TAG, "starting scan for " + schedule);
        this.currentSchedule = this.scheduler.scheduleWithFixedDelay(new ScanPidsJob(this.vehicleInfo, this.profile, schedule, this.mVehicleStateManager, this.channel, this.protocol, new ScanPidsJob.IListener() {
            public void onCanBusDataRead(ScanPidsJob job) {
                ObdService.this.updateListenersWithNewData(job.getFullPidsList());
            }

            public CANBusMonitoringCommand getCanBusMonitoringCommand() {
                return ObdService.this.canBusMonitoringStatus.canBusMonitoringCommand;
            }

            public void onCanBusMonitoringErrorDetected() {
                if (ObdService.this.gatherCanBusDataToLogs) {
                    ObdService.this.gatherCanBusDataToLogs = false;
                    if (ObdService.this.canBusMonitoringListener != null) {
                        try {
                            ObdService.this.canBusMonitoringListener.onCanBusMonitoringError(ObdServiceInterface.CAN_BUS_MONITOR_ERROR_MESSAGE_BAD_STATE);
                            ObdService.this.monitoringCanBusLimitReached = true;
                            ObdService.this.obdDataObserver.onRawCanBusMessage("\n");
                        } catch (RemoteException e) {
                            Log.e(ObdService.TAG, "RemoteException ", e);
                        }
                    }
                }
            }

            public void onComplete(ObdJob job, boolean success) {
                List<Pid> fullPids = ((ScanPidsJob) job).getFullPidsList();
                if (ObdService.this.gatherCanBusDataToLogs) {
                    if (GatherCANBusDataCommand.DEFAULT_MONITOR_COMMAND.isMonitoringFailureDetected()) {
                        Log.e(ObdService.TAG, "Can bus monitoring failed, disabling the monitoring");
                        GatherCANBusDataCommand.DEFAULT_MONITOR_COMMAND.reset();
                        ObdService.this.gatherCanBusDataToLogs = false;
                        if (ObdService.this.canBusMonitoringListener != null) {
                            try {
                                String errorData = GatherCANBusDataCommand.DEFAULT_MONITOR_COMMAND.getErrorData();
                                ICanBusMonitoringListener access$2400 = ObdService.this.canBusMonitoringListener;
                                if (TextUtils.isEmpty(errorData)) {
                                    errorData = ObdServiceInterface.GENERIC_CAN_BUS_MONITOR_ERROR_MESSAGE;
                                }
                                access$2400.onCanBusMonitoringError(errorData);
                            } catch (RemoteException e) {
                                Log.e(ObdService.TAG, "RemoteException ", e);
                            }
                        }
                        ObdService.this.updateScan();
                    } else if (GatherCANBusDataCommand.DEFAULT_MONITOR_COMMAND.getLastSampleSucceeded()) {
                        StringBuilder stringBuilder = new StringBuilder();
                        stringBuilder.append("OBD_DATA,");
                        for (Pid pid : fullPids) {
                            stringBuilder.append(pid.getId()).append(":").append(pid.getValue()).append(",");
                        }
                        try {
                            if (ObdService.this.canBusMonitoringListener != null) {
                                stringBuilder.append("Lat:").append(ObdService.this.canBusMonitoringListener.getLatitude()).append(",").append("Long:" + ObdService.this.canBusMonitoringListener.getLongitude() + ",").append("Speed:" + ObdService.this.canBusMonitoringListener.getGpsSpeed());
                            }
                        } catch (Exception e2) {
                            Log.e(ObdService.TAG, "");
                        }
                        double lastSampleDataSize = (double) GatherCANBusDataCommand.DEFAULT_MONITOR_COMMAND.getLastSampleDataSize();
                        if (!ObdService.this.onMonitoringStatusReported && lastSampleDataSize > 0.0d) {
                            int quantized;
                            ObdService.this.onMonitoringStatusReported = true;
                            if (lastSampleDataSize > 1000.0d) {
                                lastSampleDataSize = (double) Math.round(lastSampleDataSize / 1000.0d);
                                quantized = (int) (Math.round(lastSampleDataSize / 50.0d) * 50);
                            } else {
                                quantized = (int) lastSampleDataSize;
                            }
                            Log.d(ObdService.TAG, "Monitor succeeded , Data size, Actual : " + lastSampleDataSize + ", Quantized : " + quantized);
                            if (ObdService.this.canBusMonitoringListener != null) {
                                try {
                                    ObdService.this.canBusMonitoringListener.onCanBusMonitorSuccess(quantized);
                                } catch (RemoteException e3) {
                                    Log.e(ObdService.TAG, "Error reporting the can bus monitor success");
                                }
                            }
                        }
                        ObdService.this.obdDataObserver.onRawCanBusMessage(stringBuilder.toString());
                        ObdService.this.monitorCounter = ObdService.this.monitorCounter + 1;
                        if (ObdService.this.monitorCounter >= 5) {
                            Log.d(ObdService.TAG, "Monitor Batch completed");
                            ObdService.this.lastMonitorBatchCompletedTime = SystemClock.elapsedRealtime();
                            ObdService.this.monitorCounter = 0;
                            ObdService.this.updateScan();
                        }
                        GatherCANBusDataCommand.DEFAULT_MONITOR_COMMAND.resetLastSampleStats();
                    }
                    if (ObdService.this.canBusMonitoringListener != null) {
                        try {
                            if (ObdService.this.canBusMonitoringListener.isMonitoringLimitReached()) {
                                ObdService.this.monitoringCanBusLimitReached = true;
                                ObdService.this.obdDataObserver.onRawCanBusMessage("\n");
                            }
                        } catch (RemoteException e4) {
                            e4.printStackTrace();
                        }
                    }
                }
                ObdService.this.updateListenersWithNewData(fullPids);
            }
        }, this.obdDataObserver), 0, 10, TimeUnit.MILLISECONDS);
    }

    private void updateListenersWithNewData(List<Pid> pidsData) {
        synchronized (this.listeners) {
            for (IObdServiceListener listener : this.listeners) {
                try {
                    listener.scannedPids(pidsData);
                } catch (RemoteException e) {
                    Log.e(TAG, "Error notifying listener", e);
                }
            }
        }
        synchronized (this.pidListeners) {
            for (int i = this.pidListeners.size() - 1; i >= 0; i--) {
                try {
                    this.pidListeners.valueAt(i).recordReadings(pidsData);
                } catch (DeadObjectException e2) {
                    Log.e(TAG, "Removing dead pid listener");
                    this.pidListeners.removeAt(i);
                } catch (RemoteException e3) {
                    Log.e(TAG, "Error notifying pid listener", e3);
                }
            }
        }
    }

    public void scanVIN() {
        this.scheduler.submit(() -> {
            synchronized (ObdService.this.listeners) {
                String vin = ObdService.this.vehicleInfo != null ? ObdService.this.vehicleInfo.vin : null;
                for (IObdServiceListener listener : ObdService.this.listeners) {
                    try {
                        listener.scannedVIN(vin);
                    } catch (RemoteException e) {
                        Log.e(ObdService.TAG, "Error notifying listener", e);
                    }
                }
            }
        });
    }

    public void submitCommand(ObdCommand command, IListener listener) {
        this.scheduler.submit(new ObdJob(command, this.channel, this.protocol, listener, this.obdDataObserver));
    }

    public void scanSupportedPids() {
        this.scheduler.submit(() -> {
            List<Pid> pids = ObdService.this.vehicleInfo.getPrimaryEcu().supportedPids.asList();
            synchronized (ObdService.this.listeners) {
                for (IObdServiceListener listener : ObdService.this.listeners) {
                    try {
                        listener.supportedPids(pids);
                    } catch (RemoteException e) {
                        Log.e(ObdService.TAG, "Error notifying listener", e);
                    }
                }
            }
        });
    }

    private Protocol getSavedProtocol() {
        int index = SystemProperties.getInt(OVERRIDE_PROTOCOL, this.mSharedPrefs.getInt(PREFS_KEY_LAST_PROTOCOL, 0));
        if (index <= 0 || index - 1 >= Protocol.PROTOCOLS.length) {
            return null;
        }
        return Protocol.PROTOCOLS[index - 1];
    }

    public void resetChannel() {
        Log.i(TAG, "Resetting channel");
        cancelCurrentSchedule();
        if (this.isScanningEnabled) {
            this.canBusMonitoringStatus.state = CANBusMonitoringState.UNKNOWN;
            this.onMonitoringStatusReported = false;
            int newState = 4;
            if (this.channel != null) {
                VinCommand vinCommand;
                if (this.currentMode == 0) {
                    try {
                        Protocol protocol;
                        this.protocol = null;
                        BaseSTNInitializeCommand initCmd;

                        List<String> cmds = new ArrayList<>();

                        // Allow overriding startup commands for troublesome cars
                        String initialize = SystemProperties.get("persist.sys.obd.init", null);
                        if (!TextUtils.isEmpty(initialize)) {
                            String[] runCommands = initialize.split(";");  
                            Log.i(ObdService.TAG, "Using persist.sys.obd.init: " + TextUtils.join(";", cmds));
                            initCmd = new CustomInitializeCommand(runCommands);
                        } else {
                            initCmd = new InitializeCommand(getSavedProtocol(),
                                                            this.profile.details.init,
                                                            this.profile.details.detect);
                        }

                        for (ICommand cmd : initCmd.getCommands()) {
                            cmds.add(((ObdCommand)cmd).getCommand());
                        }
                        Log.i(ObdService.TAG, "Full Init Command: " + TextUtils.join(";", cmds));

                        synchronousCommand(initCmd);
                        String protocolResponse = initCmd.getProtocol();
                        if (TextUtils.isEmpty(protocolResponse)) {
                            protocol = null;
                        } else {
                            protocol = Protocol.parse(protocolResponse);
                        }
                        this.protocol = protocol;
                        Log.i(TAG, "Protocol response:" + protocolResponse);
                        if (this.protocol != null) {
                            this.configurationName = initCmd.getConfigurationName();
                            Log.i(TAG, "Configuration name :" + this.configurationName);
                            Log.i(TAG, "Initialized - " + initCmd.getResponse() + ", Protocol:" + this.protocol);
                            Log.i(TAG, "Scanning for VIN");
                            vinCommand = new VinCommand();
                            synchronousCommand(vinCommand);
                            Log.i(TAG, "Scanning for ECUs");
                            ValidPidsCommand command = new ValidPidsCommand();
                            synchronousCommand(command);
                            List<ECU> ecus = buildEcus(command);
                            CheckDTCCommand checkDTCCommand = new CheckDTCCommand(ecus);
                            synchronousCommand(checkDTCCommand);
                            boolean isCheckEngineLightIsOn = checkDTCCommand.isCheckEngineLightOn();
                            List<String> troubleCodes = null;
                            if (checkDTCCommand.getNumberOfTroubleCodes() > 0) {
                                ReadTroubleCodesCommand readTroubleCodesCommand = new ReadTroubleCodesCommand(ecus);
                                synchronousCommand(readTroubleCodesCommand);
                                troubleCodes = readTroubleCodesCommand.getTroubleCodes();
                                if (troubleCodes != null) {
                                    for (String troubleCode : troubleCodes) {
                                        Log.d(TAG, "Initial DTC: " + troubleCode);
                                    }
                                }
                            }
                            Log.i(TAG, "Building vehicle info");
                            this.vehicleInfo = build(this.protocol, ecus, vinCommand.getVIN(), isCheckEngineLightIsOn, troubleCodes);
                            dump(this.vehicleInfo);
                            ECU primaryEcu = this.vehicleInfo.getPrimaryEcu();
                            if (primaryEcu != null) {
                                PidSet pids = primaryEcu.supportedPids;
                                this.mVehicleStateManager = new VehicleStateManager(new DefaultPidProcessorFactory());
                                this.mVehicleStateManager.updateSupportedPids(pids);
                                this.mSharedPrefs.edit().putInt(PREFS_KEY_LAST_PROTOCOL, this.protocol.id).apply();
                                newState = 2;
                            } else {
                                Log.i(TAG, "No ECUs found - falling back to IDLE state");
                                this.lastDisconnectTime = SystemClock.elapsedRealtime();
                            }
                        }
                    } catch (Throwable t) {
                        Log.e(TAG, "Error while resetting the channel ", t);
                    }
                } else {
                    InitializeJ1939Command initCmd2 = new InitializeJ1939Command();
                    try {
                        synchronousCommand(initCmd2);
                        this.configurationName = initCmd2.getConfigurationName();
                        Log.i(TAG, "Configuration name :" + this.configurationName);
                        Log.i(TAG, "Scanning for VIN");
                        vinCommand = new VinCommand();
                        try {
                            synchronousCommand(vinCommand);
                        } catch (Throwable th) {
                            Log.i(TAG, "Failed to read VIN - trying to connect anyway");
                        }
                        this.vehicleInfo = new VehicleInfo(vinCommand.getVIN());
                        dump(this.vehicleInfo);
                        this.mVehicleStateManager = new VehicleStateManager(new DefaultPidProcessorFactory());
                        this.mVehicleStateManager.updateSupportedPids(J1939ObdJobAdapter.J1939SupportedObdPids);
                        newState = 2;
                    } catch (ExecutionException e) {
                        Log.e(TAG, "Error while resetting the channel ", e);
                    }
                }
            }
            postStateChange(newState);
            return;
        }
        postStateChange(4);
    }

    public void onLogFileRollOver(String loggerName, String fileName) {
        Log.d(TAG, "onLogFileRollover Logger :" + loggerName + ", File name : " + fileName);
        if (CAN_BUS_LOGGER.equals(loggerName) && this.gatherCanBusDataToLogs) {
            this.monitoringCanBusLimitReached = false;
            Log.d(TAG, "onLogFileRollOver , new data file is available " + fileName);
            if (this.canBusMonitoringListener != null) {
                try {
                    this.canBusMonitoringListener.onNewDataAvailable(fileName);
                } catch (RemoteException e) {
                    Log.e(TAG, "RemoteException ", e);
                }
            }
        }
    }

    private void postStateChange(int newState) {
        this.serviceHandler.obtainMessage(1, newState, -1).sendToTarget();
    }

    private void dump(VehicleInfo vehicleInfo) {
        Log.i(TAG, "Vehicle Info:");
        Log.i(TAG, "Protocol:" + vehicleInfo.protocol);
        Log.i(TAG, "VIN:" + vehicleInfo.vin);
        if (vehicleInfo.ecus != null) {
            Log.i(TAG, "ecus:" + vehicleInfo.ecus.size());
            ECU primaryEcu = vehicleInfo.getPrimaryEcu();
            for (ECU ecu : vehicleInfo.ecus) {
                Log.i(TAG, "  ecu: 0x" + Integer.toHexString(ecu.address) + (primaryEcu == ecu ? " PRIMARY" : ""));
                Log.i(TAG, "     : " + Arrays.toString(ecu.supportedPids.asList().toArray()));
            }
        }
        Log.i(TAG, "Check Engine light : " + (vehicleInfo.isCheckEngineLightOn ? "ON" : "OFF"));
        if (vehicleInfo.troubleCodes != null) {
            Log.i(TAG, "Trouble Codes");
            for (String troubleCode : vehicleInfo.troubleCodes) {
                Log.d(TAG, troubleCode);
            }
        }
    }

    public static List<ECU> buildEcus(ValidPidsCommand myCommand) {
        SparseArray<ECU> ecusByAddress = new SparseArray<>();
        int offset = 0;
        int i;
        for (ObdCommand command : myCommand.supportedPids()) {
            for (i = 0; i < command.getResponseCount(); i++) {
                EcuResponse response = command.getResponse(i);
                int address = response.ecu;
                ECU ecu = ecusByAddress.get(address);
                if (ecu == null) {
                    ecu = new ECU(address, new PidSet());
                    ecusByAddress.put(address, ecu);
                }
                if (response.data[0] != (byte) 65 || response.length < 6) {
                    Log.i(TAG, "Skipping " + Integer.toHexString(response.data[0]) + " response for ecu " + Integer.toHexString(address));
                } else {
                    int bitField = ByteBuffer.wrap(response.data, 2, 4).getInt();
                    Log.i(TAG, "Merging " + Integer.toHexString(bitField) + " for ecu " + Integer.toHexString(address) + " at offset " + Integer.toHexString(offset));
                    ecu.supportedPids.merge(new PidSet((long) bitField, offset));
                }
            }
            offset += 32;
        }
        List<ECU> ecus = new ArrayList<>();
        for (i = 0; i < ecusByAddress.size(); i++) {
            ecus.add(ecusByAddress.valueAt(i));
        }
        return ecus;
    }

    static VehicleInfo build(Protocol protocol, List<ECU> ecus, String vin, boolean isScanningEnabled, List<String> troubleCodes) {
        return new VehicleInfo(protocol != null ? protocol.name : "", ecus, vin, isScanningEnabled, troubleCodes);
    }

    public Protocol getProtocol() {
        return this.protocol;
    }

    public void openChannel(ChannelInfo info) {
        if (info != null) {
            if (this.channelInfo == null || !this.channelInfo.equals(info)) {
                if (this.channel != null) {
                    this.channel.disconnect();
                    this.channel = null;
                }
                this.channelInfo = info;
                ConnectionType channelType = info.getConnectionType();
                Log.i(TAG, "Opening channel " + info.asString());
                switch (channelType) {
                    case SERIAL:
                        this.channel = new STNSerialChannel(this, this, this.lastConnectedBaudRate, BAUD_RATE);
                        if ((this.firmwareManager instanceof STNSerialDeviceFirmwareManager)) {
                            ((STNSerialDeviceFirmwareManager) this.firmwareManager).init(this, this.channel);
                        } else {
                            this.firmwareManager = new STNSerialDeviceFirmwareManager(this, this.channel);
                        }
                        break;
                    case BLUETOOTH:
                        this.channel = new BluetoothChannel(this);
                        break;
                    case MOCK:
                        this.channel = new MockObdChannel(this);
                        break;
                }
                if (this.firmwareManager == null) {
                    this.firmwareManager = new DefaultFirmwareManager();
                }
                this.channel.connect(info.getAddress());
            }
        }
    }

    public void closeChannel() {
        this.vehicleInfo = null;
        this.protocol = null;
        this.currentBatteryVoltage = -1.0d;
        if (this.channelInfo != null) {
            this.channelInfo = null;
        }
        if (this.channel != null) {
            this.channel = null;
        }
    }

    public void onStateChange(int newState) {
        if (newState == 2) {
            if (this.channel instanceof STNSerialChannel) {
                this.lastConnectedBaudRate = ((STNSerialChannel) this.channel).getBaudRate();
                this.mSharedPrefs.edit().putInt(PREFS_LAST_CONNECTED_BAUD_RATE, this.lastConnectedBaudRate).apply();
            }
            newState = 4;
        }
        postStateChange(newState);
    }

    public void onMessage(String message) {
        Message msg = this.serviceHandler.obtainMessage(2);
        Bundle bundle = new Bundle();
        bundle.putString("toast", message);
        msg.setData(bundle);
        this.serviceHandler.sendMessage(msg);
    }

    public static ObdService getObdService() {
        return obdService;
    }
}
