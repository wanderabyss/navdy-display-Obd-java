package com.navdy.obd;

import java.util.List;

public class ProfileDetail {
    public String name;
    public List<String> init;
    public String detect;
    public List<ProfilePid> pids;
}