package com.navdy.obd;

import android.os.RemoteException;
import java.util.ArrayList;
import java.util.List;

public class PidListener {
    private CarState currentState = new CarState();
    public final IPidListener listener;
    public final PidSet monitoredPids;
    public final ScanSchedule schedule;

    public PidListener(IPidListener listener, ScanSchedule schedule) {
        this.listener = listener;
        this.schedule = schedule;
        this.monitoredPids = new PidSet(schedule);
    }

    public void recordReadings(List<Pid> pids) throws RemoteException {
        List<Pid> changedPids = null;
        for (Pid pid : pids) {
            if (this.monitoredPids.contains(pid) && this.currentState.recordReading(pid)) {
                if (changedPids == null) {
                    changedPids = new ArrayList<>();
                }
                changedPids.add(pid);
            }
        }
        if (changedPids != null) {
            this.listener.pidsChanged(changedPids);
        }
        this.listener.pidsRead(pids, changedPids);
    }
}
