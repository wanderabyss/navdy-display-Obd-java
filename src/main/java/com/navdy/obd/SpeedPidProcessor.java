package com.navdy.obd;

public class SpeedPidProcessor extends PidProcessor {
    public boolean isSupported(PidSet supportedPids) {
        return supportedPids != null && supportedPids.contains(13);
    }

    public boolean processPidValue(PidLookupTable vehicleState) {
        if (vehicleState.getPidValue(13) == 255.0d) {
            return vehicleState.updatePid(13, -2.147483648E9d);
        }
        return false;
    }

    public PidSet resolveDependencies() {
        return null;
    }
}
