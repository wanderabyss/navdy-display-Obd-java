package com.navdy.obd;

import android.util.SparseArray;
import java.util.ArrayList;
import java.util.List;

public class CarState {
    SparseArray<Pid> lastReadings = new SparseArray<>();

    public void recordReadings(List<Pid> pids) {
        for (Pid pid : pids) {
            recordReading(pid);
        }
    }

    public boolean recordReading(Pid pid) {
        int id = pid.getId();
        double newValue = pid.getValue();
        long newTimeStamp = pid.getTimeStamp();
        if (newValue == -2.147483648E9d) {
            return false;
        }
        Pid oldValue = this.lastReadings.get(id);
        if (oldValue == null) {
            Pid copy = new Pid(id);
            copy.setTimeStamp(newTimeStamp);
            copy.setValue(newValue);
            this.lastReadings.put(id, copy);
            return true;
        } else if (newValue == oldValue.getValue()) {
            return false;
        } else {
            oldValue.setTimeStamp(newTimeStamp);
            oldValue.setValue(newValue);
            return true;
        }
    }

    public List<Pid> getReadings(List<Pid> pids) {
        List<Pid> readings = new ArrayList<>();
        for (Pid pid : pids) {
            Pid lastReading = this.lastReadings.get(pid.getId());
            if (!(lastReading == null || lastReading.getValue() == -2.147483648E9d)) {
                readings.add(lastReading);
            }
        }
        return readings;
    }
}
