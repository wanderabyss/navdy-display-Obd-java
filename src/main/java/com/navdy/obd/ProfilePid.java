package com.navdy.obd;

import java.util.List;

public class ProfilePid {
    public String Name;
    public String ShortName;
    public String ModeAndPID;
    public String Equation;
    public double MaxValue;
    public double MinValue;
    public String Units;
}
