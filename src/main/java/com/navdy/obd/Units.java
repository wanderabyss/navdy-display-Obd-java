package com.navdy.obd;

import java.io.IOException;
import org.xmlpull.v1.XmlPullParserException;
import android.content.res.XmlResourceParser;


public enum Units
{
    KILOMETERS_PER_HOUR(System.Metric),
    MILES_PER_HOUR(System.US),
    NONE(System.None);

    protected String abbreviation;
    protected String description;
    protected System system;

    private Units() {
        this(System.None);
    }

    private Units(final System system) {
        this("", "", system);
    }

    private Units(final String abbreviation, final String description, final System system) {
        this.abbreviation = abbreviation;
        this.description = description;
        this.system = system;
    }

    static void localize(final XmlResourceParser xmlResourceParser) throws XmlPullParserException, IOException {
        Units units = null;
        final StringBuilder sb = new StringBuilder();
        Units value;
        for (int i = xmlResourceParser.getEventType(); i != 1; i = xmlResourceParser.next(), units = value) {
            final String name = xmlResourceParser.getName();
            if (i == 2) {
                value = units;
                if ("unit".equals(name)) {
                    value = valueOf(xmlResourceParser.getAttributeValue((String)null, "id"));
                }
            }
            else if (i == 3) {
                if (units != null) {
                    final String string = sb.toString();
                    if ("abbreviation".equals(name)) {
                        units.setAbbreviation(string);
                    }
                    else if ("description".equals(name)) {
                        units.setDescription(string);
                    }
                }
                if ("unit".equals(name)) {
                    units = null;
                }
                sb.setLength(0);
                value = units;
            }
            else {
                value = units;
                if (i == 4) {
                    sb.append(xmlResourceParser.getText());
                    value = units;
                }
            }
        }
    }

    public String getAbbreviation() {
        return this.abbreviation;
    }

    public String getDescription() {
        return this.description;
    }

    public System getSystem() {
        return this.system;
    }

    public void setAbbreviation(final String abbreviation) {
        this.abbreviation = abbreviation;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public enum System {
        None(0),
        Metric(1),
        US(2);

        private int value;
        System(int value) {
            this.value = value;
        }
        public int getValue() {
            return value;
        }
    }
}


//
//    public enum System {
//        private static final /* synthetic */ System[] $VALUES = null;
//        public static final System Metric = null;
//        public static final System None = null;
//        public static final System US = null;
//
//        private System(String str, int i) {
//        }
//
//        public static System valueOf(String name) {
//            return (System) Enum.valueOf(System.class, name);
//        }
//
//        public static System[] values() {
//            return (System[]) $VALUES.clone();
//        }
//
//        static {
//            None = new System("None", 0);
//            Metric = new System("Metric", 1);
//            US = new System("US", 2);
//            $VALUES = new System[]{None, Metric, US};
//        }
//    }
// }
