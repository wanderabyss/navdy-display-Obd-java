package com.navdy.obd.util;

public class HexUtil {
    public static void parseHexString(String hex, byte[] dest, int offset, int length) {
        byte[] charBytes = hex.getBytes();
        for (int i = 0; i < length; i++) {
            dest[i + offset] = (byte) ((parseHexDigit(charBytes[i * 2]) << 4) + parseHexDigit(charBytes[(i * 2) + 1]));
        }
    }

    public static int parseHexDigit(int nybble) {
        if (nybble >= 48 && nybble <= 57) {
            return nybble - 48;
        }
        if (nybble >= 97 && nybble <= 102) {
            return (nybble - 97) + 10;
        }
        if (nybble >= 65 && nybble <= 70) {
            return (nybble - 65) + 10;
        }
        throw new NumberFormatException();
    }
}
