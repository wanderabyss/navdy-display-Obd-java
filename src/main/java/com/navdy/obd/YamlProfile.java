package com.navdy.obd;

import com.navdy.obd.command.ObdCommand;
import com.navdy.obd.converters.CustomConversion;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.constructor.Constructor;

import java.io.InputStream;
import java.util.HashMap;

import parsii.tokenizer.ParseException;

public class YamlProfile extends Profile {
    static final Logger Log = LoggerFactory.getLogger(YamlProfile.class);
    public HashMap<String, ObdCommand> mapByName = new HashMap();
    public HashMap<Integer, ObdCommand> mapByPid = new HashMap();
    public HashMap<Integer, ObdCommand> mapByModePid = new HashMap();
    public ProfileDetail details = new ProfileDetail();

    // class Column {
    //     public static final String EQUATION = "Equation";
    //     public static final String MAX_VALUE = "Max Value";
    //     public static final String MIN_VALUE = "Min Value";
    //     public static final String MODE_AND_PID = "ModeAndPID";
    //     public static final String NAME = "Name";
    //     public static final String SHORT_NAME = "ShortName";
    //     public static final String UNITS = "Units";

    //     Column() {
    //     }
    // }

    static final HashMap<Integer, String> ShortPidName = createShortPidLookup();

    private static HashMap<Integer, String> createShortPidLookup() {
        HashMap<Integer, String> result = new HashMap<Integer, String>();
        result.put(0x04, "EngineLoad");
        result.put(0x05, "CoolantTemp");
        result.put(0x06, "STFT1");
        result.put(0x07, "LTFT1");
        result.put(0x08, "STFT2");
        result.put(0x09, "LTFT2");
        result.put(0x0A, "Fuelpressure");
        result.put(0x0B, "Intakepressure");
        result.put(0x0C, "RPM");
        result.put(0x0D, "Speed");
        result.put(0x0E, "TimingAdv");
        result.put(0x0F, "IntakeTemp");
        result.put(0x10, "MAF");
        result.put(0x11, "Throttle");
        result.put(0x14, "OxV");
        result.put(0x2F, "Fuel");
        result.put(0x31, "Distance");
        result.put(0x03, "FuelSystemStatus");
        result.put(0x46, "AmbientTemp");
        result.put(0x5C, "OilTemp");
        return result;
    }

    public ObdCommand lookup(String id) {
        ObdCommand command = this.mapByName.get(id);
        if (command != null) {
            return command.clone();
        }
        return null;
    }

    public ObdCommand lookup(int pid) {
        String pidname = ShortPidName.get(pid);
        if (pidname != null) {
            return this.lookup(pidname);
        }
        ObdCommand command = this.mapByPid.get(pid);
        if (command != null) {
            return command.clone();
        }
        command = this.mapByModePid.get(pid);
        if (command != null) {
            return command.clone();
        }
        return null;
    }

    void add(String modeAndPid, ObdCommand command) {
        if (modeAndPid.startsWith("01")) {
            this.mapByPid.put(Integer.parseInt(modeAndPid.substring(2, 4), 16), command);
        }
        this.mapByModePid.put(Integer.parseInt(modeAndPid, 16), command);
        this.mapByName.put(command.getName(), command);
    }

    public void load(InputStream yamlProfile) {

        Yaml yaml = new Yaml(new Constructor(ProfileDetail.class));
        this.details = yaml.load(yamlProfile);

        if (this.details != null) {
            for (ProfilePid pid : this.details.pids) {
                try {
                    add(pid.ModeAndPID, new ObdCommand(pid.ShortName, pid.ModeAndPID, new CustomConversion(pid.Equation)));
                } catch (ParseException e2) {
                    Log.error("Failed to parse equation (" + pid.Equation + ") for " + pid.Name);
                }
            }
        } else {
            Log.error("Failed to read yamlProfile");
        }

        // for (CSVRecord record : CSVFormat.DEFAULT.withHeader(new String[0]).parse(reader)) {
        //     String name = record.get(Column.SHORT_NAME);
        //     String modeAndPid = record.get(Column.MODE_AND_PID);
        //     String equation = record.get(Column.EQUATION);
        //     try {
        //         add(modeAndPid, new ObdCommand(name, modeAndPid, new CustomConversion(equation)));
        //     } catch (ParseException e2) {
        //         Log.error("Failed to parse equation (" + equation + ") for " + name);
        //     }
        // }
        // if (reader != null) {
        //     try {
        //         reader.close();
        //         bufferedReader = reader;
        //         return;
        //     } catch (Throwable e3) {
        //         Log.error("Error closing reader", e3);
        //         bufferedReader = reader;
        //         return;
        //     }
        // }
        // } catch (IOException e4) {
        //     bufferedReader = reader;
        //     throw e4;
        // } catch (Throwable th2) {
        //     bufferedReader = reader;
        // }
    }
}
