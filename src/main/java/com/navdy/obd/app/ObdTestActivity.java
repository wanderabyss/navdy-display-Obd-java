package com.navdy.obd.app;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.RemoteException;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;
import android.widget.Toast;

import com.navdy.obd.ICarService;
import com.navdy.obd.ICarService.Stub;
import com.navdy.obd.IObdService;
import com.navdy.obd.IObdServiceListener;
import com.navdy.obd.ObdServiceInterface;
import com.navdy.obd.Pid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class ObdTestActivity extends Activity {
    private static final boolean D = true;
    private static final String DISCONNECT_CMD = "disconnect";
    private static final Logger LOG = LoggerFactory.getLogger(ObdTestActivity.class);
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_TOAST = 4;
    public static final int MESSAGE_WRITE = 3;
    private static final String RAW_CMD = "raw";
    private static final String READ_CMD = "read ";
    private static final int REQUEST_CONNECT_DEVICE = 1;
    private static final int REQUEST_ENABLE_BT = 2;
    private static final String RESET_CMD = "reset";
    private static final String SCAN_CMD = "scan";
    private static final String TAG = "ObdTestApp";
    public static final String TOAST = "toast";
    private ICarService carApi;
    private ServiceConnection carServiceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.i(ObdTestActivity.TAG, "CarService connection established");
            ObdTestActivity.this.carApi = Stub.asInterface(service);
        }

        public void onServiceDisconnected(ComponentName name) {
            Log.i(ObdTestActivity.TAG, "CarService disconnected");
        }
    };
    private ArrayAdapter<String> mConversationArrayAdapter;
    @InjectView(2131361806)
    protected ListView mConversationView;
    @SuppressLint("HandlerLeak")
    private final Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case 1:
                    Log.i(ObdTestActivity.TAG, "MESSAGE_STATE_CHANGE: " + msg.arg1);
                    switch (msg.arg1) {
                        case 1:
                            return;
                        case 2:
                            ObdTestActivity.this.mConversationArrayAdapter.clear();
                            return;
                        default:
                            return;
                    }
                case 2:
                    ObdTestActivity.this.mConversationArrayAdapter.add(new String((byte[]) msg.obj, 0, msg.arg1));
                    return;
                case 3:
                    ObdTestActivity.this.mConversationArrayAdapter.add("Me:  " + new String((byte[]) msg.obj));
                    return;
                case 4:
                    Toast.makeText(ObdTestActivity.this.getApplicationContext(), msg.getData().getString(TOAST), Toast.LENGTH_SHORT).show();
                    return;
                default:
            }
        }
    };
    @InjectView(2131361807)
    protected EditText mOutEditText;
    private StringBuffer mOutStringBuffer = null;
    @InjectView(2131361808)
    protected Button mSendButton;
    private OnEditorActionListener mWriteListener = (view, actionId, event) -> {
        if ((actionId == 0 && event.getAction() == 1) || actionId == 4) {
            ObdTestActivity.this.sendMessage(view.getText().toString());
        }
        Log.i(ObdTestActivity.TAG, "END onEditorAction");
        return false;
    };
    private IObdService obdApi;
    private IObdServiceListener.Stub obdListener = new IObdServiceListener.Stub() {
        public void onConnectionStateChange(int newState) {
            Log.d(ObdTestActivity.TAG, "StateChange:" + newState);
            ObdTestActivity.this.mHandler.obtainMessage(1, newState, -1).sendToTarget();
        }

        public void onStatusMessage(String message) {
            Message msg = ObdTestActivity.this.mHandler.obtainMessage(4);
            Bundle bundle = new Bundle();
            bundle.putString("toast", message);
            msg.setData(bundle);
            ObdTestActivity.this.mHandler.sendMessage(msg);
        }

        public void onRawData(String data) {
            byte[] buffer = data.getBytes();
            ObdTestActivity.this.mHandler.obtainMessage(2, buffer.length, -1, buffer).sendToTarget();
        }

        public void scannedPids(List<Pid> pids) {
            StringBuilder message = new StringBuilder();
            for (Pid pid : pids) {
                message.append(Integer.toHexString(pid.getId()));
                message.append("=");
                message.append(pid.getValue());
                message.append(" ");
            }
            byte[] buffer = message.toString().getBytes();
            ObdTestActivity.this.mHandler.obtainMessage(2, buffer.length, -1, buffer).sendToTarget();
        }

        public void supportedPids(List<Pid> list) {
        }

        public void scannedVIN(String vin) {
        }
    };
    private ServiceConnection serviceConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.i(ObdTestActivity.TAG, "Service connection established");
            ObdTestActivity.this.obdApi = IObdService.Stub.asInterface(service);
        }

        public void onServiceDisconnected(ComponentName name) {
            Log.i(ObdTestActivity.TAG, "Service disconnected");
        }
    };

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.e(TAG, "+++ ON CREATE +++");
        LOG.info("Testing logback logging");
        setContentView(R.layout.main);
        ButterKnife.inject(this, this);
    }

    public void onStart() {
        super.onStart();
        Log.e(TAG, "++ ON START ++");
        if (this.mOutStringBuffer == null) {
            setupChat();
        }
    }

    public synchronized void onResume() {
        super.onResume();
        Log.e(TAG, "+ ON RESUME +");
    }

    private void setupChat() {
        Log.d(TAG, "setupChat()");
        this.mConversationArrayAdapter = new ArrayAdapter<>(this, R.layout.message);
        this.mConversationView.setAdapter(this.mConversationArrayAdapter);
        this.mOutEditText.setOnEditorActionListener(this.mWriteListener);
        this.mSendButton.setOnClickListener(v -> ObdTestActivity.this.sendMessage(((TextView) ObdTestActivity.this.findViewById(R.id.edit_text_out)).getText().toString()));
        this.mOutStringBuffer = new StringBuffer();
    }

    public synchronized void onPause() {
        super.onPause();
        Log.e(TAG, "- ON PAUSE -");
    }

    public void onStop() {
        super.onStop();
        Log.e(TAG, "-- ON STOP --");
    }

    public void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "--- ON DESTROY ---");
        try {
            this.obdApi.removeListener(this.obdListener);
            unbindService(this.serviceConnection);
        } catch (Throwable t) {
            Log.w(TAG, "Failed to disconnect from service", t);
        }
    }

    private void sendMessage(String message) {
        int state = 0;
        try {
            state = this.obdApi.getState();
        } catch (RemoteException e) {
            Log.w(TAG, "Failed to read service state");
        }
        if (state != 2) {
            Utility.toastNotConnectedMessage(this);
        } else if (message.length() > 0) {
            try {
                if (message.startsWith(SCAN_CMD)) {
                    List<Pid> pids = new ArrayList<>();
                    pids.add(new Pid(13));
                    pids.add(new Pid(5));
                    pids.add(new Pid(12));
                    pids.add(new Pid(16));
                    this.obdApi.scanPids(pids, 1);
                } else if (message.startsWith(RAW_CMD)) {
                    this.obdApi.startRawScan();
                } else if (message.startsWith(READ_CMD)) {
                    String response = this.obdApi.readPid(Integer.valueOf(message.substring(READ_CMD.length())));
                    if (response != null) {
                        byte[] bytes = response.getBytes();
                        this.mHandler.obtainMessage(2, bytes.length, -1, bytes).sendToTarget();
                    }
                } else if (message.equals(RESET_CMD)) {
                    this.obdApi.reset();
                } else if (message.equals(DISCONNECT_CMD)) {
                    this.obdApi.disconnect();
                } else {
                    Log.e(TAG, "Unknown command " + message);
                }
            } catch (RemoteException e2) {
                Log.e(TAG, "Failed to send " + message + " command");
            }
            this.mOutStringBuffer.setLength(0);
            this.mOutEditText.setText(this.mOutStringBuffer);
        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult " + resultCode);
        switch (requestCode) {
            case 1:
                if (resultCode == -1) {
                    String address = Objects.requireNonNull(data.getExtras()).getString(DeviceListActivity.EXTRA_DEVICE_ADDRESS);
                    Log.d(TAG, "Attempting to connect to " + address);
                    try {
                        this.obdApi.connect(address, this.obdListener);
                        return;
                    } catch (RemoteException e) {
                        Log.e(TAG, "Failed to add listener", e);
                        return;
                    }
                }
                return;
            case 2:
                if (resultCode == -1) {
                    setupChat();
                    return;
                }
                Log.d(TAG, "BT not enabled");
                Toast.makeText(this, R.string.bt_not_enabled_leaving, Toast.LENGTH_SHORT).show();
                finish();
                return;
            default:
        }
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.option_menu, menu);
        return D;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.scan:
                startActivityForResult(new Intent(this, DeviceListActivity.class), 1);
                return D;
            case R.id.auto_connect:
                ObdServiceInterface.startObdService(this);
                return D;
            case R.id.upgrade:
                startActivity(new Intent(this, UpgradeObdActivity.class));
                return D;
            case R.id.flash:
                UpgradeObdActivity.flash(this, D);
                return D;
            case R.id.flash_upgrade:
                UpgradeObdActivity.flash(this, false);
                return D;
            default:
                return false;
        }
    }
}
