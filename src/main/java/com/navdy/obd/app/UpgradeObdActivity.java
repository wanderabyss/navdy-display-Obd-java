package com.navdy.obd.app;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.navdy.obd.IObdService;
import com.navdy.obd.IObdService.Stub;
import com.navdy.obd.ObdService;
import com.navdy.obd.io.IChannelSink;
import com.navdy.obd.io.SerialChannel;
import com.navdy.obd.update.STNSerialDeviceFirmwareManager;
import com.navdy.obd.update.Update;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import butterknife.ButterKnife;
import butterknife.InjectView;

public class UpgradeObdActivity extends Activity implements ServiceConnection {
    private static final String TAG = UpgradeObdActivity.class.getSimpleName();
    @InjectView(2131361793)
    protected TextView mTxtCurrentVersion;
    @InjectView(2131361795)
    protected TextView mTxtUpgradeVersion;
    private IObdService obdApi;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upgrade_obd);
        ButterKnife.inject(this, this);
        Intent intent = new Intent(this, ObdService.class);
        intent.setAction(IObdService.class.getName());
        bindService(intent, this, 0);
    }

    private void update() {
        if (this.obdApi == null) {
            Utility.toastNotConnectedMessage(this);
            return;
        }
        int state = 0;
        try {
            state = this.obdApi.getState();
        } catch (RemoteException e) {
            Log.w(TAG, "Failed to read service state");
        }
        if (state != 2) {
            Utility.toastNotConnectedMessage(this);
            return;
        }
        try {
            String outputPath = getFilesDir().getAbsolutePath() + File.separator + "update.bin";
            writeResourceToFile(this, R.raw.update, outputPath);
            this.obdApi.updateDeviceFirmware("3.2.0", outputPath);
        } catch (RemoteException e2) {
            Log.e(TAG, "Exception updating the firmware " + e2);
        }
    }

    static void writeResourceToFile(Context context, int resId, String outputPath) {
        InputStream is = context.getResources().openRawResource(resId);
        File file = new File(outputPath);
        if (file.exists()) {
            file.delete();
        }
        try {
            byte[] buffer = new byte[is.available()];
            is.read(buffer);
            FileOutputStream fos = new FileOutputStream(file);
            fos.write(buffer);
            fos.flush();
            fos.close();
            is.close();
        } catch (IOException e) {
            Log.e(TAG, "Error writing resource to " + outputPath);
        }
    }

    public void onClick(View v) {
        update();
    }

    private void initView() {
        try {
            this.mTxtCurrentVersion.setText(this.obdApi.getFirmwareVersion());
        } catch (RemoteException e) {
            Log.e(TAG, "Error while getting firmware version " + e.getCause());
        }
    }

    public void onServiceConnected(ComponentName name, IBinder service) {
        Log.d(TAG, "Obd service connected");
        this.obdApi = Stub.asInterface(service);
        initView();
    }

    public void onServiceDisconnected(ComponentName name) {
        Log.d(TAG, "Obd service disconnected");
    }

    public static void flash(final Context context, final boolean recover) {
        new AsyncTask<Void, Void, Void>() {
            protected Void doInBackground(Void... params) {
                SerialChannel serialChannel = new SerialChannel(context, new IChannelSink() {
                    public void onStateChange(int newState) {
                        Log.e(UpgradeObdActivity.TAG, "Connected");
                    }

                    public void onMessage(String message) {
                    }
                });
                serialChannel.connect("/dev/ttymxc3");
                STNSerialDeviceFirmwareManager stnSerialDeviceFirmwareManager = new STNSerialDeviceFirmwareManager(null, serialChannel);
                try {
                    String outputPath = context.getFilesDir().getAbsolutePath() + File.separator + "update.bin";
                    UpgradeObdActivity.writeResourceToFile(context, R.raw.update, outputPath);
                    Update update = new Update();
                    update.mVersion = "";
                    update.mUpdateFilePath = outputPath;
                    stnSerialDeviceFirmwareManager.update(update, serialChannel.getSerialPort(), !recover);
                } catch (Throwable e) {
                    Log.e(UpgradeObdActivity.TAG, "Exception updating the firmware " + e);
                }
                return null;
            }
        }.execute();
    }
}
