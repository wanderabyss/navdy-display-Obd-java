package com.navdy.obd.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.navdy.obd.discovery.GroupScanner;
import com.navdy.obd.discovery.ObdChannelScanner;
import com.navdy.obd.discovery.ObdChannelScanner.Listener;
import com.navdy.obd.io.ChannelInfo;

import java.util.HashSet;

public class DeviceListActivity extends Activity implements Listener {
    private static final boolean D = true;
    public static String EXTRA_DEVICE_ADDRESS = "device_address";
    private static final String TAG = "DeviceListActivity";
    private HashSet<String> discoveredDevices;
    private OnItemClickListener mDeviceClickListener = (av, v, position, id) -> {
        DeviceListActivity.this.scanner.stopScan();
        ChannelInfo info = (ChannelInfo) av.getAdapter().getItem(position);
        Intent intent = new Intent();
        intent.putExtra(DeviceListActivity.EXTRA_DEVICE_ADDRESS, info.asString());
        DeviceListActivity.this.setResult(-1, intent);
        DeviceListActivity.this.finish();
    };
    private ArrayAdapter<ChannelInfo> mNewDevicesArrayAdapter;
    private ArrayAdapter<ChannelInfo> mPairedDevicesArrayAdapter;
    private GroupScanner scanner;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(5);
        setContentView(R.layout.device_list);
        setResult(0);
        findViewById(R.id.button_scan).setOnClickListener(v -> {
            DeviceListActivity.this.doDiscovery();
            v.setVisibility (View.GONE);
        });
        this.discoveredDevices = new HashSet<>();
        this.mPairedDevicesArrayAdapter = new ArrayAdapter<>(this, R.layout.device_name);
        this.mNewDevicesArrayAdapter = new ArrayAdapter<>(this, R.layout.device_name);
        ListView pairedListView = (ListView) findViewById(R.id.paired_devices);
        pairedListView.setAdapter(this.mPairedDevicesArrayAdapter);
        pairedListView.setOnItemClickListener(this.mDeviceClickListener);
        ListView newDevicesListView = (ListView) findViewById(R.id.new_devices);
        newDevicesListView.setAdapter(this.mNewDevicesArrayAdapter);
        newDevicesListView.setOnItemClickListener(this.mDeviceClickListener);
        this.scanner = new GroupScanner(this, this);
        ((ListView) findViewById(R.id.paired_devices)).setEmptyView(findViewById(R.id.paired_empty));
        ((ListView) findViewById(R.id.new_devices)).setEmptyView(findViewById(R.id.empty));
    }

    protected void onDestroy() {
        super.onDestroy();
        this.scanner.stopScan();
    }

    private void doDiscovery() {
        Log.d(TAG, "doDiscovery()");
        setProgressBarIndeterminateVisibility(D);
        setTitle(R.string.scanning);
        findViewById(R.id.title_new_devices).setVisibility(View.VISIBLE);
        this.scanner.startScan();
    }

    public void onScanStarted(ObdChannelScanner scanner) {
    }

    public void onScanStopped(ObdChannelScanner scanner) {
        setProgressBarIndeterminateVisibility(false);
        setTitle(R.string.select_device);
    }

    public void onDiscovered(ObdChannelScanner scanner, ChannelInfo device) {
        if (!this.discoveredDevices.add(device.getAddress())) {
            return;
        }
        if (device.isBonded()) {
            if (this.mPairedDevicesArrayAdapter.getCount() == 0) {
                findViewById(R.id.title_paired_devices).setVisibility(View.VISIBLE);
            }
            this.mPairedDevicesArrayAdapter.add(device);
            return;
        }
        this.mNewDevicesArrayAdapter.add(device);
    }
}
