package com.navdy.obd.command;

import com.navdy.obd.ECU;
import com.navdy.obd.EcuResponse;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ReadTroubleCodesCommand extends ObdCommand {
    private static final String[] TROUBLE_CATEGORY = new String[]{"P", "C", "B", "U"};
    public boolean active = true;

    private static String pid(boolean active) {
        return active ? "03" : "07";
    }

    public ReadTroubleCodesCommand(List<ECU> ecus, boolean active) {
        super(pid(active));
        this.active = active;
        int num = -1;
        if (ecus != null && ecus.size() > 0) {
            num = ecus.size();
        }
        this.setExpectedResponses(num);
        Log.info("Reading {} DTC's for {} ECUs", 
            (this.active)?"active":"pending", num == -1? "all":num);
    }

    public ReadTroubleCodesCommand(List<ECU> ecus) {
        this(ecus, true);
    }

    public ReadTroubleCodesCommand(boolean active) {
        super(pid(active));
    }

    public ReadTroubleCodesCommand() {
        super(pid(true));
    }

    public List<String> getTroubleCodes() {
        Log.info("Raw Response of {} READTroubleCodesCommand, Data {}", 
            (this.active)?"active":"pending",  getResponse());
        int responseCount = getResponseCount();
        Log.info("Raw Response of {} READTroubleCodesCommand, Data {}, Count {}", 
            (this.active)?"active":"pending",  getResponse(), responseCount);
        List<String> troubleCodes = new ArrayList<>();
        for (int i = 0; i < responseCount; i++) {
            EcuResponse ecuResponse = getResponse(i);
            if (ecuResponse != null) {
                byte[] response = getResponse(i).data;
                Object responseAsString = Arrays.toString(response);
                Log.info("Response of {} READTroubleCodesCommand, ECU {} , Data {}", 
                         (this.active)?"active":"pending", ecuResponse.ecu, responseAsString);
                if (response != null && /* response[0] == (byte) 67 && */ response.length >= 3) {
                    for (int j = 1; j < response.length-1; j += 2) {
                        int firstHex = response[j] & 255;
                        int categoryIdentifierNibble = firstHex >> 4;
                        int secondNibble = firstHex & 15;
                        if (j + 1 >= response.length) {
                            break;
                        }
                        int secondHex = response[j + 1] & 255;
                        if (categoryIdentifierNibble <= 15) {
                            String categoryIdentifier = TROUBLE_CATEGORY[categoryIdentifierNibble / 4] + String.format("%d", Integer.valueOf(categoryIdentifierNibble % 4));
                            if (firstHex == 0 && secondHex == 0) {
                                break;
                            }
                            troubleCodes.add(categoryIdentifier + String.format("%x%02x", secondNibble, secondHex));
                        }
                    }
                }
            }
        }
        return troubleCodes;
    }
}
