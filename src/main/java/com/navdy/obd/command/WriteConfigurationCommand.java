package com.navdy.obd.command;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;

public class WriteConfigurationCommand extends BatchCommand {
    static final Logger Log = LoggerFactory.getLogger(WriteConfigurationCommand.class);
    public static final String SET_DEVICE_INFO_COMMAND = "STSATI";

    public WriteConfigurationCommand(String configuration) {
        super(new ArrayList<>());
        if (configuration == null) {
            throw new IllegalArgumentException("Configuration file cannot be empty");
        } else if (configuration.contains(SET_DEVICE_INFO_COMMAND)) {
            for (String command : configuration.split("\n")) {
                add(new ObdCommand(command));
            }
        } else {
            throw new IllegalArgumentException("Configuration should set the device information to identify the configuration");
        }
    }

    public String getResponse() {
        return super.getResponse();
    }
}
