package com.navdy.obd.command;

import com.navdy.obd.Protocol;
import com.navdy.obd.io.ObdCommandInterpreter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class GatherCANBusDataCommand extends ObdCommand {
    public static final int DEFAULT_MONITORING_DURATION = 2000;
    public static final GatherCANBusDataCommand DEFAULT_MONITOR_COMMAND = new GatherCANBusDataCommand(2000);
    public static final int GATHER_CAN_BUS_DATA_PID = 1000;
    static final Logger Log = LoggerFactory.getLogger(GatherCANBusDataCommand.class);
    public static final int MAX_RETRY_COUNT = 3;
    public static final int MINIMUM_DATA_REQUIRED = 100;
    public static final BatchCommand MONITOR_COMMAND = new BatchCommand();
    public static final int MONITOR_COMMAND_EXECUTION_TIMEOUT = 5000;
    public static final int READ_TIME_OUT_MILLIS = 1000;
    public static final int TIME_OUT_TO_ = 2000;
    ByteArrayOutputStream baos;
    byte[] buffer;
    private String errorData;
    private int failureCount;
    private LastSampleStats lastSampleStats;
    private long monitorDurationMilliseconds;
    private long monitorDurationNanos;

    public static class LastSampleStats {
        public long dataSizeCaptured;
        public boolean succeeded = false;

        public void reset() {
            this.succeeded = false;
            this.dataSizeCaptured = 0;
        }
    }

    static {
        MONITOR_COMMAND.add(TURN_OFF_FORMATTING_COMMAND);
        MONITOR_COMMAND.add(DEFAULT_MONITOR_COMMAND);
        MONITOR_COMMAND.add(new ObdCommand("Turn on formating", "ATCAF1") {
            protected void processCommandResponse(Protocol protocol, ObdCommandInterpreter obdCommandInterpreter, IObdDataObserver dataObserver) throws IOException {
                obdCommandInterpreter.debug = true;
                super.processCommandResponse(protocol, obdCommandInterpreter, dataObserver);
            }
        });
    }

    public GatherCANBusDataCommand() {
        this(2000);
    }

    public GatherCANBusDataCommand(long durationMillis) {
        super("CAN Monitoring", "ATMA");
        this.failureCount = 0;
        this.errorData = "";
        this.buffer = new byte[4096];
        this.baos = new ByteArrayOutputStream(512000);
        this.lastSampleStats = new LastSampleStats();
        this.monitorDurationMilliseconds = durationMillis;
        this.monitorDurationNanos = (this.monitorDurationMilliseconds * 1000) * 1000;
    }

    protected void processCommandResponse(Protocol protocol, ObdCommandInterpreter obdCommandInterpreter, IObdDataObserver dataObserver) throws IOException {
        long start = System.nanoTime();
        Log.info("Starting the CAN bus monitoring");
        obdCommandInterpreter.setReadTimeOut(1000);
        this.baos.reset();
        this.lastSampleStats.succeeded = false;
        int totalLength = 0;
        while (!Thread.interrupted() && System.nanoTime() - start < this.monitorDurationNanos) {
            try {
                int length = obdCommandInterpreter.read(this.buffer);
                if (length > 0) {
                    totalLength += length;
                    this.baos.write(this.buffer, 0, length);
                }
            } catch (IOException e2) {
                Log.error("IOException exception {}", e2);
                this.failureCount++;
            } catch (Throwable e) {
                Log.error("Interrupted exception {}", e);
                this.failureCount++;
            }
        }
        if (this.failureCount > 0 && totalLength > 100) {
            this.errorData = null;
            this.failureCount = 0;
        }
        if (totalLength < 100) {
            if (totalLength == 0) {
                Log.info("No data read from the CAN bus, marking as failed");
            } else {
                String newData = this.baos.toString().replace('\r', '\n');
                dataObserver.onRawCanBusMessage(newData);
                this.errorData = newData;
                Log.info("Very less data read from the CAN bus, marking as failed {}", newData);
            }
            this.failureCount++;
        } else {
            String newData2 = this.baos.toString();
            Log.debug("Total length {}, String length : {}", totalLength, newData2.length());
            dataObserver.onRawCanBusMessage(newData2);
            this.lastSampleStats.succeeded = true;
            this.lastSampleStats.dataSizeCaptured = (long) totalLength;
        }
        if (!Thread.interrupted()) {
            obdCommandInterpreter.write(new byte[]{(byte) 65, (byte) 84, (byte) 73, (byte) 73, (byte) 73, (byte) 13});
            Log.debug("Clearing the terminal");
            try {
                obdCommandInterpreter.clearTerminalBulk();
                Log.debug("Terminal clear");
            } catch (InterruptedException e3) {
                e3.printStackTrace();
                Log.debug("Interrupted while clearing terminal");
            } catch (TimeoutException e4) {
                e4.printStackTrace();
                Log.debug("Timed out while clearing terminal");
            }
        }
    }

    protected long getCommandExecutionTimeOutInMillis() {
        return 5000;
    }

    public boolean isMonitoringFailureDetected() {
        return this.failureCount > 3;
    }

    public void reset() {
        this.failureCount = 0;
        this.errorData = null;
        this.lastSampleStats.succeeded = false;
    }

    public long getLastSampleDataSize() {
        return this.lastSampleStats.dataSizeCaptured;
    }

    public boolean getLastSampleSucceeded() {
        return this.lastSampleStats.succeeded;
    }

    public void resetLastSampleStats() {
        this.lastSampleStats.reset();
    }

    public String getErrorData() {
        return this.errorData;
    }
}
