package com.navdy.obd.command;

public class BaseSTNInitializeCommand extends BatchCommand {
    private ObdCommand deviceInfoCommand = ObdCommand.DEVICE_INFO_COMMAND;

    public BaseSTNInitializeCommand(ICommand... commands) {
        super(commands);
        add(this.deviceInfoCommand);
    }

    public String getConfigurationName() {
        String response = this.deviceInfoCommand.getResponse();
        if (response != null) {
            return response.trim();
        }
        return null;
    }

    public String getProtocol() {
        return null;
    }

}
