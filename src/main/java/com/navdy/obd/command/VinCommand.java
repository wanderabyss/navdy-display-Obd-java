package com.navdy.obd.command;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class VinCommand extends ObdCommand {
    private static final int DATA_LEN = 4;
    private static final int HEADER_LEN = 3;
    static final Logger Log = LoggerFactory.getLogger(VinCommand.class);
    private static final int VIN_LENGTH = 20;

    public VinCommand() {
        super("0902");
    }

    public String getVIN() {
        byte[] response = getByteResponse();
        if (response == null) {
            return null;
        }
        try {
            if (response[0] != (byte) 73) {
                return new String(response).trim();
            }
            if (response.length <= 23) {
                return new String(response, 3, response.length - 3).trim();
            }
            StringBuilder vin = new StringBuilder();
            for (int pos = 3; pos + 4 <= response.length; pos += 7) {
                vin.append(new String(response, pos, 4));
            }
            return vin.toString().trim();
        } catch (Throwable e) {
            Log.error("Failed to parse vin", e);
            return null;
        }
    }
}
