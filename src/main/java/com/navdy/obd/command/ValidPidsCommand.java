package com.navdy.obd.command;

import com.navdy.obd.Protocol;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class ValidPidsCommand implements ICommand {
    static final Logger Log = LoggerFactory.getLogger(ValidPidsCommand.class);
    public static int MAX_ADDRESS = 128;
    private List<ObdCommand> supportedPids = new ArrayList<>();

    public String getName() {
        return "validPids";
    }

    public List<ObdCommand> supportedPids() {
        return this.supportedPids;
    }

    public void execute(InputStream input, OutputStream output, Protocol protocol, IObdDataObserver commandObserver) throws IOException {
        Log.debug("OBD - Scanning for valid pids with protocol = " + protocol);
        int address = 0;
        boolean done = false;
        while (!done && address <= MAX_ADDRESS) {
            String cmd = String.format("01%02x", Integer.valueOf(address));
            Log.debug("OBD - Reading " + cmd);
            ObdCommand command = new ObdCommand(cmd);
            command.execute(input, output, protocol, commandObserver);
            String response = command.getResponse();
            Log.debug("OBD - Response: " + (response != null ? response.replace('\r', '\n') : null));
            this.supportedPids.add(command);
            address += 32;
            done = !anyEcuSupportsMorePids(command);
        }
    }

    private boolean anyEcuSupportsMorePids(ObdCommand command) {
        for (int i = 0; i < command.getResponseCount(); i++) {
            if (supportsMorePids(command.getResponse(i).data)) {
                return true;
            }
        }
        return false;
    }

    private boolean supportsMorePids(byte[] data) {
        return data != null && data.length > 5 && (data[5] & 1) != 0;
    }

    public String getResponse() {
        return null;
    }

    public double getDoubleValue() {
        return 0.0d;
    }

    public int getIntValue() {
        return 0;
    }
}
