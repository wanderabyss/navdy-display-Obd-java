package com.navdy.obd.command;

import com.navdy.obd.Protocol;
import com.navdy.obd.can.CANBusDataDescriptor;
import com.navdy.obd.command.CANBusDataProcessor.ICanBusDataListener;
import com.navdy.obd.io.ObdCommandInterpreter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class CANBusMonitoringCommand extends BatchCommand {
    public static final int CAN_BUS_MONITOR_COMMAND_PID = 1001;
    private CANBusDataProcessor canbusDataProcessor;
    ArrayList<Integer> monitoredPids = new ArrayList<>();

    public CANBusMonitoringCommand(List<CANBusDataDescriptor> descriptors) {
        super();
        add(ObdCommand.TURN_OFF_FORMATTING_COMMAND);
        add(ObdCommand.CLEAR_ALL_PASS_FILTERS_COMMAND);
        HashSet<String> uniqueHeaders = new HashSet<>();
        for (CANBusDataDescriptor descriptor : descriptors) {
            this.monitoredPids.add(descriptor.obdPid);
            if (!uniqueHeaders.contains(descriptor.header)) {
                uniqueHeaders.add(descriptor.header);
                if (descriptor.header.length() == 3) {
                    add(new ObdCommand("STFAP " + descriptor.header + ",7FF"));
                }
            }
        }
        this.canbusDataProcessor = new CANBusDataProcessor(descriptors);
        add(this.canbusDataProcessor);
        add(new ObdCommand("Turn on formating", "ATCAF1") {
            protected void processCommandResponse(Protocol protocol, ObdCommandInterpreter obdCommandInterpreter, IObdDataObserver dataObserver) throws IOException {
                obdCommandInterpreter.debug = true;
                super.processCommandResponse(protocol, obdCommandInterpreter, dataObserver);
            }
        });
        add(ObdCommand.CLEAR_ALL_PASS_FILTERS_COMMAND);
    }

    public void setCANBusDataListener(ICanBusDataListener dataListener) {
        this.canbusDataProcessor.setCANBusDataListener(dataListener);
    }

    public void reset() {
        this.canbusDataProcessor.reset();
    }

    public List<Integer> getMonitoredPidsList() {
        return this.monitoredPids;
    }

    public void startMonitoring() {
        this.canbusDataProcessor.startMonitoring();
    }

    public boolean isSamplingComplete() {
        return this.canbusDataProcessor.isSamplingComplete();
    }

    public boolean isMonitoringFailed() {
        return this.canbusDataProcessor.isMonitoringFailed();
    }

    public boolean isSamplingSuccessful() {
        return this.canbusDataProcessor.isSamplingSuccessful();
    }
}
