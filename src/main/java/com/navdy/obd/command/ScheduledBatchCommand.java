package com.navdy.obd.command;

import android.os.SystemClock;
import com.navdy.obd.Pid;
import com.navdy.obd.Protocol;
import com.navdy.obd.ScanSchedule.Scan;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ScheduledBatchCommand extends BatchCommand {
    public List<Sample> samples;

    public ScheduledBatchCommand(List<Scan> scanList, ICommand[] commands) {
        super(Arrays.asList(commands));
        if (scanList == null || commands.length != scanList.size()) {
            throw new IllegalArgumentException("scanList length must match commands length");
        }
        this.samples = new ArrayList<>(scanList.size());
        for (int i = 0; i < scanList.size(); i++) {
            Scan scan = scanList.get(i);
            this.samples.add(new Sample(scan.pid, scan.scanInterval));
        }
    }

    public List<Pid> getPids() {
        List<Pid> result = new ArrayList<>(this.samples.size());
        for (int i = 0; i < this.samples.size(); i++) {
            result.add(this.samples.get(i).pid);
        }
        return result;
    }

    public void execute(InputStream input, OutputStream output, Protocol protocol, IObdDataObserver commandObserver) throws IOException {
        long nextSampleTime = 10000;
        boolean needToSleep = true;
        while (needToSleep) {
            int l = this.commands.size();
            for (int i = 0; i < l; i++) {
                Sample sample = this.samples.get(i);
                ICommand command = this.commands.get(i);
                long lastReading = sample.lastSampleTimestamp;
                int scanInterval = sample.getScanInterval();
                long startTime = SystemClock.elapsedRealtime();
                long timeToWait = ((long) scanInterval) - (startTime - lastReading);
                if (lastReading == 0 || timeToWait <= 0) {
                    command.execute(input, output, protocol, commandObserver);
                    long finishTime = SystemClock.elapsedRealtime();
                    sample.lastSampleTimestamp = startTime;
                    sample.setLastSamplingTime(finishTime - startTime);
                    sample.updated = true;
                    needToSleep = false;
                } else {
                    if (timeToWait < nextSampleTime) {
                        nextSampleTime = timeToWait;
                    }
                    sample.updated = false;
                }
            }
            if (needToSleep) {
                try {
                    Thread.sleep(nextSampleTime);
                } catch (InterruptedException e) {
                    needToSleep = false;
                }
            }
        }
    }
}
