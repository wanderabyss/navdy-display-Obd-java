package com.navdy.obd.command;

import android.support.v4.media.TransportMediator;
import com.navdy.obd.ECU;
import com.navdy.obd.EcuResponse;
import java.util.Arrays;
import java.util.List;

public class CheckDTCCommand extends ObdCommand {
    public CheckDTCCommand(List<ECU> ecus) {
        super("0101");
        if (ecus != null && ecus.size() > 0) {
            this.setExpectedResponses(ecus.size());
        }
    }
    public CheckDTCCommand() {
        super("0101");
    }

    public boolean isCheckEngineLightOn() {
        int responseCount = getResponseCount();
        for (int i = 0; i < responseCount; i++) {
            EcuResponse ecuResponse = getResponse(i);
            if (ecuResponse != null) {
                byte[] response = getResponse(i).data;
                if (response != null && response.length >= 3 && response[0] == (byte) 65 && response[1] == (byte) 1) {
                    int num = response[2] & 255;
                    if ((num & 128) > 0) {
                        Log.info("Check engine light is on , ECU {}, Number of DTCs set {}", ecuResponse.ecu, num - 128);
                        return true;
                    }
                }
                Log.info("Response of CheckDTCCommand, ECU {} , Data {}", ecuResponse.ecu, Arrays.toString(response));
            }
        }
        return false;
    }

    public int getNumberOfTroubleCodes() {
        int responseCount = getResponseCount();
        int numberOfTroubleCodes = 0;
        for (int i = 0; i < responseCount; i++) {
            EcuResponse ecuResponse = getResponse(i);
            if (ecuResponse != null) {
                byte[] response = getResponse(i).data;
                if (response != null && response.length >= 3 && response[0] == (byte) 65 && response[1] == (byte) 1) {
                    int num = response[2] & 255;
                    numberOfTroubleCodes += num & TransportMediator.KEYCODE_MEDIA_PAUSE;
                    Log.info("Check engine light is on , ECU {}, Number of DTCs set {}", ecuResponse.ecu, num - 128);
                }
                Log.info("Response of CheckDTCCommand, ECU {} , Data {}", ecuResponse.ecu, Arrays.toString(response));
            }
        }
        return numberOfTroubleCodes;
    }
}
