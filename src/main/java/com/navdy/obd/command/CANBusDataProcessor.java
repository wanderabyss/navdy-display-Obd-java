package com.navdy.obd.command;

import com.navdy.obd.Protocol;
import com.navdy.obd.can.CANBusDataDescriptor;
import com.navdy.obd.io.ObdCommandInterpreter;
import com.navdy.obd.util.HexUtil;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeoutException;

public class CANBusDataProcessor extends ObdCommand {
    public static final int MINIMUM_SAMPLES_NEEDED = 3;
    public static final int MONITORING_DURATION = 10000;
    public static final int READ_TIME_OUT_MILLIS = 1000;
    public static final int SAMPLING_DURATION = 250;
    ByteArrayOutputStream baos = new ByteArrayOutputStream(512000);
    byte[] buffer = new byte[8192];
    private ICanBusDataListener canBusDataListener;
    private HashMap<String, HeaderData> headerToDataMapping = new HashMap<>();
    private boolean isSampling = true;
    private long monitorDurationMilliseconds;
    private long monitorDurationNanos;
    private boolean monitoringFailed = false;
    private int numberOfSuccessSamples = 0;
    private boolean samplingFailed = false;

    class HeaderData {
        int count = 0;
        ArrayList<CANBusDataDescriptor> descriptorsList;
        String header;
        int interval;
        long lastProcessedTime = 0;
        int length;

        HeaderData() {
        }
    }

    public interface ICanBusDataListener {
        void onCanBusDataRead(int i, double d);
    }

    public CANBusDataProcessor(List<CANBusDataDescriptor> canBusDataDescriptors) {
        super("STM");
        for (CANBusDataDescriptor descriptor : canBusDataDescriptors) {
            HeaderData headerData;
            String header = descriptor.header;
            if (this.headerToDataMapping.containsKey(header)) {
                headerData = (HeaderData) this.headerToDataMapping.get(header);
            } else {
                headerData = new HeaderData();
                headerData.descriptorsList = new ArrayList(8);
                this.headerToDataMapping.put(header, headerData);
            }
            headerData.descriptorsList.add(descriptor);
            headerData.interval = descriptor.dataFrequency;
        }
    }

    protected void processCommandResponse(Protocol protocol, ObdCommandInterpreter obdCommandInterpreter, IObdDataObserver dataObserver) throws IOException {
        Throwable e;
        this.monitorDurationMilliseconds = this.isSampling ? 250 : 10000;
        this.monitorDurationNanos = (this.monitorDurationMilliseconds * 1000) * 1000;
        long start = System.nanoTime();
        Log.info("Starting the CAN bus monitoring");
        obdCommandInterpreter.setReadTimeOut(1000);
        this.baos.reset();
        int totalLength = 0;
        StringBuilder headerBuilder = new StringBuilder(3);
        int lineLength = 0;
        char[] dataArray = new char[32];
        resetHeaderStatsForSampling();
        while (System.nanoTime() - start < this.monitorDurationNanos) {
            try {
                int length = obdCommandInterpreter.read(this.buffer);
                if (length > 0) {
                    totalLength += length;
                    int i = 0;
                    int lineLength2 = lineLength;
                    while (i < length) {
                        try {
                            char ch = (char) this.buffer[i];
                            boolean validHexDigit = (ch >= '0' && ch <= '9') || ((ch >= 'A' && ch <= 'F') || (ch >= 'a' && ch <= 'f'));
                            if (!validHexDigit && ch != '\r' && ch != '\n') {
                                Log.debug("Received an invalid character, ending monitoring , Char");
                                lineLength = lineLength2;
                                break;
                            }
                            if (ch == '\r' || ch == '\n') {
                                if (headerBuilder.length() == 3 && lineLength2 > 5) {
                                    processData(headerBuilder.toString(), dataArray, lineLength2 - 3);
                                }
                                lineLength = 0;
                                headerBuilder.setLength(0);
                            } else if (lineLength2 < 3) {
                                headerBuilder.append(ch);
                                lineLength = lineLength2 + 1;
                            } else {
                                lineLength = lineLength2 + 1;
                                dataArray[lineLength2 - 3] = ch;
                            }
                            i++;
                            lineLength2 = lineLength;
                        } catch (Exception e3) {
                            e = e3;
                            lineLength = lineLength2;
                        }
                    }
                    lineLength = lineLength2;
                }
            } catch (InterruptedException e4) {
                e = e4;
            } catch (IOException e5) {
                e = e5;
            }
        }
        long elapsedTimeMillis = (System.nanoTime() - start) / 1000000;
        int interval;
        for (HeaderData headerData : this.headerToDataMapping.values()) {
            interval = headerData.count == 0 ? 0 : (int) (elapsedTimeMillis / ((long) headerData.count));
            Log.error("Header {}, Count {}, Freq :{}", headerBuilder.toString(), Integer.valueOf(headerData.count), Integer.valueOf(interval));
            if (interval != 0 || interval > headerData.interval) {
                if (this.isSampling) {
                    this.samplingFailed = true;
                } else {
                    this.monitoringFailed = true;
                }
            }
        }
        if (!this.samplingFailed) {
            this.numberOfSuccessSamples++;
        }
        boolean terminalClear = false;
        while (!terminalClear) {
            int i2 = 6;
            obdCommandInterpreter.write(new byte[]{(byte) 65, (byte) 84, (byte) 73, (byte) 73, (byte) 73, (byte) 13});
            Log.debug("Clearing the terminal");
            try {
                obdCommandInterpreter.clearTerminalBulk(false, true);
                terminalClear = true;
                Log.debug("Terminal clear");
            } catch (InterruptedException e6) {
                e6.printStackTrace();
                Log.debug("Interrupted while clearing terminal");
            } catch (TimeoutException e7) {
                Log.debug("Clearing the terminal timed out");
            }
        }
    }

    private void resetHeaderStatsForSampling() {
        for (HeaderData headerData : this.headerToDataMapping.values()) {
            headerData.count = 0;
        }
    }

    public void setCANBusDataListener(ICanBusDataListener canBusDataListener) {
        this.canBusDataListener = canBusDataListener;
    }

    private void processData(String header, char[] data, int dataLength) {
        if (this.headerToDataMapping.containsKey(header)) {
            HeaderData headerData = (HeaderData) this.headerToDataMapping.get(header);
            headerData.count++;
            if (!this.isSampling) {
                if (headerData.lastProcessedTime == 0 || System.currentTimeMillis() - headerData.lastProcessedTime > ((long) headerData.interval)) {
                    headerData.lastProcessedTime = System.currentTimeMillis();
                    List<CANBusDataDescriptor> descriptors = headerData.descriptorsList;
                    if (descriptors != null) {
                        for (CANBusDataDescriptor descriptor : descriptors) {
                            int byteOffset = descriptor.byteOffset;
                            int byteLength = descriptor.lengthInBits / 8;
                            if ((byteOffset + byteLength) * 2 < dataLength) {
                                byte[] byteData = parseHexArray(data, byteOffset * 2, byteLength * 2);
                                if (byteData != null) {
                                    double value;
                                    if (byteData.length == 1) {
                                        value = (double) (byteData[0] & 255);
                                    } else {
                                        value = (double) bytesToInt(byteData, 0, byteData.length, !descriptor.littleEndian);
                                    }
                                    this.canBusDataListener.onCanBusDataRead(descriptor.obdPid, value * descriptor.resolution);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    public static int bytesToInt(byte[] byteArray, int offset, int length, boolean bigEndian) {
        if (byteArray == null || length <= 0 || length > 4 || byteArray.length < offset + length) {
            throw new NumberFormatException("Incorrect data");
        }
        int data = 0;
        for (int i = 0; i < length; i++) {
            data += byteArray[offset + (bigEndian ? i : (length - i) - 1)] & 255;
            if (i < length - 1) {
                data <<= 8;
            }
        }
        return data;
    }

    public static byte[] parseHexArray(char[] hexArray, int position, int length) {
        byte[] byteData = new byte[(length / 2)];
        int i = position;
        int bytePosition = 0;
        while (i < position + length) {
            int bytePosition2 = bytePosition + 1;
            byteData[bytePosition] = (byte) ((HexUtil.parseHexDigit(hexArray[i]) << 4) + HexUtil.parseHexDigit(hexArray[i + 1]));
            i += 2;
            bytePosition = bytePosition2;
        }
        return byteData;
    }

    public void reset() {
        this.samplingFailed = false;
        this.monitoringFailed = false;
        this.numberOfSuccessSamples = 0;
        resetHeaderStatsForSampling();
    }

    public boolean isSamplingComplete() {
        return this.samplingFailed || this.numberOfSuccessSamples >= 3;
    }

    public boolean isSamplingSuccessful() {
        return !this.samplingFailed && this.numberOfSuccessSamples >= 3;
    }

    public void startMonitoring() {
        this.isSampling = false;
    }

    public boolean isMonitoringFailed() {
        return this.monitoringFailed;
    }

    protected long getCommandExecutionTimeOutInMillis() {
        return this.isSampling ? 10000 : 20000;
    }
}
