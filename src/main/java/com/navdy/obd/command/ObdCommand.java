package com.navdy.obd.command;

import com.navdy.obd.EcuResponse;
import com.navdy.obd.Protocol;
import com.navdy.obd.ResponseParser;
import com.navdy.obd.converters.AbstractConversion;
import com.navdy.obd.io.ObdCommandInterpreter;
import com.navdy.os.SystemProperties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class ObdCommand implements ICommand {
    static final Logger Log = LoggerFactory.getLogger(ObdCommand.class);
    public static final Boolean doLogRaw = SystemProperties.getBoolean("com.navdy.obd.ObdRaw", false);
    static final Logger LogRaw = LoggerFactory.getLogger("ObdRaw");
    public static final String OK = "OK";
    public static final String WAIT = "WAIT";
    public static final int ANY_ECU = -1;
    public static final int UNKNOWN = -1;
    public static final int NO_FILTER = 0;
    public static final long TIMEOUT_MILLS = 10000;
    private static final int CARRIAGE_RETURN = 13;
    public static final long NANOS_PER_MS = 1000000;
    public static final int NO_DATA = Integer.MIN_VALUE;

    public static final ObdCommand RESET_COMMAND = new ObdCommand("INIT", "atws");
    public static final ObdCommand ALLOW_LONG_MESSAGES = new ObdCommand("ATAL");
    public static final ObdCommand AUTO_PROTOCOL_COMMAND = new ObdCommand("SP", "atsp A6");
    public static final ObdCommand DEVICE_INFO_COMMAND = new ObdCommand("ATI", "ATI");
    public static final ObdCommand ECHO_OFF_COMMAND = new ObdCommand("ECHO OFF", "ate0");
    public static final ObdCommand HEADERS_OFF_COMMAND = new ObdCommand("HDR OFF", "ath0");
    public static final ObdCommand HEADERS_ON_COMMAND = new ObdCommand("HDR ON", "ath1");
    public static final ObdCommand SPACES_OFF_COMMAND = new ObdCommand("SPACE", "ats0");
    public static final ObdCommand SCAN_COMMAND = new ObdCommand("SCAN", "atma");
    public static final ObdCommand READ_PROTOCOL_COMMAND = new ObdCommand("DP", "atdpn");
    public static final ObdCommand TURN_OFF_FORMATTING_COMMAND = new ObdCommand("CAN Formatting OFF", "ATCAF0");
    public static final ObdCommand TURN_ON_FORMATTING_COMMAND = new ObdCommand("CAN Formatting ON", "ATCAF1");
    public static final ObdCommand SLEEP_COMMAND = new ObdCommand("STSLEEP");
    public static final ObdCommand CLEAR_ALL_PASS_FILTERS_COMMAND = new ObdCommand("STFCP");
    public static final ObdCommand TURN_OFF_VOLTAGE_DELTA_WAKEUP = new ObdCommand("STSLVG off");
    public static final ObdCommand TURN_OFF_VOLTAGE_LEVEL_WAKEUP = new ObdCommand("STSLVL off,off");
    public static final ObdCommand TURN_ON_VOLTAGE_DELTA_WAKEUP = new ObdCommand("STSLVG on");
    public static final ObdCommand TURN_ON_VOLTAGE_LEVEL_WAKEUP = new ObdCommand("STSLVL off,on");
    public static final ObdCommand DETECT_PROTOCOL_COMMAND = new ObdCommand("Detect", "0100");

    private static ExecutorService singleThreadedExecutor = Executors.newSingleThreadExecutor();
    private String command;
    private byte[] commandBytes;
    private AbstractConversion conversion;
    private IOException error;
    private int expectedResponses;
    private String name;
    private String response;
    private EcuResponse[] responses;
    private int targetEcu;
    private long timeoutMillis;
    private long timeoutNanos;

    static final Map<Integer, String> ObdErrors = createErrors();
    private static Map<Integer, String> createErrors() {
        Map<Integer, String> result = new HashMap<Integer, String>();
        result.put(0x10, "generalReject");
        result.put(0x11, "serviceNotSupported");
        result.put(0x12, "subFunctionNotSupported-invalidFormat");
        result.put(0x21, "busy-RepeatRequest");
        result.put(0x22, "conditionsNotCorrect or requestSequenceError");
        result.put(0x23, "routineNotComplete");
        result.put(0x31, "requestOutOfRange");
        result.put(0x33, "securityAccessDenied");
        result.put(0x35, "invalidKey");
        result.put(0x36, "exceedNumberOfAttempts");
        result.put(0x37, "requiredTimeDelayNotExpired");
        result.put(0x40, "downloadNotAccepted");
        result.put(0x41, "improperDownloadType");
        result.put(0x42, "can'tDownloadToSpecifiedAddress");
        result.put(0x43, "can'tDownloadNumberOfBytesRequested");
        result.put(0x50, "uploadNotAccepted");
        result.put(0x51, "improperUploadType");
        result.put(0x52, "can'tUploadFromSpecifiedAddress");
        result.put(0x53, "can'tUploadNumberOfBytesRequested");
        result.put(0x71, "transferSuspended");
        result.put(0x72, "transferAborted");
        result.put(0x74, "illegalAddressInBlockTransfer");
        result.put(0x75, "illegalByteCountInBlockTransfer");
        result.put(0x76, "illegalBlockTransferType");
        result.put(0x77, "blockTransferDataChecksumError");
        result.put(0x78, "requestCorrectlyReceived-ResponsePending");
        result.put(0x79, "incorrectByteCountDuringBlockTransfer");
        return result;
    }

    public static ObdCommand createSetVoltageLevelWakeupTriggerCommand(boolean below, float voltage, int seconds) {
        return new ObdCommand("STSLVLW " + (below ? "<" : ">") + Float.toString(voltage) + ", " + Integer.toString(seconds));
    }

    public ObdCommand(String name, String command, AbstractConversion conversion, long timeOut) {
        this(name, command, conversion);
        this.timeoutMillis = timeOut;
        this.timeoutNanos = 1000000 * timeOut;
    }

    public ObdCommand(String name, String command, AbstractConversion conversion) {
        this.expectedResponses = -1;
        this.targetEcu = -1;
        this.timeoutMillis = 10000;
        this.timeoutNanos = 10000000000L;
        this.name = name;
        this.conversion = conversion;
        setCommand(command);
    }

    public ObdCommand(String command) {
        this(null, null, null);
        String[] scom = command.split(":");
        String pid;
        if (scom.length == 1) {
            this.name = pid = command.trim();
        } else {
            this.name = scom[0].trim();
            pid = scom[1].trim(); 
        }
        setCommand(pid);
    }

    public ObdCommand(String name, String command) {
        this(name, command, null);
    }

    public void setCommand(String command) {
        this.command = command;
        this.commandBytes = null;
    }

    public ObdCommand clone() {
        return new ObdCommand(this.name, this.command, this.conversion, this.timeoutMillis);
    }

    public String getCommand() {
        return this.command;
    }

    public void setExpectedResponses(int count) {
        if (count > 9 || count == 0 || count < -1) {
            throw new IllegalArgumentException("invalid expected count " + count);
        }
        this.expectedResponses = count;
        this.commandBytes = null;
    }

    public void setTargetEcu(int ecuAddress) {
        if (ecuAddress != this.targetEcu) {
            this.targetEcu = ecuAddress;
        }
    }

    public int getTargetEcu() {
        return this.targetEcu;
    }

    public String getName() {
        return this.name;
    }

    public byte[] getByteResponse() {
        return getByteResponse(0);
    }

    public byte[] getByteResponse(int filter) {
        return getEcuResponse(this.targetEcu, filter);
    }

    public byte[] getEcuResponse(int ecu) {
        return getEcuResponse(ecu, 0);
    }

    public byte[] getEcuResponse(int ecu, int filter) {
        if (this.responses == null) {
            return null;
        }
        for (EcuResponse response : this.responses) {
            if (ecu == -1 || response.ecu == ecu) {
                byte[] data = response.data;
                if (filter == 0) {
                    return data;
                }
                if (data != null && data[0] == filter) {
                    return data;
                }
            }
        }
        return null;
    }

    public EcuResponse getResponse(int i) {
        return this.responses[i];
    }

    public int getResponseCount() {
        return this.responses != null ? this.responses.length : 0;
    }

    public String getResponse() {
        return this.response;
    }

    public void execute(InputStream input, OutputStream output, IObdDataObserver commandObserver) throws IOException {
        execute(input, output, null, commandObserver);
    }

    public void execute(InputStream input, OutputStream output, Protocol protocol, IObdDataObserver commandObserver) throws IOException {
        this.error = null;
        final InputStream inputStream = input;
        final OutputStream outputStream = output;
        final IObdDataObserver iObdDataObserver = commandObserver;
        final Protocol protocol2 = protocol;
        Future future = singleThreadedExecutor.submit(new Runnable() {
            public void run() {
                try {
                    ObdCommandInterpreter obdCommandInterpreter = new ObdCommandInterpreter(inputStream, outputStream);
                    ObdCommand.this.responses = null;
                    ObdCommand.this.response = null;
                    if (ObdCommand.this.commandBytes == null) {
                        obdCommandInterpreter.write(ObdCommand.this.command + (ObdCommand.this.expectedResponses != -1 ? Character.valueOf((char) (ObdCommand.this.expectedResponses + 48)) : ""));
                        ObdCommand.this.commandBytes = obdCommandInterpreter.getCommandBytes();
                    } else {
                        obdCommandInterpreter.write(ObdCommand.this.commandBytes);
                    }
                    ObdCommand.Log.debug("wrote: {}{} ", ObdCommand.this.command, ObdCommand.this.expectedResponses != -1 ? Integer.valueOf(ObdCommand.this.expectedResponses) : "");
                    if (doLogRaw) {
                        LogRaw.info("wrote: {}{} ", ObdCommand.this.command, ObdCommand.this.expectedResponses != -1 ? Integer.valueOf(ObdCommand.this.expectedResponses) : "");
                    }
                    if (iObdDataObserver != null) {
                        iObdDataObserver.onCommand(new String(ObdCommand.this.commandBytes));
                    }
                    ObdCommand.this.processCommandResponse(protocol2, obdCommandInterpreter, iObdDataObserver);
                } catch (IOException ie) {
                    ObdCommand.this.error = ie;
                    if (iObdDataObserver != null) {
                        iObdDataObserver.onError(ie.toString());
                    }
                }
            }
        });
        try {
            future.get(getCommandExecutionTimeOutInMillis(), TimeUnit.MILLISECONDS);
            if (this.error != null) {
                throw this.error;
            }
        } catch (InterruptedException e) {
            Log.error("Trying to cancel the task, Success : " + future.cancel(true));
            singleThreadedExecutor.shutdownNow();
            singleThreadedExecutor = Executors.newSingleThreadExecutor();
            throw new IOException("Command IO thread was interrupted", e);
        } catch (ExecutionException e2) {
            throw new IOException(e2.getCause());
        } catch (TimeoutException e3) {
            Log.error("Trying to cancel the task, Success : " + future.cancel(true));
            singleThreadedExecutor.shutdownNow();
            singleThreadedExecutor = Executors.newSingleThreadExecutor();
            throw new IOException(e3);
        }
    }

    protected void processCommandResponse(Protocol protocol, ObdCommandInterpreter obdCommandInterpreter, IObdDataObserver dataObserver) throws IOException {
        String response;
        if (SLEEP_COMMAND == this) {
            Log.info("Reading response of Sleep command ");
            try {
                response = obdCommandInterpreter.readLine();
                if (response == null || response.length() <= 0) {
                    Log.info("Empty response for SLEEP command");
                    throw new IOException("Invalid response while putting chip to sleep");
                }
                Log.info("Response {}", response.trim());
                if (response.trim().equals(OK)) {
                    return;
                }
            } catch (Throwable e) {
                Log.error("Interrupted while reading response for sleep command", e);
                return;
            }
        }
        StringBuilder responseBuilder = new StringBuilder();
        try {
            response = obdCommandInterpreter.readResponse();
            if (doLogRaw) {
                LogRaw.info("raw read: {} ", response);
            }
            boolean textResponse = obdCommandInterpreter.didGetTextResponse();
            responseBuilder.append(response);
            if (this == RESET_COMMAND) {
                Log.info("Reset Command, Clearing the terminal");
                try {
                    responseBuilder.append(obdCommandInterpreter.clearTerminal());
                    textResponse = obdCommandInterpreter.didGetTextResponse();
                } catch (TimeoutException e3) {
                    Log.error("Clearing terminal timed out");
                    return;
                } catch (Throwable e2) {
                    Log.error("Interrupted while clearing the terminal ", e2);
                    return;
                }
            }
            this.response = responseBuilder.toString();
            if (dataObserver != null) {
                dataObserver.onResponse(this.response);
            }
            this.responses = ResponseParser.parse(textResponse, this.response, protocol);
    
            if (this.responses != null) {
                for (EcuResponse rsp : this.responses) {
                    int ecu = rsp.ecu;
                    byte[] data = rsp.data;

                    if (doLogRaw) {
                        LogRaw.info("read: ecu={}, data={} ", ecu, data);
                    }
                    if (doLogRaw) {
                        if (data.length >= 3) {
                            if (data[0] == 0x7F) {
                                if (ObdErrors.containsKey(data[2])) {
                                    LogRaw.info("Error: code=0x{} err={}", Integer.toHexString(data[1]), ObdErrors.get(data[2]));
                                }
                                LogRaw.info("Unknown Error: code=0x{}", Integer.toHexString(data[1]));
                            }
                        }
                    }
                }
            }

        } catch (InterruptedException e4) {
            Log.error("Exception: ", e4);
        }
    }

    protected long getCommandExecutionTimeOutInMillis() {
        return this.timeoutMillis;
    }

    public double getDoubleValue() {
        byte[] data = getByteResponse();
        if (data != null) {
            try {
                if (data[0] == (byte) 65 && this.conversion != null) {
                    return this.conversion.convert(data);
                }
            } catch (Throwable e) {
                Log.debug("Exception converting " + new String(data), e);
            }
        }
        return -2.147483648E9d;
    }

    public int getIntValue() {
        return (int) Math.round(getDoubleValue());
    }
}
