package com.navdy.obd.command;

import android.text.TextUtils;
import com.navdy.obd.Protocol;
import com.navdy.os.SystemProperties;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class InitializeCommand extends BaseSTNInitializeCommand {
    static final Logger Log = LoggerFactory.getLogger(ValidPidsCommand.class);

    public InitializeCommand(Protocol protocol, List<String> commands, String initcmd) {
        super();
        Boolean add_protocol = true;
        if ((commands == null) || (commands.size() == 0)) {
            Log.info("No profile init, using default");

            add(ObdCommand.TURN_OFF_VOLTAGE_DELTA_WAKEUP);
            add(ObdCommand.RESET_COMMAND);
            add(ObdCommand.ECHO_OFF_COMMAND);
            add(ObdCommand.HEADERS_ON_COMMAND);
            add(ObdCommand.SPACES_OFF_COMMAND);
            add(ObdCommand.TURN_ON_FORMATTING_COMMAND);
            add(ObdCommand.TURN_OFF_VOLTAGE_LEVEL_WAKEUP);
            add(ObdCommand.ALLOW_LONG_MESSAGES);
        } else {
            Log.info("Using profile init");
            for (String com : commands) {
                ObdCommand command = new ObdCommand(com);
                if ((command.getCommand().length() > 4) && 
                    (command.getCommand().toUpperCase().substring(0,4)).equals("ATSP")) {
                    add_protocol = false;
                }
                add(command);
            }            
        }
        if (add_protocol) {
            if (protocol != null) {
                add(new ObdCommand("SetProtocol", "atsp A" + Integer.toHexString(protocol.id)));
            } else {
                add(ObdCommand.AUTO_PROTOCOL_COMMAND);
            }
        }
        if (TextUtils.isEmpty(initcmd)) {
            add(ObdCommand.DETECT_PROTOCOL_COMMAND);    
        } else {
            add(new ObdCommand(initcmd));
        }
        
        add(ObdCommand.READ_PROTOCOL_COMMAND);
    }

    public String getProtocol() {
        return ObdCommand.READ_PROTOCOL_COMMAND.getResponse();
    }
}
