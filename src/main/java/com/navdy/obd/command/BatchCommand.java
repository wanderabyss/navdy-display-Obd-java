package com.navdy.obd.command;

import com.navdy.obd.Protocol;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

public class BatchCommand implements ICommand {
    protected ArrayList<ICommand> commands;

    public BatchCommand(ICommand... commands) {
        this(Arrays.asList(commands));
    }

    public BatchCommand(List<ICommand> commands) {
        this.commands = new ArrayList<>();
        this.commands.addAll(commands);
    }

    public void add(ICommand command) {
        this.commands.add(command);
    }

    public List<ICommand> getCommands() {
        return this.commands;
    }

    public String getName() {
        StringBuilder names = new StringBuilder("batch(");
        Iterator it = this.commands.iterator();
        while (it.hasNext()) {
            names.append(((ICommand) it.next()).getName());
            names.append(",");
        }
        names.append(")");
        return names.toString();
    }

    public void execute(InputStream input, OutputStream output, Protocol protocol, IObdDataObserver commandObserver) throws IOException {
        Iterator it = this.commands.iterator();
        while (it.hasNext()) {
            ((ICommand) it.next()).execute(input, output, protocol, commandObserver);
        }
    }

    public double getDoubleValue() {
        return 0.0d;
    }

    public int getIntValue() {
        return 0;
    }

    public String getResponse() {
        StringBuilder responses = new StringBuilder();
        Iterator it = this.commands.iterator();
        while (it.hasNext()) {
            String response = ((ICommand) it.next()).getResponse();
            responses.append(response != null ? response.trim() : null);
            responses.append(",");
        }
        return responses.toString();
    }
}
