package com.navdy.obd;

import android.os.Parcel;
import android.os.Parcelable;

public class ECU implements Parcelable {
    public static final Creator<ECU> CREATOR = new Creator<ECU>() {
        public ECU createFromParcel(Parcel source) {
            return new ECU(source);
        }

        public ECU[] newArray(int size) {
            return new ECU[size];
        }
    };
    public final int address;
    public final PidSet supportedPids;

    public ECU(int address, PidSet supportedPids) {
        this.address = address;
        this.supportedPids = supportedPids;
    }

    public int describeContents() {
        return 0;
    }

    public ECU(Parcel in) {
        this.address = in.readInt();
        this.supportedPids = in.readParcelable(PidSet.class.getClassLoader());
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.address);
        dest.writeParcelable(this.supportedPids, 0);
    }
}
