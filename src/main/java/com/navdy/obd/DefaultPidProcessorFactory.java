package com.navdy.obd;

import java.util.ArrayList;
import java.util.List;

public class DefaultPidProcessorFactory implements PidProcessorFactory {
    private static List<Integer> PIDS_WITH_PRE_PROCESSORS = new ArrayList<>();

    static {
        PIDS_WITH_PRE_PROCESSORS.add(Pids.INSTANTANEOUS_FUEL_CONSUMPTION);
        PIDS_WITH_PRE_PROCESSORS.add(Pids.FUEL_LEVEL);
        PIDS_WITH_PRE_PROCESSORS.add(Pids.VEHICLE_SPEED);
        PIDS_WITH_PRE_PROCESSORS.add(Pids.AMBIENT_AIR_TEMRERATURE);
        PIDS_WITH_PRE_PROCESSORS.add(Pids.ENGINE_COOLANT_TEMPERATURE);
        PIDS_WITH_PRE_PROCESSORS.add(Pids.ENGINE_OIL_TEMRERATURE);
    }

    public PidProcessor buildPidProcessorForPid(int pid) {
        switch (pid) {
            case Pids.VEHICLE_SPEED:
                return new SpeedPidProcessor();
            case Pids.AMBIENT_AIR_TEMRERATURE:
                return new AmbientTempPidProcessor();
            case Pids.ENGINE_COOLANT_TEMPERATURE:
                return new EngineCoolantPidProcessor();
            case Pids.ENGINE_OIL_TEMRERATURE:
                return new EngineOilTempPidProcessor();
            case Pids.FUEL_LEVEL:
                return new FuelLevelPidProcessor();
            case Pids.INSTANTANEOUS_FUEL_CONSUMPTION:
                return new InstantFuelConsumptionPidProcessor();
            default:
                return null;
        }
    }

    public List<Integer> getPidsHavingProcessors() {
        return PIDS_WITH_PRE_PROCESSORS;
    }
}
