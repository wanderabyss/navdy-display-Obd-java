package com.navdy.obd;

import com.navdy.obd.util.HexUtil;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ch.qos.logback.core.net.SyslogConstants;

public class ResponseParser {
    private static final char MULTI_LINE_CONTINUED = '2';
    private static final char MULTI_LINE_START = '1';
    private static final char SINGLE_LINE = '0';
    private static Pattern groupPattern = Pattern.compile("([0-9A-F]:)((?:[0-9A-F]{2})+)\\s");
    private static Pattern multiLineResponse = Pattern.compile("^([0-9A-F]{3})\\s?0:");

    public static EcuResponse[] parse(boolean textResponse, String response, Protocol protocol) {
        Matcher matcher = multiLineResponse.matcher(response);
        EcuResponse[] result;
        if (matcher.find()) {
            result = new EcuResponse[1];
            byte[] data = new byte[Integer.parseInt(matcher.group(1), 16)];
            int offset = 0;
            Matcher groupMatcher = groupPattern.matcher(response);
            while (groupMatcher.find()) {
                String group = groupMatcher.group(2);
                int bytes = group.length() / 2;
                HexUtil.parseHexString(group, data, offset, bytes);
                offset += bytes;
            }
            result[0] = new EcuResponse(0, data);
            return result;
        } else if (textResponse) {
            return null;
        } else {
            String[] lines = response.split("\r");
            if (protocol == null) {
                result = new EcuResponse[1];
                String data2 = join(lines);
                result[0] = new EcuResponse(0, true, data2.length() / 2);
                result[0].append(data2);
                return result;
            }
            int i;
            List<EcuResponse> responses = new ArrayList<>();
            FrameFormat format = protocol.format;
            for (String line : lines) {
                int ecu = Integer.parseInt(line.substring(format.ecuStart, format.ecuEnd), 16);
                switch (protocol.id) {
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                        parseIsoFrame(ecu, line, format.ecuEnd, responses);
                        break;
                    default:
                        parseCanFrame(ecu, line, format.ecuEnd, responses);
                        break;
                }
            }
            if (responses.size() <= 0) {
                return null;
            }
            result = new EcuResponse[responses.size()];
            for (i = 0; i < result.length; i++) {
                EcuResponse ecuResponse = responses.get(i);
                ecuResponse.flatten();
                result[i] = ecuResponse;
            }
            return result;
        }
    }

    private static void parseIsoFrame(int ecu, String line, int offset, List<EcuResponse> responses) {
        EcuResponse response = findEcuResponse(ecu, responses);
        if (response == null) {
            response = new EcuResponse(ecu);
            responses.add(response);
        }
        response.addFrame(line.substring(offset));
    }

    private static void parseCanFrame(int ecu, String line, int offset, List<EcuResponse> responses) {
        int lenFieldLen = 1;
        int offset2 = offset + 1;
        char frameFlag = line.charAt(offset);
        switch (frameFlag) {
            case SyslogConstants.LOG_LPR /*48*/:
            case Pids.DISTANCE_TRAVELLED /*49*/:
                boolean multiline = frameFlag == MULTI_LINE_START;
                if (multiline) {
                    lenFieldLen = 3;
                }
                int len = Integer.parseInt(line.substring(offset2, offset2 + lenFieldLen), 16);
                offset = offset2 + lenFieldLen;
                EcuResponse ecuResponse = new EcuResponse(ecu, multiline, len);
                ecuResponse.append(line.substring(offset, Math.min((len * 2) + offset, line.length())));
                responses.add(ecuResponse);
                return;
            case '2':
                offset = offset2 + 1;
                EcuResponse existingResponse = findEcuResponse(ecu, responses);
                if (existingResponse == null) {
                    throw new IllegalStateException("missing starting ecu response");
                }
                existingResponse.append(line.substring(offset));
                return;
            default:
                throw new IllegalStateException("invalid frame flag");
        }
    }

    private static EcuResponse findEcuResponse(int ecu, List<EcuResponse> responses) {
        for (int i = 0; i < responses.size(); i++) {
            EcuResponse response = responses.get(i);
            if (response.ecu == ecu) {
                return response;
            }
        }
        return null;
    }

    private static String join(String[] strings) {
        if (strings == null || strings.length == 0) {
            return "";
        }
        StringBuilder builder = new StringBuilder();
        for (String append : strings) {
            builder.append(append);
        }
        return builder.toString();
    }
}
