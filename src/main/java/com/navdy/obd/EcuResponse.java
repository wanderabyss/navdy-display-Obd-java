package com.navdy.obd;

import com.navdy.obd.util.HexUtil;
import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

public class EcuResponse {
    public byte[] data;
    public final int ecu;
    private List<byte[]> frames;
    public int length;
    public boolean multiline;
    private int offset = 0;

    public EcuResponse(int ecu, boolean multiline, int length) {
        this.ecu = ecu;
        this.length = length;
        this.multiline = multiline;
    }

    public EcuResponse(int ecu, byte[] data) {
        this.ecu = ecu;
        this.data = data;
        this.multiline = true;
        this.length = data.length;
    }

    public EcuResponse(int ecu) {
        this.ecu = ecu;
        this.multiline = false;
    }

    public void addFrame(String hexString) {
        if (this.frames == null) {
            this.frames = new ArrayList<>();
        }
        byte[] bytes = new byte[(hexString.length() / 2)];
        HexUtil.parseHexString(hexString, bytes, 0, bytes.length);
        this.frames.add(bytes);
    }

    void flatten() {
        if (this.frames != null && this.frames.size() != 0) {
            if (this.frames.size() == 1) {
                this.data = this.frames.get(0);
            } else {
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                for (int i = 0; i < this.frames.size(); i++) {
                    byte[] frame = this.frames.get(i);
                    if (i == 0) {
                        stream.write(frame, 0, 2);
                    }
                    stream.write(frame, 3, frame.length - 4);
                }
                this.data = stream.toByteArray();
            }
            this.length = this.data.length;
        }
    }

    public void append(String hexString) {
        if (this.data == null) {
            this.data = new byte[this.length];
        }
        int srcLen = hexString.length() / 2;
        HexUtil.parseHexString(hexString, this.data, this.offset, Math.min(srcLen, this.length - this.offset));
        this.offset += srcLen;
    }
}
