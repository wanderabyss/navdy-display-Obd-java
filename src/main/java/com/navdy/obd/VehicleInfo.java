package com.navdy.obd;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class VehicleInfo implements Parcelable {
    public static final Creator<VehicleInfo> CREATOR = new Creator<VehicleInfo>() {
        public VehicleInfo createFromParcel(Parcel source) {
            return new VehicleInfo(source);
        }

        public VehicleInfo[] newArray(int size) {
            return new VehicleInfo[size];
        }
    };
    public final List<ECU> ecus;
    public boolean isCheckEngineLightOn;
    private ECU primaryEcu;
    public final String protocol;
    public List<String> troubleCodes;
    public final String vin;

    public VehicleInfo(String vin) {
        this(null, null, vin, false, null);
    }

    public VehicleInfo(String protocol, List<ECU> ecus, String vin, boolean isCheckEngineLightOn, List<String> troubleCodes) {
        this.protocol = protocol;
        this.ecus = ecus != null ? Collections.unmodifiableList(ecus) : null;
        this.vin = vin;
        this.isCheckEngineLightOn = isCheckEngineLightOn;
        this.troubleCodes = troubleCodes;
    }

    public int describeContents() {
        return 0;
    }

    public VehicleInfo(Parcel in) {
        List<ECU> ecus = new ArrayList<>();
        this.protocol = in.readString();
        this.vin = in.readString();
        in.readList(ecus, null);
        this.ecus = Collections.unmodifiableList(ecus);
        this.isCheckEngineLightOn = in.readByte() != (byte) 0;
        List<String> troubleCodes = new ArrayList<>();
        in.readList(troubleCodes, null);
        this.troubleCodes = Collections.unmodifiableList(troubleCodes);
    }

    public ECU getPrimaryEcu() {
        if (this.primaryEcu == null) {
            int bestPidCount = 0;
            ECU bestEcu = null;
            for (ECU ecu : this.ecus) {
                int pidCount = ecu.supportedPids.size();
                if (pidCount > bestPidCount && ecu.supportedPids.contains(13)) {
                    bestEcu = ecu;
                    bestPidCount = pidCount;
                }
            }
            this.primaryEcu = bestEcu;
        }
        return this.primaryEcu;
    }

    public List<ECU> getEcus() {
        return this.ecus;
    }

    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.protocol);
        dest.writeString(this.vin);
        dest.writeList(this.ecus);
        dest.writeByte((byte) (this.isCheckEngineLightOn ? 1 : 0));
        dest.writeList(this.troubleCodes);
    }
}
