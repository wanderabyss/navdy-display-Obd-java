package com.navdy.obd;

import com.navdy.obd.command.IObdDataObserver;
import com.navdy.obd.command.ObdCommand;
import com.navdy.obd.io.IChannel;
import java.io.IOException;
import java.io.InputStream;

public class ObdScanJob extends ObdJob {
    private IRawData listener;
    private boolean shuttingDown = false;

    public interface IRawData {
        void onRawData(String str);
    }

    public ObdScanJob(IChannel channel, Protocol protocol, IRawData listener, IObdDataObserver commandObserver) {
        super(ObdCommand.SCAN_COMMAND, channel, protocol, commandObserver);
        this.listener = listener;
    }

    public void cancel() {
        this.shuttingDown = true;
    }

    public void run() {
        Log.debug("Staring scan");
        InputStream input = null;
        try {
            input = this.channel.getInputStream();
            this.command.execute(input, this.channel.getOutputStream(), this.protocol, this.commandObserver);
        } catch (IOException e) {
            Log.error("Failed executing command {}", this.command.getName());
        }
        if (input != null) {
            byte[] buffer = new byte[80];
            int bytesRead = 0;
            while (!this.shuttingDown && bytesRead >= 0) {
                try {
                    if (input.available() > 0) {
                        bytesRead = input.read(buffer);
                        if (bytesRead > 0) {
                            String data = new String(buffer, 0, bytesRead);
                            Log.debug("Scanned - {}", data);
                            this.listener.onRawData(data);
                        }
                    } else {
                        Thread.sleep(10);
                    }
                } catch (Throwable e2) {
                    Log.debug("Exception while scanning", e2);
                }
            }
        }
    }
}
