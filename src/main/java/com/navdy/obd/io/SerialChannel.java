package com.navdy.obd.io;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import com.navdy.hardware.SerialPort;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.ByteBuffer;

public class SerialChannel implements IChannel {
    public static final int DEFAULT_BAUD_RATE = 9600;
    private static final String DEFAULT_BAUD_RATE_PROP = "persist.sys.obd.baudrate";
    public static final String SERIAL_SERIVCE = "serial";
    private static final String TAG = "SerialChannel";
    private int baudRate;
    private final Context context;
    private SerialPort serialPort;
    private final IChannelSink sink;
    private int state;

    private static class SerialInputStream extends InputStream {
        SerialPort serialPort;

        public SerialInputStream(SerialPort serialPort) {
            this.serialPort = serialPort;
        }

        public int read() throws IOException {
            ByteBuffer buffer = ByteBuffer.allocate(1);
            int result = this.serialPort.read(buffer);
            if (result < 0) {
                return result;
            }
            if (result == 0) {
                return -2;
            }
            return buffer.get() & 255;
        }

        public int read(byte[] b) throws IOException {
            ByteBuffer buffer = ByteBuffer.allocate(b.length);
            int length = this.serialPort.read(buffer);
            if (length > 0) {
                buffer.get(b, 0, length);
            }
            return length;
        }
    }

    private static class SerialOutputStream extends OutputStream {
        SerialPort serialPort;

        public SerialOutputStream(SerialPort serialPort) {
            this.serialPort = serialPort;
        }

        public void write(int oneByte) throws IOException {
            this.serialPort.write(ByteBuffer.wrap(new byte[]{(byte) oneByte}), 1);
        }

        public void write(@NonNull byte[] buffer, int offset, int count) throws IOException {
            this.serialPort.write(ByteBuffer.wrap(buffer, offset, count), count);
        }

        public void write(byte[] b) throws IOException {
            this.serialPort.write(ByteBuffer.wrap(b), b.length);
        }
    }

    public SerialChannel(Context context, IChannelSink sink) {
        this(context, sink, DEFAULT_BAUD_RATE);
    }

    public SerialChannel(Context context, IChannelSink sink, int baudRate) {
        this.state = 0;
        this.context = context;
        this.sink = sink;
        this.baudRate = baudRate;
    }

    public int getState() {
        return this.state;
    }

    private void setState(int newState) {
        if (newState != this.state) {
            this.state = newState;
            if (this.sink != null) {
                this.sink.onStateChange(newState);
            }
        }
    }

    public void connect(String address) {
        try {
            this.serialPort = new SerialPort(address);
            if (this.serialPort.open(this.baudRate)) {
                setState(2);
            }
        } catch (IOException e) {
            Log.e(TAG, "Failed to get fd for " + address, e);
        }
    }

    public void disconnect() {
        try {
            if (this.serialPort != null) {
                this.serialPort.close();
            }
            setState(0);
        } catch (IOException e) {
            Log.e(TAG, "Failed to close underlying file", e);
        }
    }

    public InputStream getInputStream() {
        return new SerialInputStream(this.serialPort);
    }

    public OutputStream getOutputStream() {
        return new SerialOutputStream(this.serialPort);
    }

    public SerialPort getSerialPort() {
        return this.serialPort;
    }
}
