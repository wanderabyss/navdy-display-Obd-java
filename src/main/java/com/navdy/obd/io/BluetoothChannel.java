package com.navdy.obd.io;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.util.Log;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

public class BluetoothChannel implements IChannel {
    private static final boolean D = true;
    private static final UUID SERIAL_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private static final String TAG = BluetoothChannel.class.getSimpleName();
    private final BluetoothAdapter mAdapter = BluetoothAdapter.getDefaultAdapter();
    private ConnectThread mConnectThread;
    private final IChannelSink mSink;
    private BluetoothSocket mSocket;
    private int mState = 0;

    private class ConnectThread extends Thread {
        private final BluetoothDevice mmDevice;
        private BluetoothSocket mmSocket;

        public ConnectThread(BluetoothDevice device) {
            this.mmDevice = device;
            BluetoothSocket tmp = null;
            try {
                tmp = device.createRfcommSocketToServiceRecord(BluetoothChannel.SERIAL_UUID);
            } catch (IOException e) {
                Log.e(BluetoothChannel.TAG, "create() failed", e);
            }
            this.mmSocket = tmp;
        }

        public void run() {
            Log.i(BluetoothChannel.TAG, "BEGIN mConnectThread");
            setName("ConnectThread");
            try {
                this.mmSocket.connect();
            } catch (IOException e) {
                Log.e(BluetoothChannel.TAG, "Exception connecting", e);
                boolean fallbackFailed = BluetoothChannel.D;
                try {
                    Log.e(BluetoothChannel.TAG, "trying fallback...");
                    this.mmSocket = (BluetoothSocket) this.mmDevice.getClass().getMethod("createRfcommSocket", Integer.TYPE).invoke(this.mmDevice, new Object[]{1});
                    this.mmSocket.connect();
                    fallbackFailed = false;
                    Log.e(BluetoothChannel.TAG, "Connected");
                } catch (Exception e1) {
                    Log.e(BluetoothChannel.TAG, "Failed to connect using fallback", e1);
                }
                if (fallbackFailed) {
                    BluetoothChannel.this.connectionFailed();
                    try {
                        this.mmSocket.close();
                        return;
                    } catch (IOException e2) {
                        Log.e(BluetoothChannel.TAG, "unable to close() socket during connection failure", e2);
                        return;
                    }
                }
            }
            synchronized (BluetoothChannel.this) {
                BluetoothChannel.this.mConnectThread = null;
            }
            BluetoothChannel.this.connected(this.mmSocket, this.mmDevice);
        }

        public void cancel() {
            try {
                this.mmSocket.close();
            } catch (IOException e) {
                Log.e(BluetoothChannel.TAG, "close() of connect socket failed", e);
            }
        }
    }

    public BluetoothChannel(IChannelSink mSink) {
        this.mSink = mSink;
    }

    private synchronized void setState(int state) {
        Log.d(TAG, "setState() " + this.mState + " -> " + state);
        this.mState = state;
        this.mSink.onStateChange(state);
    }

    public synchronized int getState() {
        return this.mState;
    }

    public synchronized void connect(String address) {
        BluetoothDevice device = this.mAdapter.getRemoteDevice(address);
        Log.d(TAG, "connect to: " + device);
        if (this.mState == 1 && this.mConnectThread != null) {
            this.mConnectThread.cancel();
            this.mConnectThread = null;
        }
        this.mConnectThread = new ConnectThread(device);
        this.mConnectThread.start();
        setState(1);
    }

    public synchronized void disconnect() {
        stop();
    }

    public synchronized void connected(BluetoothSocket socket, BluetoothDevice device) {
        Log.d(TAG, "connected");
        if (this.mConnectThread != null) {
            this.mConnectThread.cancel();
            this.mConnectThread = null;
        }
        this.mSocket = socket;
        this.mSink.onMessage("Connected to " + device.getName());
        setState(2);
    }

    public synchronized void stop() {
        Log.d(TAG, "stop");
        if (this.mConnectThread != null) {
            this.mConnectThread.cancel();
            this.mConnectThread = null;
        }
        if (this.mSocket != null) {
            BluetoothSocket bluetoothSocket = this.mSocket;
            try {
                bluetoothSocket.close();
                this.mSocket = bluetoothSocket;
            } catch (IOException e) {
                Log.e(TAG, "Failed to close bluetooth socket", e);
                this.mSocket = bluetoothSocket;
                setState(0);
            } finally {
                this.mSocket = null;
            }
        }
        setState(0);
    }

    private void connectionFailed() {
        this.mSink.onMessage("Unable to connect device");
        setState(0);
    }

    public InputStream getInputStream() throws IOException {
        return this.mSocket.getInputStream();
    }

    public OutputStream getOutputStream() throws IOException {
        return this.mSocket.getOutputStream();
    }
}
