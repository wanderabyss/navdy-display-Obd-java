package com.navdy.obd.io;

public class ChannelInfo {
    private static final String SEPARATOR = "|";
    private static final String SEPARATOR_REGEX = "\\|";
    private String address;
    private boolean bonded;
    private ConnectionType connectionType;
    private String name;

    public enum ConnectionType {
        SERIAL,
        BLUETOOTH,
        MOCK,
        NONE
    }

    public ChannelInfo(ConnectionType connectionType, String name, String address) {
        this(connectionType, name, address, false);
    }

    public ChannelInfo(ConnectionType connectionType, String name, String address, boolean bonded) {
        this.bonded = false;
        this.connectionType = connectionType;
        this.name = name;
        this.address = address;
        this.bonded = bonded;
    }

    public static ChannelInfo parse(String string) {
        if (string == null) {
            return null;
        }
        String[] parts = string.split(SEPARATOR_REGEX);
        if (parts.length == 3) {
            return new ChannelInfo(ConnectionType.valueOf(parts[0]), parts[1], parts[2]);
        }
        return null;
    }

    public ConnectionType getConnectionType() {
        return this.connectionType;
    }

    public String getName() {
        return this.name;
    }

    public String getAddress() {
        return this.address;
    }

    public boolean isBonded() {
        return this.bonded;
    }

    public String asString() {
        return this.connectionType + SEPARATOR + this.name + SEPARATOR + this.address;
    }

    public String toString() {
        return this.name + "(" + this.connectionType + ")\n" + this.address;
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ChannelInfo that = (ChannelInfo) o;
        if (this.connectionType == that.connectionType && this.address.equals(that.address)) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return (this.connectionType.hashCode() * 31) + this.address.hashCode();
    }
}
