package com.navdy.obd.io;

import com.navdy.os.SystemProperties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.concurrent.TimeoutException;

import ch.qos.logback.classic.Level;

public class ObdCommandInterpreter {
    private static final int CARRIAGE_RETURN = 13;
    static final Logger Log = LoggerFactory.getLogger(ObdCommandInterpreter.class);
    public static final long NANOS_PER_MS = 1000000;
    public static final char TERMINAL_INPUT_INDICATOR = '>';
    public boolean debug = SystemProperties.getBoolean("persist.sys.obd.debug", false);
    private boolean didGetTextResponse;
    private InputStream inputStream;
    private boolean isInterrupted;
    private byte[] lastCommandBytes;
    private int lastReadCharacter = -1;
    private OutputStream outputStream;
    private long readTimeOutMillis = 10000;
    private long readTimeOutNanos = ((this.readTimeOutMillis * 1000) * 1000);

    public ObdCommandInterpreter(InputStream inputStream, OutputStream outputStream) {
        this.inputStream = inputStream;
        this.outputStream = outputStream;
    }

    public void setReadTimeOut(int readTimeOutMillis) {
        Log.debug("ObdCommandInterpreter : setReadTimeOut {} ", readTimeOutMillis);
        this.readTimeOutMillis = (long) readTimeOutMillis;
        this.readTimeOutNanos = this.readTimeOutMillis * 1000000;
    }

    public int read() throws IOException {
        return this.inputStream.read();
    }

    public void write(String command) throws IOException {
        Log.debug("Command {} ", (Object) command);
        int length = command.length();
        this.lastCommandBytes = new byte[(length + 1)];
        System.arraycopy(command.getBytes(), 0, this.lastCommandBytes, 0, length);
        this.lastCommandBytes[length] = (byte) 13;
        this.outputStream.write(this.lastCommandBytes);
        this.outputStream.flush();
    }

    public void write(byte[] commandBytes) throws IOException {
        this.outputStream.write(commandBytes);
        this.outputStream.flush();
        this.lastCommandBytes = commandBytes;
    }

    public String readResponse() throws IOException, InterruptedException {
        return readResponse(true);
    }

    public String readLine() throws IOException, InterruptedException {
        return readResponse(false);
    }

    public int read(byte[] buffer) throws IOException, InterruptedException {
        return this.inputStream.read(buffer);
    }

    public String readResponse(boolean readCompleteResponse) throws InterruptedException, IOException {
        StringBuilder responseBuilder = new StringBuilder();
        this.didGetTextResponse = false;
        long start = System.nanoTime();
        long elapsedTime = 0;
        while (!Thread.interrupted()) {
            int read = this.inputStream.read();
            this.lastReadCharacter = read;
            if (read == -1 || this.lastReadCharacter == 62) {
                break;
            }
            elapsedTime = System.nanoTime() - start;
            if (elapsedTime < this.readTimeOutNanos) {
                if (this.debug) {
                    Log.debug("Character {}", Character.valueOf((char) this.lastReadCharacter));
                }
                if (this.lastReadCharacter != -2) {
                    boolean validHexDigit = (this.lastReadCharacter >= 48 && this.lastReadCharacter <= 57) || ((this.lastReadCharacter >= 65 && this.lastReadCharacter <= 70) || (this.lastReadCharacter >= 97 && this.lastReadCharacter <= 102));
                    boolean isWhiteSpace = this.lastReadCharacter == 32 || this.lastReadCharacter == 13;
//                        Log.debug("validHexDigit {}", (validHexDigit)?"true":"false");
//                        Log.debug("isWhiteSpace {}", (isWhiteSpace)?"true":"false");
                    if (!(validHexDigit || isWhiteSpace)) {
//                            Log.debug("didGetTextResponse");
                        this.didGetTextResponse = true;
                    }
                    if (this.didGetTextResponse || this.lastReadCharacter != 32) {
                        responseBuilder.append((char) this.lastReadCharacter);
                    }
                    if (!readCompleteResponse && this.lastReadCharacter == 13) {
                        Log.debug("!readCompleteResponse");
                        break;
                    }
                }
                if (Thread.interrupted()) {
                    Log.debug("Interrupted");
                    this.isInterrupted = true;
                    throw new InterruptedException();
                }
            } else {
                Log.debug("Timeout");
                break;
            }
        }
        if (Thread.interrupted()) {
            this.isInterrupted = true;
            Log.info("readResponse, Thread is interrupted");
            throw new InterruptedException();
        } else if (elapsedTime > this.readTimeOutNanos) {
            elapsedTime /= 1000000;
            Log.info("ObdCommandInterpreter : readResponse, timed out");
            throw new IOException("Timeout after " + elapsedTime + " ms");
        } else if (!readCompleteResponse || this.lastReadCharacter == 62) {
            return responseBuilder.toString();
        } else {
            throw new IOException("ObdCommandInterpreter : Unexpected end of stream");
        }
    }

    public String clearTerminal() throws IOException, InterruptedException, TimeoutException {
        return clearTerminal(false, false);
    }

    public void clearTerminalBulk() throws InterruptedException, TimeoutException {
        clearTerminalBulk(false, false);
    }

    public String clearTerminal(final boolean b, final boolean b2) throws IOException, InterruptedException, TimeoutException {
        ObdCommandInterpreter.Log.info("ObdCommandInterpreter : clearTerminal");
        final StringBuilder sb = new StringBuilder("");
        final long nanoTime = System.nanoTime();
        while (true) {
            final int read = this.inputStream.read();
            this.lastReadCharacter = read;
            if (read == -1 || Thread.interrupted() || (b2 && System.nanoTime() - nanoTime > this.readTimeOutNanos)) {
                break;
            }
            if (this.lastReadCharacter == -2) {
                ObdCommandInterpreter.Log.debug(" ObdCommandInterpreter : terminalClear, response {}", sb.toString().trim());
                break;
            }
            if (this.lastReadCharacter < 0) {
                continue;
            }
            sb.append(this.lastReadCharacter);
        }
        final long n = System.nanoTime() - nanoTime;
        if (b2 && n > this.readTimeOutNanos) {
            throw new TimeoutException("Clearing terminal timed out");
        }
        if (Thread.interrupted()) {
            this.isInterrupted = true;
            ObdCommandInterpreter.Log.debug("readResponse, Thread is interrupted");
            throw new InterruptedException();
        }
        ObdCommandInterpreter.Log.debug("Clearing terminal took {} ms", (Object)n);
        return sb.toString();
    }

    public void clearTerminalBulk(boolean mayInterrupt, boolean timed) throws InterruptedException, TimeoutException {
        Log.info("ObdCommandInterpreter : clearTerminal, timed {}", timed);
        long start = System.nanoTime();
        int length = 0;
        do {
            if ((mayInterrupt && Thread.interrupted()) || (timed && System.nanoTime() - start > this.readTimeOutNanos)) {
                break;
            }
            try {
                length = read(new byte[Level.INFO_INT]);
                Log.debug("clearTerminal length {} ", length);
            } catch (IOException e2) {
                Log.error("IOException exception {}", e2);
            } catch (Throwable e) {
                Log.error("Interrupted exception {}", e);
            }
        } while (length > 0);
        Log.debug("Terminal clear");
        long elapsedTime = System.nanoTime() - start;
        if (timed && elapsedTime > this.readTimeOutNanos) {
            throw new TimeoutException("Clearing terminal timed out");
        } else if (!mayInterrupt || !Thread.interrupted()) {
            if (mayInterrupt && Thread.interrupted()) {
                this.isInterrupted = true;
                Log.debug("readResponse, Thread is interrupted");
                throw new InterruptedException();
            }
            Log.debug("Clearing terminal took {} ms", elapsedTime);
        }
    }

    public boolean didGetTextResponse() {
        return this.didGetTextResponse;
    }

    public byte[] getCommandBytes() {
        return this.lastCommandBytes;
    }
}
