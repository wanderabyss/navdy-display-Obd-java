package com.navdy.obd;

import ch.qos.logback.core.CoreConstants;

public class Protocol {
    public static final int SAE_J1850_VPW = 1;
    public static final int SAE_J1850_PWM = 2;
    public static final int ISO_9141_2 = 3;
    public static final int ISO_14230_4_KWP = 4;
    public static final int ISO_14230_4_KWP_FAST = 5;
    public static final int CAN_11_500 = 6;
    public static final int CAN_29_500 = 7;
    public static final int CAN_11_250 = 8;
    public static final int CAN_29_250 = 9;
    public static final int SAE_J1939_CAN = 10;
    public static final Protocol SAE_J1850_VPW_PROTOCOL = new Protocol(SAE_J1850_VPW, "SAE J1850 PWM", 4, 6);
    public static final Protocol SAE_J1850_PWM_PROTOCOL = new Protocol(SAE_J1850_PWM, "SAE J1850 VPW", 4, 6);
    public static final Protocol ISO_9141_2_PROTOCOL = new Protocol(ISO_9141_2, "ISO 9141-2", 4, 6);
    public static final Protocol ISO_14230_4_KWP_PROTOCOL = new Protocol(ISO_14230_4_KWP, "ISO 14230-4 KWP", 4, 6);
    public static final Protocol ISO_14230_4_KWP_FAST_PROTOCOL = new Protocol(ISO_14230_4_KWP_FAST, "ISO 14230-4 KWP (fast)", 4, 6);
    public static final Protocol CAN_11_500_PROTOCOL = new Protocol(CAN_11_500, "ISO 15765-4 (CAN 11/500)", 0, 3);
    public static final Protocol CAN_29_500_PROTOCOL = new Protocol(CAN_29_500, "ISO 15765-4 (CAN 29/500)", 6, 8);
    public static final Protocol CAN_11_250_PROTOCOL = new Protocol(CAN_11_250, "ISO 15765-4 (CAN 11/250)", 0, 3);
    public static final Protocol CAN_29_250_PROTOCOL = new Protocol(CAN_29_250, "ISO 15765-4 (CAN 29/250)", 6, 8);
    public static final Protocol SAE_J1939_CAN_PROTOCOL = new Protocol(SAE_J1939_CAN, "SAE J1939 (CAN 29/250)", 6, 8);
    public static Protocol[] PROTOCOLS = new Protocol[]{SAE_J1850_PWM_PROTOCOL, SAE_J1850_VPW_PROTOCOL, ISO_9141_2_PROTOCOL,
                                                        ISO_14230_4_KWP_PROTOCOL, ISO_14230_4_KWP_FAST_PROTOCOL, CAN_11_500_PROTOCOL,
                                                        CAN_29_500_PROTOCOL, CAN_11_250_PROTOCOL, CAN_29_250_PROTOCOL, SAE_J1939_CAN_PROTOCOL};
    public final FrameFormat format;
    public final int id;
    public final String name;

    public Protocol(int id, String name, int ecuStart, int ecuEnd) {
        this.id = id;
        this.name = name;
        this.format = new FrameFormat(ecuStart, ecuEnd);
    }

    static Protocol parse(String protocolNumber) {
        if (protocolNumber.startsWith("A")) {
            protocolNumber = protocolNumber.substring(1);
        }
        try {
            int protocolIndex = Integer.valueOf(protocolNumber.trim());
            if (protocolIndex < 1 || protocolIndex > PROTOCOLS.length) {
                return null;
            }
            return PROTOCOLS[protocolIndex - 1];
        } catch (NumberFormatException e) {
            return null;
        }
    }

    public String toString() {
        return "Protocol{name='" + this.name + CoreConstants.SINGLE_QUOTE_CHAR + CoreConstants.CURLY_RIGHT;
    }
}
