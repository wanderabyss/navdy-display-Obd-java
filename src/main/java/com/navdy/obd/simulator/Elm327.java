package com.navdy.obd.simulator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;

public class Elm327 extends Thread {
    static final Logger Log = LoggerFactory.getLogger(Elm327.class);
    private Ecu ecu = new Ecu();
    private InputStream input;
    private OutputStream output;

    Elm327(InputStream input, OutputStream output) {
        this.input = input;
        this.output = output;
    }

    public void run() {
        try {
            Log.info("Starting Elm327 reader thread");
            BufferedReader reader = new BufferedReader(new InputStreamReader(this.input));
            OutputStreamWriter writer = new OutputStreamWriter(this.output);
            while (true) {
                String command = reader.readLine();
                if (command != null) {
                    String command2 = command.toLowerCase();
                    if (command2.startsWith("at")) {
                        handleCommand(writer, command2);
                    } else if (command2.startsWith("01")) {
                        handleRead(writer, command2);
                    } else if (command2.equals("quit")) {
                        Log.info("Quitting Elm327 reader thread");
                        return;
                    } else {
                        Log.warn("Unknown command {}", command2);
                        writer.write("UNKNOWN COMMAND\r>");
                    }
                    writer.flush();
                } else {
                    return;
                }
            }
        } catch (Throwable e) {
            Log.error("Exception - exiting Elm327 thread", e);
        }
    }

    private void handleCommand(OutputStreamWriter writer, String command) throws IOException {
        Log.debug("Handling command {}", command);
        writer.write("OK\r>");
    }

    private void handleRead(OutputStreamWriter writer, String command) throws IOException {
        Log.debug("Reading pid {}", command);
        StringBuilder response = new StringBuilder();
        response.append("41");
        for (String tail = command.substring(2).trim(); tail.length() >= 2; tail = tail.substring(2)) {
            String pid = tail.substring(0, 2);
            if (this.ecu.supports(pid)) {
                response.append(pid);
                response.append(this.ecu.readPid(pid));
            }
        }
        response.append("\r>");
        writer.write(response.toString());
    }
}
