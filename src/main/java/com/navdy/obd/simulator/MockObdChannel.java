package com.navdy.obd.simulator;

import com.navdy.obd.io.ChannelInfo;
import com.navdy.obd.io.ChannelInfo.ConnectionType;
import com.navdy.obd.io.IChannel;
import com.navdy.obd.io.IChannelSink;
import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PipedInputStream;
import java.io.PipedOutputStream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class MockObdChannel implements IChannel {
    public static final String CONNECTION_NAME = "mock_obd_channel";
    static final Logger Log = LoggerFactory.getLogger(MockObdChannel.class);
    public static ChannelInfo MOCK_CHANNEL_INFO = new ChannelInfo(ConnectionType.MOCK, "simulated", CONNECTION_NAME);
    private PipedInputStream inputStream;
    private PipedOutputStream outputStream;
    private Elm327 simulator;
    private IChannelSink sink;
    private int state = 0;

    public MockObdChannel(IChannelSink sink) {
        this.sink = sink;
    }

    public int getState() {
        return this.state;
    }

    private void setState(int newState) {
        this.state = newState;
        this.sink.onStateChange(newState);
    }

    public void connect(String address) {
        if (!CONNECTION_NAME.equals(address)) {
            Log.error("Invalid address passed to channel");
        } else if (this.state == 0) {
            Log.debug("Connecting to mock obd channel");
            this.outputStream = new PipedOutputStream();
            this.inputStream = new PipedInputStream();
            try {
                this.simulator = new Elm327(new PipedInputStream(this.outputStream), new PipedOutputStream(this.inputStream));
                this.simulator.start();
                setState(2);
            } catch (Throwable e) {
                Log.error("Exception starting elm327", e);
            }
        }
    }

    public void disconnect() {
        if (this.state != 0) {
            sendQuitCommand();
            safelyClose(this.outputStream);
            safelyClose(this.inputStream);
            if (this.simulator != null) {
                try {
                    this.simulator.join();
                } catch (Throwable e) {
                    Log.debug("Interrupted joining simulator thread", e);
                }
                this.simulator = null;
            }
            this.outputStream = null;
            this.inputStream = null;
            setState(0);
        }
    }

    private void sendQuitCommand() {
        if (this.outputStream != null) {
            try {
                this.outputStream.write("QUIT\r>".getBytes());
            } catch (Throwable e) {
                Log.error("Exception sending quit command", e);
            }
        }
    }

    private void safelyClose(Closeable stream) {
        if (stream != null) {
            try {
                stream.close();
            } catch (Throwable e) {
                Log.error("Exception closing stream", e);
            }
        }
    }

    public InputStream getInputStream() throws IOException {
        return this.inputStream;
    }

    public OutputStream getOutputStream() throws IOException {
        return this.outputStream;
    }
}
