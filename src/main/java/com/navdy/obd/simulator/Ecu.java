package com.navdy.obd.simulator;

import com.navdy.obd.update.STNSerialDeviceFirmwareManager;
import java.util.HashMap;
import java.util.Set;

public class Ecu {
    private HashMap<String, IPid> pids = new HashMap<>();

    class Pid implements IPid {
        int bytes;
        String code;
        LinearInterpolation interpolator;
        String name;

        Pid(String name, String code, int bytes, LinearInterpolation interpolator) {
            this.name = name;
            this.code = code;
            this.bytes = bytes;
            this.interpolator = interpolator;
        }

        public String getValue(long timestamp) {
            long value = this.interpolator.getValue(timestamp);
            if (this.bytes == 1) {
                return String.format("%02x", 255 & value);
            } else if (this.bytes != 2) {
                return "";
            } else {
                return String.format("%04x", 65535 & value);
            }
        }

        public String getCode() {
            return this.code;
        }
    }

    class SupportedPids implements IPid {
        private String bitMask;

        SupportedPids(Set<String> pidCodes) {
            byte[] bitField = new byte[4];
            for (String code : pidCodes) {
                int id = Integer.valueOf(code, 16) - 1;
                int pos = id / 8;
                bitField[pos] = (byte) (bitField[pos] | (1 << (7 - (id % 8))));
            }
            StringBuilder hexString = new StringBuilder();
            for (byte val : bitField) {
                if (val < (byte) 16) {
                    hexString.append('0');
                }
                hexString.append(Integer.toHexString(val));
            }
            this.bitMask = hexString.toString();
        }

        public String getValue(long timestamp) {
            return this.bitMask;
        }

        public String getCode() {
            return "00";
        }
    }

    Ecu() {
        addPid(new Pid("Speed", "0d", 1, LinearInterpolation.generate(50, 0, 100, 10000, 5000)));
        addPid(new Pid("RPM", "0c", 2, LinearInterpolation.generate(50, 0, 16000, 10000, 500)));
        addPid(new Pid("Temperature", STNSerialDeviceFirmwareManager.CURRENT_UPDATE_FILE_VERSION, 1, LinearInterpolation.generate(50, 0, 255, 100000, 10000)));
        addPid(new SupportedPids(this.pids.keySet()));
    }

    public void addPid(IPid pid) {
        this.pids.put(pid.getCode(), pid);
    }

    public boolean supports(String pid) {
        return this.pids.keySet().contains(pid);
    }

    public String readPid(String code) {
        long timestamp = System.currentTimeMillis();
        IPid pid = this.pids.get(code);
        if (pid != null) {
            return pid.getValue(timestamp);
        }
        return "";
    }
}
