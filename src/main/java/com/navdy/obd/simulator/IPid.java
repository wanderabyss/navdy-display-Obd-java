package com.navdy.obd.simulator;

/* compiled from: Ecu */
interface IPid {
    String getCode();

    String getValue(long j);
}
