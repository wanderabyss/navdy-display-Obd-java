package com.navdy.obd;

public class FrameFormat {
    public final int ecuEnd;
    public final int ecuStart;

    public FrameFormat(int ecuStart, int ecuEnd) {
        this.ecuStart = ecuStart;
        this.ecuEnd = ecuEnd;
    }
}
