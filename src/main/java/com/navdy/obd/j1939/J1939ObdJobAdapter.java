package com.navdy.obd.j1939;

import android.os.SystemClock;
import android.util.Log;
import com.navdy.obd.ObdDataObserver;
import com.navdy.obd.ObdService;
import com.navdy.obd.Pid;
import com.navdy.obd.PidSet;
import com.navdy.obd.Pids;
import com.navdy.obd.Utility;
import com.navdy.obd.VehicleStateManager;
import com.navdy.obd.io.IChannel;
import com.navdy.obd.j1939.MonitorJ1939DataJob.IDataObserver;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class J1939ObdJobAdapter implements Runnable, IDataObserver {
    public static PidSet J1939SupportedObdPids = new PidSet();
    public static final String TAG = ObdService.class.getSimpleName();
    private IChannel channel;
    private J1939ObdJobListener listener;
    private MonitorJ1939DataJob monitorJ1939DataJob;
    private List<Pid> monitoredPidsList;
    private ObdDataObserver obdDataObserver;
    public HashMap<Long, Pid> parameterToPidMapping;
    private J1939Profile profile;
    private boolean started = false;
    private VehicleStateManager vehicleStateManager;

    public interface J1939ObdJobListener {
        void onNewDataAvailable();
    }

    static {
        J1939SupportedObdPids.add(13);
        J1939SupportedObdPids.add(12);
        J1939SupportedObdPids.add(5);
        J1939SupportedObdPids.add(47);
        J1939SupportedObdPids.add(256);
        J1939SupportedObdPids.add(49);
        J1939SupportedObdPids.add(Pids.ENGINE_OIL_PRESSURE);
        J1939SupportedObdPids.add(Pids.ENGINE_TRIP_FUEL);
        J1939SupportedObdPids.add(Pids.TOTAL_VEHICLE_DISTANCE);
    }

    public void onParameterGroupData(int pgn, List<Parameter> parameters) {
        for (Parameter params : parameters) {
            Pid pid = this.parameterToPidMapping.get(params.spn);
            byte[] data = params.getRecentData();
            if (data != null) {
                int dataLength = params.lengthInBits >= 8 ? params.lengthInBits / 8 : 1;
                if (dataLength >= 1) {
                    double value = (params.resolution * ((double) Utility.bytesToInt(data, 0, dataLength, false))) + ((double) params.valueOffset);
                    pid.setTimeStamp(SystemClock.elapsedRealtime());
                    pid.setValue(value);
                }
            }
        }
        if (!this.started) {
            this.started = true;
            this.vehicleStateManager.update(this.monitoredPidsList);
        }
        this.vehicleStateManager.onScanComplete();
        if (this.listener != null) {
            this.listener.onNewDataAvailable();
        }
    }

    public List<Pid> getMonitoredPidsList() {
        return this.monitoredPidsList;
    }

    public J1939ObdJobAdapter(IChannel channel, J1939Profile profile, List<Pid> pids, VehicleStateManager vehicleStateManager, ObdDataObserver logger) {
        this.obdDataObserver = logger;
        this.parameterToPidMapping = new HashMap();
        this.monitoredPidsList = pids;
        this.vehicleStateManager = vehicleStateManager;
        this.channel = channel;
        List<Parameter> parametersToMonitor = new ArrayList<>();
        for (Pid pid : pids) {
            List<Parameter> parameters = profile.getParameters(pid.getId());
            if (parameters != null) {
                parametersToMonitor.addAll(parameters);
                for (Parameter param : parameters) {
                    this.parameterToPidMapping.put(param.spn, pid);
                }
            }
        }
        this.monitorJ1939DataJob = new MonitorJ1939DataJob(channel, parametersToMonitor, this.obdDataObserver, this);
    }

    public void setListener(J1939ObdJobListener listener) {
        this.listener = listener;
    }

    public void run() {
        this.monitorJ1939DataJob.run();
        Log.d(TAG, "Monitoring finished, disconnecting the channel");
        if (this.channel != null) {
            this.channel.disconnect();
        }
    }
}
