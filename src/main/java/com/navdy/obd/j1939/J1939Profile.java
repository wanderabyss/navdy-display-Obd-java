package com.navdy.obd.j1939;

import android.util.Log;

import com.navdy.obd.Pids;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class J1939Profile {
    public static final long SPN_ENGINE_AVERAGE_FUEL_ECONOMY = 185;
    public static final long SPN_ENGINE_OIL_PRESSURE = 100;
    public static final long SPN_ENGINE_TRIP_FUEL = 5053;
    public static final long SPN_TOTAL_DISTANCE_TRAVELLED = 917;
    public static final String TAG = J1939Profile.class.getSimpleName();
    public static HashMap<Integer, List<Parameter>> obdPidToJ1939ParameterMapping = new HashMap();
    public static HashMap<Long, Integer> parameterToCustomPidMapping = new HashMap();

    class Column {
        public static final String BIT_LENGTH = "BitLength";
        public static final String BIT_OFFSET = "BitOffset";
        public static final String BYTE_OFFSET = "ByteOffset";
        public static final String MAX_VALUE = "MaxValue";
        public static final String MIN_VALUE = "MinValue";
        public static final String NAME = "Name";
        public static final String OBD_PID = "OBD_Pid";
        public static final String PGN = "PGN";
        public static final String RESOLUTION = "Resolution";
        public static final String SPN = "SPN";
        public static final String VALUE_OFFSET = "ValueOffset";

        Column() {
        }
    }

    static {
        parameterToCustomPidMapping.put(100L, Pids.ENGINE_OIL_PRESSURE);
        parameterToCustomPidMapping.put(185L, 256);
        parameterToCustomPidMapping.put(SPN_ENGINE_TRIP_FUEL, Pids.ENGINE_TRIP_FUEL);
        parameterToCustomPidMapping.put(917L, Pids.TOTAL_VEHICLE_DISTANCE);
    }

    List<Parameter> getParameters(int pid) {
        return obdPidToJ1939ParameterMapping.get(pid);
    }

    public void load(InputStream inputStream) {
        Reader reader;
        Throwable e;
        Throwable th;
        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
            for (CSVRecord record : CSVFormat.DEFAULT.withHeader().parse(bufferedReader)) {
                String name = record.get("Name");
                long spn = Long.parseLong(record.get(Column.SPN));
                int pgn = Integer.parseInt(record.get(Column.PGN));
                int byteOffset = Integer.parseInt(record.get("ByteOffset"));
                int bitOffset = Integer.parseInt(record.get("BitOffset"));
                int bitLength = Integer.parseInt(record.get("BitLength"));
                int valueOffset = Integer.parseInt(record.get("ValueOffset"));
                double resolution = Double.parseDouble(record.get("Resolution"));
                double minValue = Double.parseDouble(record.get("MinValue"));
                double maxValue = Double.parseDouble(record.get("MaxValue"));
                int obd2Pid = Integer.parseInt(record.get("OBD_Pid"));
                Parameter parameter = new Parameter(name, spn, pgn, byteOffset, bitOffset, bitLength, (long) valueOffset, resolution, minValue, maxValue);
                Log.d(TAG, "Parameter " + parameter + ", PID : " + obd2Pid);
                if (obd2Pid == -1) {
                    Log.d(TAG, "PID is not defined for parameter :" + name);
                    if (parameterToCustomPidMapping.containsKey(spn)) {
                        obd2Pid = parameterToCustomPidMapping.get(spn);
                        Log.d(TAG, "Custom PID found " + obd2Pid);
                    } else {
                        Log.d(TAG, "No custom PID found either");
                    }
                }
                if (obd2Pid != -1) {
                    List<Parameter> parameters = obdPidToJ1939ParameterMapping.get(obd2Pid);
                    if (parameters == null) {
                        parameters = new ArrayList<>();
                        obdPidToJ1939ParameterMapping.put(obd2Pid, parameters);
                    }
                    parameters.add(parameter);
                }
            }
        } catch (RuntimeException e5) {
            try {
                Log.d(TAG, "RunTimeException during loading the profile", e5);
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (Throwable e22) {
                        Log.d(TAG, "IOException closing the reader", e22);
                    }
                }
            } catch (Throwable th3) {
                if (bufferedReader != null) {
                    try {
                        bufferedReader.close();
                    } catch (Throwable e222) {
                        Log.d(TAG, "IOException closing the reader", e222);
                    }
                }
                throw th3;
            }
        } catch (IOException e6) {
            Log.d(TAG, "IOException during loading the profile", e6);
            try {
                bufferedReader.close();
            } catch (Throwable e2222) {
                Log.d(TAG, "IOException closing the reader", e2222);
            }
        }
    }
}
