package com.navdy.obd.j1939;

public class Parameter {
    int bitOffset;
    int byteOffset;
    int lengthInBits;
    double maxValue;
    double minValue;
    public String name;
    public int pgn;
    byte[] recentData;
    double resolution;
    public long spn;
    long valueOffset;

    public Parameter(String name, long spn, int pgn, int byteOffset, int bitOffset, int lengthInBits, long valueOffset, double resolution, double minValue, double maxValue) {
        this.name = name;
        this.spn = spn;
        this.pgn = pgn;
        this.byteOffset = byteOffset;
        this.bitOffset = bitOffset;
        this.lengthInBits = lengthInBits;
        this.resolution = resolution;
        this.valueOffset = valueOffset;
        this.minValue = minValue;
        this.maxValue = maxValue;
    }

    public byte[] getRecentData() {
        return this.recentData;
    }

    public void setRecentData(byte[] recentData) {
        this.recentData = recentData;
    }

    public String toString() {
        return "Parameter { name=" + this.name + "," + "\nSPN = " + this.spn + "," + "\nPGN = " + this.pgn + "," + "\nByteOffset = " + this.byteOffset + "," + "\nBitOffset = " + this.bitOffset + "," + "\nLength in bits = " + this.lengthInBits + "," + "\nResolution = " + this.resolution + "," + "\nValue offset = " + this.valueOffset + "," + "\nMin = " + this.minValue + "," + "\nMax = " + this.maxValue + "," + "}";
    }
}
