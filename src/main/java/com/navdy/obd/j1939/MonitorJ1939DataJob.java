package com.navdy.obd.j1939;

import android.util.Log;

import com.navdy.obd.ObdDataObserver;
import com.navdy.obd.ObdJob;
import com.navdy.obd.ObdJob.IListener;
import com.navdy.obd.ObdService;
import com.navdy.obd.Protocol;
import com.navdy.obd.Utility;
import com.navdy.obd.command.BatchCommand;
import com.navdy.obd.command.ICommand;
import com.navdy.obd.command.ObdCommand;
import com.navdy.obd.io.IChannel;
import com.navdy.obd.io.ObdCommandInterpreter;
import com.navdy.obd.util.HexUtil;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class MonitorJ1939DataJob implements Runnable {
    private static final String ADD_PGN_FILTER_COMMAND = "STFPGA %s";
    private static final int CARRIAGE_RETURN = 13;
    public static final ObdCommand CLEAR_ALL_PASS_FILTERS = new ObdCommand("STFCA");
    private static ObdCommand CLEAR_FILTERS_COMMAND = new ObdCommand("Clear PGN filters", "STFPGC");
    public static final int HEADER_SIZE = 4;
    private static final String MONITORING_COMMAND = "STM";
    public static final String TAG = ObdService.class.getSimpleName();
    public static final int TIMEOUT_MILLIS = ((int) TimeUnit.SECONDS.toMillis(5));
    private static ObdCommand TURN_OFF_SILENT_MONITORING_COMMAND = new ObdCommand("TurnOffSilentMonitiroing", "ATCSM0");
    private static ObdCommand TURN_ON_HEADERS = new ObdCommand("Turn on Headers", "ATH1");
    private static final String VALID_PGN_FILTER_REGEX = "[0-9A-Fa-f]{1,6}";
    private IChannel channel;
    private ObdCommandInterpreter commandInterpreter;
    private IDataObserver dataObserver;
    private ObdDataObserver obdDataObserver;
    private HashMap<Integer, List<Parameter>> parameterGroupLookup = new HashMap();
    private ArrayList<Integer> parameterGroupsToMonitor = new ArrayList<>();

    public interface IDataObserver {
        void onParameterGroupData(int i, List<Parameter> list);
    }

    public MonitorJ1939DataJob(IChannel channel, List<Parameter> parametersList, ObdDataObserver logger, IDataObserver dataObserver) {
        this.channel = channel;
        this.obdDataObserver = logger;
        this.dataObserver = dataObserver;
        try {
            this.commandInterpreter = new ObdCommandInterpreter(channel.getInputStream(), channel.getOutputStream());
            this.commandInterpreter.setReadTimeOut(TIMEOUT_MILLIS);
            if (parametersList != null) {
                for (Parameter parameter : parametersList) {
                    List<Parameter> parameterGroup = this.parameterGroupLookup.get(parameter.pgn);
                    if (parameterGroup == null) {
                        parameterGroup = new ArrayList<>();
                        this.parameterGroupLookup.put(parameter.pgn, parameterGroup);
                        this.parameterGroupsToMonitor.add(parameter.pgn);
                    }
                    parameterGroup.add(parameter);
                }
            }
        } catch (IOException e) {
            Log.e(TAG, "Error accessing streams from channel ", e);
        }
    }

    public void run() {
        if (this.commandInterpreter == null) {
            Log.e(TAG, "Command interpreter is not initialized properly");
            return;
        }
        try {
            Log.d(TAG, "Starting to monitor the J1939 data on the CAN bus");
            if (this.parameterGroupsToMonitor.size() == 0) {
                Log.e(TAG, "No parameters to be monitored, not monitoring");
                return;
            }
            final BatchCommand setupMonitoringCommand = new BatchCommand();
            setupMonitoringCommand.add(new ObdCommand("DUMMY", "\n"));
            setupMonitoringCommand.add(new ObdCommand("STM\r"));
            setupMonitoringCommand.add(TURN_OFF_SILENT_MONITORING_COMMAND);
            setupMonitoringCommand.add(CLEAR_ALL_PASS_FILTERS);
            for (Object aParameterGroupsToMonitor : this.parameterGroupsToMonitor) {
                String pgnHex = Long.toHexString((long) aParameterGroupsToMonitor).toUpperCase();
                ObdCommand filterCommand = new ObdCommand(String.format(ADD_PGN_FILTER_COMMAND, pgnHex));
                Log.d(TAG, "Adding PGN filter with command " + filterCommand.getCommand());
                setupMonitoringCommand.add(filterCommand);
            }
            setupMonitoringCommand.add(TURN_ON_HEADERS);
            new ObdJob(setupMonitoringCommand, this.channel, Protocol.SAE_J1939_CAN_PROTOCOL, new IListener() {
                public void onComplete(ObdJob job, boolean success) {
                    Log.d(MonitorJ1939DataJob.TAG, "ApplyFilter complete , Success :" + success);
                    if (success) {
                        for (ICommand command : setupMonitoringCommand.getCommands()) {
                            ObdCommand obdCommand = (ObdCommand) command;
                            Log.d(MonitorJ1939DataJob.TAG, "Response for " + obdCommand.getCommand() + " - " + obdCommand.getResponse().trim());
                        }
                        MonitorJ1939DataJob.this.startMonitoring();
                        return;
                    }
                    MonitorJ1939DataJob.this.obdDataObserver.onError("Failed to apply PGN filters");
                    Log.d(MonitorJ1939DataJob.TAG, "Failed to apply the PGN filters, not initiating the monitoring");
                }
            }, this.obdDataObserver, false).run();
        } catch (Exception exception) {
            Log.d(TAG, "Exception while adding filters " + exception);
            exception.printStackTrace();
        }
    }

    private void startMonitoring() {
        Log.d(TAG, "startMonitoring ");
        try {
            this.commandInterpreter.write(MONITORING_COMMAND);
        } catch (IOException e) {
            Log.e(TAG, "IOException when writing the command");
        }
        this.obdDataObserver.onCommand(MONITORING_COMMAND);
        Log.d(TAG, MONITORING_COMMAND);
        while (true) {
            try {
                String response = this.commandInterpreter.readLine();
                this.obdDataObserver.onResponse(response);
                byte[] bytes = new byte[(response.length() / 2)];
                HexUtil.parseHexString(response, bytes, 0, bytes.length);
                byte firstByte = bytes[0];
                int pgn = Utility.bytesToInt(bytes, 1, 2, true);
                int src = bytes[3];
                List<Parameter> parameterGroup = this.parameterGroupLookup.get(pgn);
                if (parameterGroup != null) {
                    for (Parameter parameter : parameterGroup) {
                        int dataLength;
                        if (parameter.lengthInBits >= 8) {
                            dataLength = parameter.lengthInBits / 8;
                        } else {
                            dataLength = 1;
                        }
                        if (parameter.getRecentData() == null) {
                            parameter.setRecentData(new byte[dataLength]);
                        }
                        System.arraycopy(bytes, parameter.byteOffset + 4, parameter.getRecentData(), 0, dataLength);
                    }
                    if (this.dataObserver != null) {
                        this.dataObserver.onParameterGroupData(pgn, parameterGroup);
                    }
                }
            } catch (IOException e2) {
                Log.e(TAG, "Error while monitoring J1939 ", e2);
                this.obdDataObserver.onError("IOException while monitoring " + e2.toString());
                return;
            } catch (InterruptedException e3) {
                Log.e(TAG, "Monitoring interrupted ", e3);
                this.obdDataObserver.onError("Monitoring was interrupted, finishing execution");
                return;
            }
        }
    }
}
