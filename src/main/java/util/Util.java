package util;

import android.content.Context;
import android.util.Log;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.regex.Pattern;

public class Util {
    private static final int END_OF_STREAM = -1;
    private static final int INPUT_BUFFER_SIZE = 16384;
    private static final String TAG = "ObdService";
    public static final String UTF_8 = "UTF-8";

    public static String readObdAssetFile(Context context, String assetFileName) throws IOException {
        InputStream is = context.getAssets().open(assetFileName);
        String content = convertInputStreamToString(is, UTF_8);
        try {
            is.close();
        } catch (IOException e) {
            Log.e(TAG, "Error closing the stream after reading the asset file");
        }
        return content;
    }

    public static String convertInputStreamToString(InputStream inputStream, String charSet) throws IOException {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        byte[] buffer = new byte[16384];
        while (true) {
            int n = inputStream.read(buffer);
            if (n == -1) {
                return byteArrayOutputStream.toString(charSet);
            }
            byteArrayOutputStream.write(buffer, 0, n);
        }
    }

    public static Configuration[] loadConfigurationMappingList(Context context, String fileName) {
        int i = 0;
        Configuration[] configurationsList = null;
        try {
            String[] configurationMappingEntries = readObdAssetFile(context, fileName).split("\n");
            configurationsList = new Configuration[configurationMappingEntries.length];
            int length = configurationMappingEntries.length;
            int i2 = 0;
            while (i < length) {
                String[] parts = configurationMappingEntries[i].split(",");
                Configuration configuration = new Configuration();
                configuration.pattern = Pattern.compile(parts[0], 2);
                configuration.configurationName = parts[1];
                int i3 = i2 + 1;
                configurationsList[i2] = configuration;
                i++;
                i2 = i3;
            }
        } catch (IOException ignored) {
        }
        return configurationsList;
    }

    public static Configuration pickConfiguration(Configuration[] configurationMapping, String expression) {
        for (Configuration configuration : configurationMapping) {
            if (configuration.pattern.matcher(expression).matches()) {
                return configuration;
            }
        }
        return null;
    }
}
